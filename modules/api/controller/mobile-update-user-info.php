<?php

error_reporting(E_ERROR);
include_once API_LIBRARY_PATH . '/API.php';
$fs = new functions();
$db = new Database();
$utilities = new APIUtilities();
$utilities->addAPIHeaders();

$currentUser = Auth::getAuth('current_user');

//Ex
// /API/mobile-update-user-info?userAccountName=&userEmail=&userUsername=&userFname=&userLname=&userContactNumber=

//  requires authentication
if (!$currentUser) {
    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = APIStatus::ERROR_UNAUTHORIZED;
    $response['error_message'] = 'You must be logged in to access this service';

    echo json_encode($response);
    return;
}

if (!array_key_exists('userAccountName', $_GET)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for userAccountName parameter';
    echo json_encode($response);
    return;
}

if (!array_key_exists('userEmail', $_GET)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for userEmail parameter';
    echo json_encode($response);
    return;
}

if (!array_key_exists('userUsername', $_GET)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for userUsername parameter';
    echo json_encode($response);
    return;
}

if (!array_key_exists('userFname', $_GET)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for userFname parameter';
    echo json_encode($response);
    return;
}

if (!array_key_exists('userLname', $_GET)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for userLname parameter';
    echo json_encode($response);
    return;
}

if (!array_key_exists('userContactNumber', $_GET)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for userContactNumber parameter';
    echo json_encode($response);
    return;
}

$response = [
    'status'        => APIStatus::STATUS_ERROR,
    'results'       => null,
    'error'         => null,
    'error_message' => null,
];



$userAccountName = stripslashes(htmlspecialchars($_GET['userAccountName'], ENT_QUOTES));
$userEmail = stripslashes(htmlspecialchars($_GET['userEmail'], ENT_QUOTES));
$userUsername = stripslashes(htmlspecialchars($_GET['userUsername'], ENT_QUOTES));
$userFname = stripslashes(htmlspecialchars($_GET['userFname'], ENT_QUOTES));
$userLname = stripslashes(htmlspecialchars($_GET['userLname'], ENT_QUOTES));
$userContactNumber = stripslashes(htmlspecialchars($_GET['userContactNumber'], ENT_QUOTES));

//$userContactNumber     = filter_input(INPUT_GET, 'userContactNumber');

try {

        if ($db->query("SELECT * FROM tbuser a 
            LEFT JOIN tbcompany b ON a.company_id=b.id WHERE a.email={$db->escape($userEmail)}  AND a.id!={$db->escape($currentUser['id'])}", "numrows") > 0) {
            $userInfo = $db->query("SELECT * FROM tbuser a 
            LEFT JOIN tbcompany b ON a.company_id=b.id WHERE a.email={$db->escape($userEmail)}  AND a.id!={$db->escape($currentUser['id'])}", "row");


            if ($userInfo['company_id'] != $currentUser['company_id']) {
                $msg = "User is already registered in other company (" . $userInfo['name'] . ")";
            } else {
                $msg = "User is already registered in current company";

            }
        }else{
            if (!$fs->VerifyMailAddress($userEmail)) {
                $msg = "Please type your correct email format.";
            } else {
                if ($db->query("SELECT * FROM tbuser WHERE display_name={$db->escape($userAccountName)} AND id!={$db->escape($currentUser['id'])} AND company_id={$db->escape($currentUser['company_id'])} AND is_active={$db->escape(1)}", "numrows") > 0) {
                    $msg = "Your account name is already exist."."SELECT * FROM tbuser WHERE display_name={$db->escape($userAccountName)} AND id!={$db->escape($currentUser['id'])} AND company_id={$db->escape($currentUser['company_id'])} AND is_active={$db->escape(1)}";
                } else {
                    $date = $fs->currentDateTime();
                   
                    $updateUser = array("email" => trim($userEmail), "display_name" => trim($userAccountName),
                        "first_name" => trim($userFname), "last_name" => trim($userLname),"username"=>trim($userUsername));
                   

                    $userID = $db->update("tbuser", $updateUser,array("id"=>$currentUser['id']));

                    // Audit Logs
                    // $userCompany->auditLogs($auth, "tbuser", "8", $userID);

                    $login = $db->query("SELECT *
                                        FROM tbuser
                                        WHERE id={$db->escape($currentUser['id'])} ","row");
                    Auth::setAuth('current_user',$login);

                    $msg = "User was successfully saved.";
                }
            }
        }
        


        $response['status']      = APIStatus::STATUS_SUCCESS;
        $response['results']     = $msg;
        $response['server_date'] = $fs->currentDateTime();
    
    
} catch (APIException $e) {

    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = $e->cause;
    $response['error_message'] = $e->message;
    $response['server_date']   = $fs->currentDateTime();
}

echo $fs->view_json_formatter(json_encode($response));
error_reporting(0);
