<?php

$auth      = new Auth();
$files_dao = new FilesDAO();

$current_user = $auth->getAuth("current_user");

$valid_formats    = unserialize(FILES_EXTENSION);
$parameters       = filter_input_array(INPUT_POST);
$target_directory = $parameters["target_directory"];

$uploaded_files = array();

$file_count                  = count($_FILES['file']['tmp_name']);
$user_temporary_files_folder = md5(microtime(true)).md5(md5($current_user["id"]));
$temporary_files_path        = "images/{$target_directory}/temporary_files/{$user_temporary_files_folder}";

if (array_key_exists("upload_permanent", $parameters)) {
    if ($parameters["upload_permanent"]) {
        $temporary_files_path = "images/{$target_directory}/{$user_temporary_files_folder}";
    }
}

//  Create the path if it is not yet existing
if (!is_dir($temporary_files_path)) {
    mkdir($temporary_files_path);
}

//for ($i = 0; $i < $file_count; $i ++) {
//    $uploaded_file     = $_FILES['file']['tmp_name'][$i];
//    $original_filename = $_FILES['file']['name'][$i];
//    $splitted_filename = explode(".", $original_filename);
//    $extension         = $splitted_filename[count($splitted_filename) - 1];
//    $new_filename      = md5(md5(time() + " " + $i)) . "." . $extension;
//
//    $target_file_path = "{$temporary_files_path}/{$new_filename}";
//
//    move_uploaded_file($uploaded_file, $target_file_path);
//    array_push($uploaded_files, array(
//        "original_filename_no_extension" => str_replace("." . $extension, "", $original_filename),
//        "original_filename" => $original_filename,
//        "new_filename" => $new_filename,
//        "full_file_path" => $target_file_path,
//        "test_only" => $_FILES['file']
//    ));
//}

$uploaded_file     = $_FILES['file']['tmp_name'];
$original_filename = $_FILES['file']['name'];
$splitted_filename = explode(".", $original_filename);
$extension         = $splitted_filename[count($splitted_filename) - 1];
$new_filename      = md5(microtime(true)) . md5(md5(uniqid() + " " + $i)) . "." . $extension;

$target_file_path = "{$temporary_files_path}/{$original_filename}";

move_uploaded_file($uploaded_file, $target_file_path);
$this->results =  array(
    "original_filename_no_extension" => str_replace("." . $extension, "", $original_filename),
    "original_filename" => $original_filename,
    "new_filename" => $new_filename,
    "full_file_path" => $target_file_path
);
include(dirname(__FILE__) . "/../" . "/view/api_response.phtml");
