<?php

error_reporting(E_ERROR);
include_once API_LIBRARY_PATH . '/API.php';
$function = new functions();

$utilities = new APIUtilities();
$utilities->addAPIHeaders();
$db = new Database();
$currentUser = Auth::getAuth('current_user');

//  requires authentication

$info = array();

$info["user-info"] = $currentUser;
$info["company-info"] = new Company($db,$currentUser['company_id']);

$positions = $db->query("SELECT * FROM tbpositions WHERE id = {$db->escape($currentUser['position'])}","row");

$info["position-info"] = $positions;

$auth = array();
$auth["Auth"] = $info;

echo $function->view_json_formatter(json_encode($auth));
error_reporting(0);
