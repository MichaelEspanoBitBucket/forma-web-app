<?php

include_once API_LIBRARY_PATH . '/API.php';

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

//  default validation
$validationResults = $utilities->validate();
if ($validationResults['error']) {
    echo json_encode($validationResults);
    return;
}

$userFields = $_POST['fields'];

$db = new APIDatabase();
$auth = Auth::getAuth('current_user');
$userManager = new APIUserManager($db, $utilities);

$response = [
    'results' => null,
    'error' => null,
    'error_message' => null,
];

try {
    $db->connect();
    $users = $userManager->getOnlineUsers($auth['company_id'], $auth['id']);

    $response['status'] = APIStatus::STATUS_SUCCESS;
    $response['results'] = $users;

    //  TODO, search for how to implement finally in php
    $db->disconnect();
} catch (APIException $e) {
    $response['status'] = APIStatus::STATUS_ERROR;
    $response['error'] = $e->cause;
    $response['error_message'] = $e->message;
}

echo json_encode($response);
