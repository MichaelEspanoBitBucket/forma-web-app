<?php

include_once API_LIBRARY_PATH . '/API.php';

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

//  default validation
$validationResults = $utilities->validate();
if ($validationResults['error']) {
    echo json_encode($validationResults);
    return;
}

$response = [
    'status' => APIStatus::STATUS_ERROR,
    'results' => null,
    'error' => null,
    'error_message' => null,
];

//  validate required parameters
if (!array_key_exists('form_id', $_POST)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for form_id parameter';
    echo json_encode($response);
    return;
}
if (!array_key_exists('request_id', $_POST)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for request_id parameter';
    echo json_encode($response);
    return;
}

$formId = $_POST['form_id'];
$requestId = $_POST['request_id'];

$current_user = Auth::getAuth('current_user');

try {
    $database = new APIDatabase();
    $database->connect();
    $requestManager = new APIRequestManager($database, $utilities);

    $actions = $requestManager->getAvailableActions($formId, $requestId, $current_user['id']);

    $database->disconnect();

    $response['status'] = APIStatus::STATUS_SUCCESS;
    $response['results'] = [
        'actions' => $actions
    ];
} catch (APIException $e) {

    $response['status'] = APIStatus::STATUS_ERROR;
    $response['error'] = $e->cause;
    $response['error_message'] = $e->message;
}

echo json_encode($response);
