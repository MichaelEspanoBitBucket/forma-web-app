<?php

//error_reporting(E_ALL);

include_once API_LIBRARY_PATH . '/API.php';

$function = new functions();

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

$search = new Search();

/**/
//  <editor-fold desc="Validation" defaultstate="collapsed">
//  default validation
$validationResults = $utilities->validate();
if ($validationResults['error']) {
    echo json_encode($validationResults);
    return;
}

$response = [
    'status' => APIStatus::STATUS_ERROR,
    'results' => null,
    'error' => null,
    'error_message' => null,
];

//  validate required parameters
if (!array_key_exists('form_id', filter_input_array(INPUT_POST))) {
    $response['error']         = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for form_id parameter';
    echo json_encode($response);
    return;
}

// </editor-fold>

$formId               = filter_input(INPUT_POST, 'form_id');
$userSearchParameters = stripslashes(filter_input(INPUT_POST, 'search_parameters'));

try {

    $auth         = new Auth();
    $current_user = $auth->getAuth('current_user');

    $database       = new APIDatabase();
    $database->connect();
    $requestManager = new APIRequestManager($database, $utilities);

    //  <editor-fold desc="Search Parameters Setup" defaultstate="collapsed">
    $searchParameters = new APISearchParameters($formId, $current_user['id'], $current_user['user_level_id']);
    //$searchParameters = new APISearchParameters($formId, "", "2");

    $userSearchParameters = json_decode($userSearchParameters, true);
    if (!empty($userSearchParameters)) {
        if (!empty($userSearchParameters['range'])) {
            $searchParameters->searchRecordsFrom  = $userSearchParameters['range']['from'];
            $searchParameters->searchRecordsCount = $userSearchParameters['range']['number_of_records'];
        }

        $searchParameters->starred = $userSearchParameters['starred'];

        if (!empty($userSearchParameters['date_created_comparison'])) {
            $searchParameters->dateCreatedComparison = $userSearchParameters['date_created_comparison'];
        }

        if (!empty($userSearchParameters['date_updated_comparison'])) {
            $searchParameters->dateUpdatedComparison = $userSearchParameters['date_updated_comparison'];
        }

        if (filter_var($userSearchParameters['include_requestor_info'], FILTER_VALIDATE_BOOLEAN)) {
            $searchParameters->includeRequestorInfo = $userSearchParameters['include_requestor_info'];
        }

        if (filter_var($userSearchParameters['include_processor_info'], FILTER_VALIDATE_BOOLEAN)) {
            $searchParameters->includeProcessorInfo = $userSearchParameters['include_processor_info'];
        }

        if (!empty($userSearchParameters['extra_conditions_by_fields'])) {
            $searchParameters->extraConditionFields = $userSearchParameters['extra_conditions_by_fields'];
        }

        if (!empty($userSearchParameters['fields'])) {
//            if (is_array($searchParameters['fields'])) {
            $searchParameters->fields = $userSearchParameters['fields'];
//            }
        }

        $searchParameters->validateFields();
    }
    //  </editor-fold>
//    $requestList = $requestManager->getAvailableRequests($searchParameters);
    $requestList         = $requestManager->searchRequests($searchParameters);
    $filteredRequestList = array();
    for ($i = 0; $i < count($requestList); $i ++) {
        if (array_key_exists("ID", $requestList[$i]) && !$requestList[$i]["ID"]) {
            continue;
        }

        //	assign requestor image
        if ($requestList[$i]["requestor_id"]) {
            $encryptID = md5(md5($requestList[$i]["requestor_id"]));
            $path      = $encryptID . "." . "/small_" . $encryptID . "." . $requestList[$i]['requestor_image_extension'];
            $imageURL  = "/images/formalistics/tbuser/" . $path;

            $requestList[$i]["requestor_image_url"] = $imageURL;
        }

        //	assign processor image
        if ($requestList[$i]["processor_id"]) {
            $encryptID = md5(md5($requestList[$i]["requestor_id"]));
            $path      = $encryptID . "." . "/small_" . $encryptID . "." . $requestList[$i]['processor_image_extension'];
            $imageURL  = "/images/formalistics/tbuser/" . $path;

            $requestList[$i]["processor_image_url"] = $imageURL;
        }

        array_push($filteredRequestList, $requestList[$i]);
    }

    $database->disconnect();

    $response['status']                 = APIStatus::STATUS_SUCCESS;
    $response['results']                = $filteredRequestList;
    $response['extra']                  = $searchParameters;
    $response['user_search_parameters'] = $userSearchParameters;
    $response['server_date']            = $function->currentDateTime();
} catch (APIException $e) {
    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = $e->cause;
    $response['error_message'] = $e->message;
    $response['server_date']   = $function->currentDateTime();
}

echo json_encode($response);
//print_r($response);
//error_reporting(0);
