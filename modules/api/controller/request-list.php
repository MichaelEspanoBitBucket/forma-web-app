<?php

include_once API_LIBRARY_PATH . '/API.php';

$function = new functions();

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

//  default validation
$validationResults = $utilities->validate(APIUtilities::NO_LOGIN_REQUIRED);
if ($validationResults['error']) {
    echo json_encode($validationResults);
    return;
}

$response = [
    'status' => APIStatus::STATUS_ERROR,
    'results' => null,
    'error' => null,
    'error_message' => null,
];

//  validate required parameters
if (!array_key_exists('form_id', $_POST)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for form_id parameter';
    echo json_encode($response);
    return;
}

$formId = $_POST['form_id'];
$userSearchParameters = $_POST['search_parameters'];

//$currentUser = Auth::getAuth('current_user');

try {
    $database = new APIDatabase();
    $database->connect();
    $requestManager = new APIRequestManager($database, $utilities);

    $searchParameters = new APISearchParameters($formId, $current_user['id'], $current_user['user_level_id']);
    //$searchParameters = new APISearchParameters($formId, "", "2");
    
    $userSearchParameters = json_decode($userSearchParameters, true);
    if (!empty($userSearchParameters)) {
        if (!empty($userSearchParameters['range'])) {
            $searchParameters->searchRecordsFrom = $userSearchParameters['range']['from'];
            $searchParameters->searchRecordsCount = $userSearchParameters['range']['number_of_records'];
        }

        $searchParameters->starred = $userSearchParameters['starred'];

        if (!empty($userSearchParameters['date_created_comparison'])) {
            $searchParameters->dateCreatedComparison = $userSearchParameters['date_created_comparison'];
        }

        if (!empty($userSearchParameters['date_updated_comparison'])) {
            $searchParameters->dateUpdatedComparison = $userSearchParameters['date_updated_comparison'];
        }

        if (filter_var($userSearchParameters['include_requestor_info'], FILTER_VALIDATE_BOOLEAN)) {
            $searchParameters->includeRequestorInfo = $userSearchParameters['include_requestor_info'];
        }

        if (filter_var($userSearchParameters['include_processor_info'], FILTER_VALIDATE_BOOLEAN)) {
            $searchParameters->includeProcessorInfo = $userSearchParameters['include_processor_info'];
        }

        if (!empty($userSearchParameters['extra_conditions_by_fields'])) {
            $searchParameters->extraConditionFields = $userSearchParameters['extra_conditions_by_fields'];
        }

        if (!empty($userSearchParameters['fields'])) {
//            if (is_array($searchParameters['fields'])) {
            $searchParameters->fields = $userSearchParameters['fields'];
//            }
        }

        $searchParameters->validateFields();
    }
    
//    $requestList = $requestManager->getAvailableRequests($searchParameters);
    $requestList = $requestManager->searchRequests($searchParameters);
    
    $database->disconnect();

    $response['status'] = APIStatus::STATUS_SUCCESS;
    $response['results'] = $requestList;
    $response['extra'] = $searchParameters;
    $response['user_search_parameters'] = $userSearchParameters;
    $response['server_date'] = $function->currentDateTime();
} catch (APIException $e) {

    $response['status'] = APIStatus::STATUS_ERROR;
    $response['error'] = $e->cause;
    $response['error_message'] = $e->message;
    $response['server_date'] = $function->currentDateTime();
}

echo json_encode($response);
