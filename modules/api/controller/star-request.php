<?php

error_reporting(E_ALL);

include_once API_LIBRARY_PATH . '/API.php';

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

//  default validation
$validationResults = $utilities->validate();
if ($validationResults['error']) {
    echo json_encode($validationResults);
    return;
}

$response = [
    'status' => APIStatus::STATUS_ERROR,
    'results' => null,
    'error' => null,
    'error_message' => null,
];

//  validate input
if (!array_key_exists('request_id', $_POST)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for request_id parameter';
    echo json_encode($response);
    return;
}

if (!array_key_exists('form_id', $_POST)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for form_id parameter';
    echo json_encode($response);
    return;
}

$requestId = $_POST['request_id'];
$formId = $_POST['form_id'];

try {

    $auth = new Auth();

    $database = new APIDatabase();
    $database->connect();
    $requestManager = new APIRequestManager($database, $utilities);

    $requestManager->markRequestAsStarred($auth->getAuth('current_user'), $formId, $requestId);
    $response['status'] = APIStatus::STATUS_SUCCESS;
    $response['results'] = [
        'message' => 'Successfully marked document as starred.'
    ];

    //  TODO, search for how to implement finally in php
    $database->disconnect();
} catch (APIException $ex) {
    $response['status'] = APIStatus::STATUS_ERROR;
    $response['error'] = $e->cause;
    $response['error_message'] = $e->message;
}

error_reporting(0);
echo json_encode($response);
