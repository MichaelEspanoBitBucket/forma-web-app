<?php

/*
 * Expected Input - POST
 *  request_data    - array, required
 *  form_id         - string, required
 *  action          - string, required
 */

include_once API_LIBRARY_PATH . '/API.php';

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

//  default validation
$validationResults = $utilities->validate();
if ($validationResults['error']) {
    echo json_encode($validationResults);
    return;
}

$response = [
    'results' => APIStatus::STATUS_ERROR,
    'error' => APIStatus::ERROR_INVALID_PARAMETERS,
    'error_message' => null,
];

//  <editor-fold desc="Input Validation" defaultstate="collapsed">
//  validate that the required parameters are provided
$response['status'] = APIStatus::STATUS_ERROR;
if (!array_key_exists('request_data', $_POST) || $_POST['request_data'] == '') {
    $response['error_message'] = 'Please provide value for request_data.';
    echo json_encode($response);
    return;
} else if (!json_decode($_POST['request_data'])) {
    $response['error_message'] = 'The request data must be a valid JSON object. ' + json_last_error();
    echo json_encode($response);
    return;
}

if (!array_key_exists('form_id', $_POST) || $_POST['form_id'] == '') {
    $response['error_message'] = 'Please provide value for form_id.';
    echo json_encode($response);
    return;
}

if (!array_key_exists('request_id', $_POST) || $_POST['request_id'] == '') {
    $response['error_message'] = 'Please provide value for request_id.';
    echo json_encode($response);
    return;
}

if (!array_key_exists('action', $_POST) || $_POST['action'] == '') {
    $response['error_message'] = 'Please provide value for action.';
    echo json_encode($response);
    return;
}
$response['status'] = null;
//  </editor-fold>

$requestData = $_POST['request_data'];
$formId = $_POST['form_id'];
$requestId = $_POST['request_id'];
$action = $_POST['action'];

$response['error'] = null;
$response['error_message'] = null;

$current_user = Auth::getAuth('current_user');

try {
    $database = new APIDatabase();
    $database->connect();
    $requestManager = new APIRequestManager($database, $utilities);

    $resultData = $requestManager->updateRequest($current_user, $formId, $requestId, $action, $requestData);

    $database->disconnect();

    $response['status'] = APIStatus::STATUS_SUCCESS;
    $response['results'] = $resultData;
} catch (APIException $e) {

    $response['status'] = APIStatus::STATUS_ERROR;
    $response['error'] = $e->cause;
    $response['error_message'] = $e->message;
}

echo json_encode($response);
