<?php

/*
 * Expected Input - POST
 *  user_id     - text, required
 *  fields      - array, optional
 */

include_once API_LIBRARY_PATH . '/API.php';

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

//  default validation
$validationResults = $utilities->validate();
if ($validationResults['error']) {
    echo json_encode($validationResults);
    return;
}

//  validate input
$requestedUserId = $_POST['user_id'];
$requestedFields = $_POST['fields'];

$response = [
    'status' => null,
    'results' => null,
    'error' => null,
    'error_message' => null,
];

//  validate that the required parameters are provided
if (!isset($requestedUserId) || $requestedUserId == '') {
    $response['status'] = APIStatus::STATUS_ERROR;
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide value for user_id.';
} else {
    $db = new APIDatabase();
    $userManager = new APIUserManager($db, $utilities);

    try {
        $db->connect();
        $user = $userManager->getUser($requestedUserId, $requestedFields);

        $response['status'] = APIStatus::STATUS_SUCCESS;
        $response['results'] = $user;

        //  TODO, search for how to implement finally in php
        $db->disconnect();
    } catch (APIException $e) {
        $response['status'] = APIStatus::STATUS_ERROR;
        $response['error'] = $e->cause;
        $response['error_message'] = $e->message;
    }
}

echo json_encode($response);
