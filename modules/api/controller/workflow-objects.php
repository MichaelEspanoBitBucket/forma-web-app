<?php

error_reporting(E_ERROR);

include_once API_LIBRARY_PATH . '/API.php';
$function = new functions();

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

$currentUser = Auth::getAuth('current_user');

//  requires authentication
if (!$currentUser) {
    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = APIStatus::ERROR_UNAUTHORIZED;
    $response['error_message'] = 'You must be logged in to access this service';

    echo json_encode($response);
    return;
}

$response = [
    'status'        => APIStatus::STATUS_ERROR,
    'results'       => null,
    'error'         => null,
    'error_message' => null,
];

$lastDateUpdated = filter_input(INPUT_GET, 'last_date_updated');
$workflowIdList  = json_decode(filter_input(INPUT_GET, 'workflow_id_list'));
$formIdList = json_decode(filter_input(INPUT_GET, 'form_id'));

$workflowIdListClause = join(',', $workflowIdList);
$formIdListClause = join(',', $formIdList);

if ($lastDateUpdated) {
    $dateUpdatedWhereClause = "AND w.date > '{$lastDateUpdated}'";
} else {
    $dateUpdatedWhereClause = "";
}

try {

    $db = new Database();

    // $query = "SELECT 
    //             wobj.id,
    //             workflow_id,
    //             object_id,
    //             form_id,
    //             type_rel AS node_type,
    //             processorType AS processor_type,
    //             processor,
    //             status,
    //             fieldEnabled AS fields_enabled,
    //             fieldRequired AS fields_required,
    //             fieldHiddenValue AS fields_hidden,
    //             buttonStatus AS actions,
    //             date AS last_modified
    //         FROM
    //             tbworkflow_objects wobj
    //                 LEFT JOIN
    //             tbworkflow w ON wobj.workflow_id = w.id
    //         WHERE
    //             w.id IN ({$workflowIdListClause}) {$dateUpdatedWhereClause}";

    //added joshua reyes for workflowsync in mobility ionic
    $query = "SELECT 
                wobj.id,
                workflow_id,
                object_id,
                form_id,
                type_rel AS node_type,
                processorType AS processor_type,
                processor,
                status,
                fieldEnabled AS fields_enabled,
                fieldRequired AS fields_required,
                fieldHiddenValue AS fields_hidden,
                buttonStatus AS actions,
                date AS last_modified
            FROM
                tbworkflow_objects wobj
                    LEFT JOIN
                tbworkflow w ON wobj.workflow_id = w.id
            WHERE
                w.is_active = '1' AND w.form_id IN ({$formIdListClause}) {$dateUpdatedWhereClause}";

    $results = $db->query($query);

    $response['status']  = APIStatus::STATUS_SUCCESS;
    $response['results'] = $results;
} catch (Exception $e) {
    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = $e->cause;
    $response['error_message'] = $e->message;
}

$response['server_date'] = $function->currentDateTime();

echo json_encode($response);

error_reporting(0);
