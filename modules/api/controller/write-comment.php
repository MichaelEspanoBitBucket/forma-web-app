<?php

//error_reporting(E_ALL);

header('Content-Type: application/json');

include_once API_LIBRARY_PATH . '/API.php';

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

//  default validation
$validationResults = $utilities->validate();
if ($validationResults['error']) {
    echo json_encode($validationResults);
    return;
}

$response['error'] = null;
$response['error_message'] = null;

//  validate input
if (!array_key_exists('comment', $_POST)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for the text parameter';
    echo json_encode($response);
    return;
}

if (!array_key_exists('request_id', $_POST)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for the request_id parameter';
    echo json_encode($response);
    return;
}

if (!array_key_exists('form_id', $_POST)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for the form_id parameter';
    echo json_encode($response);
    return;
}

$comment = $_POST['comment'];
$requestId = $_POST['request_id'];
$formId = $_POST['form_id'];

try {

    $auth = new Auth();
    $current_user = $auth->getAuth('current_user');

    $post = new post();
    $comment = $post->createComment($comment, 1, $requestId, $current_user, $formId);
    $response['status'] = APIStatus::STATUS_SUCCESS;

    //  filter the response with the only needed data.
    $response['results'] = [
        'id' => $comment['commentID'],
		'date_created' => $comment['datePosted'],
		
    ];
} catch (Exception $e) {
    $response['status'] = APIStatus::STATUS_ERROR;
    $response['error'] = 'Error saving comment.';
    $response['error_message'] = $e->message;
}

echo json_encode($response);
