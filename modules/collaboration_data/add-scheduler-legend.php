<?php

error_reporting(1);

include_once('collaboration_dependencies.php');

include_once("../../library/collaboration-repository/SchedulerRepository.php");

$auth = Auth::getAuth('current_user');
$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);


if(!$auth){
	http_response_code(401);
	echo "Failed to load data sources. User not logged in.";
}else{

	$auth = Auth::getAuth('current_user');
	$dbRep = new SchedulerRepository($conn, $auth);

	if(isset($_POST["Legend"])){
		$Legend = $_POST["Legend"];
		$id = $dbRep->addLegend($Legend);
		$Legend["id"] = $id;
		//var_dump($_POST["Legend"]);
		header('Cache-Control: no-cache, must-revalidate');
		header('Content-type: application/json');
		$arr = array('id' => $Legend);
		$dbPrefJson = json_encode($Legend);
		echo $dbPrefJson;
	 }else{
	 
	 	echo "No parameters are given";
	 }
	

}

?>
