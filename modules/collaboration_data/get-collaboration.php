<?php 
error_reporting(0);

include_once('collaboration_dependencies.php');

include_once("../../library/collaboration-repository/CollaborationRepository.php");

$auth = Auth::getAuth('current_user');
$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);

if(!$auth){

	http_response_code(401);
	echo "Failed to load data sources. User not logged in.";
}else{
	
	$dbRep = new CollaborationRepository($conn, $auth);
	header('Cache-Control: no-cache, must-revalidate');
	header('Content-type: application/json');

	



	
	if(isset($_POST["collabID"])){
		$dbPref = $dbRep->getCollaboration($_POST["collabID"]);
		if($dbPref==null){
			
		}
		$current_user = array('id' => $auth["id"],
			'display_name' => $auth["display_name"],
			'companyId' => $auth["company_id"],
			'displayName' => $auth["display_name"],
			'firstName' => $auth["first_name"],
			'lastName' => $auth["last_name"],
			'user_level_id' => $auth["user_level_id"]
			);
		$dbPref["current_user"] = $current_user;
		$dbPref["isCollaborator"] = $dbRep->isCollaborator($_POST["collabID"], $auth["id"]); 
		$dbPref["SOCKET_ACTIVE"] = SOCKET_ACTIVE;
		$dbPref["SOCKET_SERVER"] = SOCKET_SERVER;
		


		// echo SOCKET_ACTIVE;
		// echo SOCKET_SERVER;
	 //    echo $auth['id'];    
	 //    echo $auth['company_id'];      
	 //    echo $auth['display_name'];
	 //    echo $auth['first_name'];
	 //    echo $auth['last_name'];
	 //    echo $auth['user_level_id'];
	 //    echo $current_page = 'http://' . $_SERVER['HTTP_HOST'] . '/user_view/' . $auth['last_name'] . "_" . $auth['first_name']; 
	 //    // echo $companyUserImg;




		$dbPrefJson = json_encode($dbPref);
		echo $dbPrefJson;
	}else{
		echo "Parameters are not supplied.";
	}
	
}
?>