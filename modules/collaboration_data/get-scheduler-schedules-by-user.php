<?php 
error_reporting(0);

include_once('collaboration_dependencies.php');

include_once("../../library/collaboration-repository/SchedulerRepository.php");

$auth = Auth::getAuth('current_user');
$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);

if(!$auth){

	http_response_code(401);
	echo "Failed to load data sources. User not logged in.";
}else{
	
	$dbRep = new SchedulerRepository($conn, $auth);
	header('Cache-Control: no-cache, must-revalidate');
	header('Content-type: application/json');

	
	
	if(isset($_POST["user_id"])){
		$dbPref = $dbRep->getSchedulesByUser($_POST["user_id"], $_POST["from"], $_POST["to"]);
		$dbPrefJson = json_encode($dbPref);
		echo $dbPrefJson;
	}else{
		echo "Parameters are not supplied.";
	}
	
}
?>