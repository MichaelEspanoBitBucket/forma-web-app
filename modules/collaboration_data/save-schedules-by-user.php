<?php

error_reporting(0);

include_once('collaboration_dependencies.php');

include_once("../../library/collaboration-repository/SchedulerRepository.php");

$auth = Auth::getAuth('current_user');
$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);


if(!$auth){
	http_response_code(401);
	echo "Failed to load data sources. User not logged in.";
}else{

	$auth = Auth::getAuth('current_user');
	$dbRep = new SchedulerRepository($conn, $auth);
	//var_dump($_POST["Collab"]);
		header('Cache-Control: no-cache, must-revalidate');
	header('Content-type: application/json');

	


	if(isset($_POST["User"])){
		$User = $_POST["User"];


		$newSchedid = $dbRep->saveUserSchedules($User["collaboration_id"], $User["user_id"],$User["schedules"]);
	
		//$dbPref = array('id' => $newSchedid, );
		$dbPrefJson = json_encode($newSchedid);
		echo $dbPrefJson;
	 }else{
	 
	 	echo "No parameters are given";
	 }
	

}

?>
