<?php
/*code here*/
/* Redirect user if they already logged */
if(Auth::hasAuth('current_user')){
    //$this->view->current_user = Auth::getAuth('current_user');

}
else{
    header('location: /');
}
$auth = Auth::getAuth('current_user');

require_once(realpath('.') . "/library/dashboard-repositories/DashboardRepository.php");

$userCompany = new userQueries();
$company = $userCompany->getCompany($auth['company_id']);
include(dirname(__FILE__) . "/../" . "/view/index.phtml");