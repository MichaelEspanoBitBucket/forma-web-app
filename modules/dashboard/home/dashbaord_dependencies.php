<?php
	$path = $_SERVER["DOCUMENT_ROOT"];
	include_once($path .'/config/files_config.php');
	include_once($path .'/config/database_url_config.php');


	include_once($path .'/library/library.php');
	include_once($path .'/library/functions.php');
	include_once($path .'/library/Database.php');
	include_once($path .'/library/Auth.php');

	include_once($path .'/library/upload.php');
	include_once($path .'/library/userQueries.php');
	include_once($path .'/library/post.php');
	include_once($path .'/library/Redis_Formalistics.php');
	include_once($path .'/library/Formalistics.php');
	include_once($path .'/library/Person.php');
	include_once($path .'/config/config.php');

?>
