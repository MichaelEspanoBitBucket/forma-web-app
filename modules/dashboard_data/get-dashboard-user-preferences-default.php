<?php 

error_reporting(0);

include_once('dashbaord_dependencies.php');

include_once("../../library/dashboard-repositories/DashboardRepository.php");

include_once("../../library/dashboard-repositories/DashboardTemplateDAO.php");

$auth = Auth::getAuth('current_user');
$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
$userCompany = new userQueries();
$company = $userCompany->getCompany($auth['company_id']);

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');


if(!$auth){

	http_response_code(401);
	echo "Failed to load data sources. User not logged in.";
	
}else{
	
		$auth = Auth::getAuth('current_user');
		$dbPref = new DashboardTemplateDAO($conn, $auth);
		
		if (isset($_GET['group_id'])){
			$result["groupTemplates"] = $dbPref->getAllTemplatesByGroupId($_GET['group_id']);
		}

		if (isset($_GET['user_id'])){
			$result["userTemplates"] = $dbPref->getAllTemplatesByUserId($_GET['user_id']);
		}

		if(!isset($_GET['group_id']) && !isset($_GET['user_id'])){
			
			$result = $dbPref->getAllTemplates();


		}

		echo json_encode($result);

	
}
?>