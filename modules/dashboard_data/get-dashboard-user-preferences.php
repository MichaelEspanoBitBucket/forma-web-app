<?php 

error_reporting(1);

include_once('dashbaord_dependencies.php');

require_once("../../library/dashboard-repositories/DashboardRepository.php");


$auth = Auth::getAuth('current_user');
$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
$userCompany = new userQueries();
$company = $userCompany->getCompany($auth['company_id']);


if(!$auth){

	http_response_code(401);
	echo "Failed to load data sources. User not logged in.";
	
}else{
	
	$dbRep = new DashboardRepository($conn, $auth);
	header('Cache-Control: no-cache, must-revalidate');
	header('Content-type: application/json');

	if ($auth['user_level_id'] == "1" || $auth['user_level_id'] == "2") {
       
        $level = "admin";
    } else if ($auth['user_level_id'] == "3") {
      
        $level = "ordinary";
    } else {
      
        $level = "guest";
    }

	$dbPref = $dbRep->getUserPreferences();
	$dbPref["Current_User"]['id'] = $auth['id'];
	$dbPref["Current_User"]['display_name'] = $auth['display_name'];
	$dbPref["Current_User"]['level'] = $level;
	$dbPref["Current_User"]['user_level_id'] = $auth['user_level_id'];	
	
	$encrypted_id = md5(md5($auth['id']));
    $path = $encrypted_id . "." . "/small_" . $encrypted_id . "." . ($auth["extension"]==""?"png":$auth["extension"]);
    $image_url = "/images/formalistics/tbuser/" . $path;
    $dbPref["Current_User"]['image_url'] = $image_url;	 	

    //echo json_encode($auth);

    $dbPref["CompanyMod"] = unserialize(ENABLE_COMPANY_MOD);
    $dbPref["Properties"] = unserialize(PARENT_MODULE_PROPERTIES);
    $dbPref["CompanyInfo"] = $company;
	$dbPrefJson = json_encode($dbPref);
	echo $dbPrefJson;
}
?>