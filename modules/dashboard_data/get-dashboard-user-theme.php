<?php 

error_reporting(0);

include_once('dashbaord_dependencies.php');
// include_once('../../library/upload.php');
// include_once('../../library/userQueries.php');
// include_once('../../library/post.php');
// include_once('../../library/notifications.php');
// include_once('../../library/ListViewFormula.php');
include_once('../../library/Search.php');
include_once('../../library/DatabaseConnection.php');
include_once('../../library/MySQLDatabase.php');
if(!Auth::hasAuth('current_user')){ 
    return false;
}

$db = new Database();
$fn = new functions();
$auth = Auth::getAuth('current_user');
$redis_cache = getRedisConnection();
$user_id = $auth['id'];

$themeStyle = array("light_color" => "#7f8c8d", "dark_color" => "#667273");


//if(isset($_COOKIE['application'])){
header('Cache-Control: no-cache, must-revalidate');
    header('Content-type: application/json');
$application = $_COOKIE['application'];

if ($redis_cache) {

    $cachedFormat = "$user_id";

    $cache_theme = json_decode($redis_cache->get("user_theme"), true);

    $cached_query_result = $cache_theme ['' . $cachedFormat . ''];

    if ($cached_query_result) {
        // echo "from cache";

        $theme = $cached_query_result;
    } else {
        // echo "from db";

        $theme = $fn->getDashboardTheme($application);

        $cache_theme ['' . $cachedFormat . ''] = $theme;
        $redis_cache->set("user_theme", json_encode($cache_theme));
    }
} else {
    $theme = $fn->getDashboardTheme($application);
}
	 echo json_encode($theme);

?>