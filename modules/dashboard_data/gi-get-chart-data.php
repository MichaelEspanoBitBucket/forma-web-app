<?php
error_reporting(0);
include_once('dashbaord_dependencies.php');

/* Redirect if not authenticated */
if(!Auth::hasAuth('current_user')){
	http_response_code(401);
	echo "Failed to load chart data. User not logged in.";
} 

$auth = Auth::getAuth('current_user');
$path = $_SERVER["DOCUMENT_ROOT"];
include_once($path . "/library/gi-repositories/DataWarehouse.php");

$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
$dwRepo = new DataWarehouse($conn, $auth);

$chartData = null;
if(isset($_GET["id"])){
	
}else if(isset($_POST["query"])){
	$startTime = microtime(true);
	$data = $dwRepo->getChartData($_POST["query"]);
	$endTime = microtime(true);
	$data["exec time"] = $endTime - $startTime;
	echo json_encode($data);
}

?>
