<?php
//check if session is alive if not.. retun false;
if(!Auth::hasAuth('current_user')){ 
    return false;
}

$auth = Auth::getAuth('current_user');
$db = new Database();
$fs = new functions();
$company_id = $auth['company_id'];
$userID = $auth['id'];
$timezone = "Asia/Manila";
if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);

    if(isset($_POST['action'])){
        $action = $_POST['action'];
        $date = $fs->currentDateTime();
        
        if($action=="loadLinkMaintenance"){
        	$ctr = 0;
            $getLinkMaintenance = $db->query("SELECT menu.* FROM tblink_maintenance maintentance LEFT JOIN tblink_menu menu ON maintentance.id = menu.link_maintenance_id WHERE maintentance.company_id = '". $company_id  ."'","array");
            
            if(count($getLinkMaintenance)==0){
                echo "<div class='noti-fly-text' style='color: rgb(66, 66, 66); text-align: center; display: inherit; padding: 16px; background-color: #f4f4f4; box-sizing: border-box;border-radius: 6px;'>No Link Found</div>";
            }else{
            	foreach ($getLinkMaintenance as $key => $value) {
            		if($value['title']){
            			$ctr++;
			            ?>
			                <a href="<?= $value['url']; ?>" target="_blank">
			                    <div class="fl-dropflyout-wrapper">
			                        <div class="fl-dropflyout-content-wrapper" style="padding:0px;">
			                            <div class="fl-dropfyout-img-content fl-table-ellip" title="<?= $value['url']; ?>"><span style="font-size:12px;"><img src="http://www.google.com/s2/favicons?domain_url=<?= $value['url']; ?>" style="  float: left; margin-right: 4px;"> <?= $value['title']; ?> </span></div>
			                        </div>
			                    </div> 
			                </a>
			            <?php
			        }
	            }
	            if($ctr==0){
	            	?>
	            	<div class='noti-fly-text' style='color: rgb(66, 66, 66); text-align: center; display: inherit; padding: 16px; background-color: #f4f4f4; box-sizing: border-box;border-radius: 6px;'>No Link Found</div>
	            	<?php
	            }
            }
            
        }else if($action=="saveLinkMaintenance"){
        	$getLinkMaintenance = $db->query("SELECT * FROM tblink_maintenance WHERE company_id = '". $company_id ."'","array");

        	if(count($getLinkMaintenance)==0){
        		//insert
        		//save link maintenance
	        	$arr_link_maintenance = array("company_id"=>$company_id,
	        		"date_created"=>$date,
	        		"created_by"=>$userID,
	        		"is_active"=>1);
	        	$new_id = $db->insert("tblink_maintenance",$arr_link_maintenance);


	        	//save link menu
	        	$linkArray = json_decode($_POST['linkArray'],true);
	            foreach ($linkArray as $key => $value) {
	            	$arrLinkMenu = array("link_maintenance_id"=>$new_id,
	            		"title"=>$value['link-value'],
	            		"url"=>$value['link-url'],
	            		"is_active"=>1);
	            	$db->insert("tblink_menu",$arrLinkMenu);
	            }
        	}else{
        		//update
        		$link_maintenance_id = $getLinkMaintenance[0]['id'];

        		//update link maintenance
        		$arr_link_maintenance = array("date_updated"=>$date,
	        		"updated_by"=>$userID);
	        	$db->update("tblink_maintenance",$arr_link_maintenance,array("id"=>$link_maintenance_id));

	        	//delete old
	        	$db->delete("tblink_menu",array("link_maintenance_id"=>$link_maintenance_id));

	        	//insert new
	        	$linkArray = json_decode($_POST['linkArray'],true);
	            foreach ($linkArray as $key => $value) {
	            	$arrLinkMenu = array("link_maintenance_id"=>$link_maintenance_id,
	            		"title"=>$value['link-value'],
	            		"url"=>$value['link-url'],
	            		"is_active"=>1);
	            	$db->insert("tblink_menu",$arrLinkMenu);
	            }
        	}
        	
        }
    }


?>