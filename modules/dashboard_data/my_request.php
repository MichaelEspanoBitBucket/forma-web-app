<?php
error_reporting(0);
//check if session is alive if not.. retun false;
include_once('dashbaord_dependencies.php');
require_once("../../library/Search.php");
class MyRequest extends Database {

    public function setQuery($form_tablename, $id, $search_value, $others) {
        $auth = Auth::getAuth('current_user');
        // $db = new Database;
        $userID = $auth['id'];
        $search_value = $this->addslash_escape(mysql_escape_string($search_value));
        $query_str = "(SELECT $id as form_id, form.id as request_id, form.DateCreated as DateCreated, form.TrackNo as TrackNo,form.id as request_id, u.first_name,u.last_name,u.middle_name, form.Status as Status,
                    form.Requestor as requestor_id, form.Processor as processors_id, tbw.form_name, tbw.id as form_id , COALESCE(tbfc.category_name,'Others') as category_name
                    FROM `$form_tablename` form LEFT JOIN tbuser u ON u.id=form.requestor, tb_workspace tbw LEFT JOIN tbform_category tbfc ON tbw.category_id = tbfc.id
                    WHERE form.requestor = '$userID' AND tbw.is_delete = 0 AND tbw.is_active = 1 AND  tbw.id = $id AND (tbw.form_name LIKE '%" . $search_value . "%' OR form.TrackNo LIKE '%" . $search_value . "%' OR form.Status LIKE '%" . $search_value . "%' OR form.DateCreated LIKE '%" . $search_value . "%') 
                     AND (NOT EXISTS(SELECT * FROM tbtrash_bin WHERE record_id = form.id 
                                AND form_id = $id 
                                AND table_name='" . $form_tablename . "'))
                     $others) ";
        return $query_str;
    }

    // public function countMyRequest(){
    //   $search = new Search();
    //   $ctr = 0;
    //   $forms_array = $search->getCompaniesForm();
    //   foreach ($forms_array as $forms) {
    //     $id =  $forms['id'];
    //     $request_array = $search->countOwnRequest($id);
    //     $ctr+=$request_array;
    //   }
    //   return $ctr;
    // }
    public function countMyRequest() {
        //$search = new Search();
        //$ctr = 0;
        //$unionQuery = "";
        //$forms_array = $search->getCompaniesForm("AND is_active = 1");
        //$search_value = "";
        //foreach ($forms_array as $forms) {
        //  $id =  $forms['id'];
        //  $form_tablename = $forms['form_table_name'];
        //  $unionQuery .= $this->setQuery($form_tablename,$id,$search_value);
        //  if($ctr<count($forms_array)-1){
        //    $unionQuery.=" UNION ALL ";
        //  }
        //  $ctr++;
        //}
        //// echo $unionQuery;
        //$countTotal = $this->query($unionQuery,"numrows");
        //// return $ctr;
        //return $countTotal;
        return "0";
    }

    public function getMyRequest($post_start, $post_end, $column_sort, $column_sort_type, $search_value, $others, $view_type) {
        $auth = Auth::getAuth('current_user');
        $redis_cache = false;//getRedisConnection();
        $search = new Search();
        $userID = $auth['id'];
        $returnJson = array();
        $countTotal = 0;
        if (MY_REQUEST_CUSTOM_VIEW == "1") {
            $other_condition = " and my_request_visible = '1'"; //my request visibility
        }

        $forms_array = $search->getCompaniesForm("AND is_active = 1" . $other_condition); //my request visibility

        $countRecord = 0;
        $unionQuery = "";
        $ctr = 0;
        $limit = "";
        // $countTotal = MyRequest::countMyRequest();
        foreach ($forms_array as $forms) {
            $id = $forms['id'];
            $form_tablename = $forms['form_table_name'];
            $sqlStr = "SELECT count(*) as count FROM information_schema.tables WHERE table_schema = '" . DB_NAME . "' AND table_name = '" . $form_tablename . "'";
            $query = $this->query($sqlStr, "row");

            if ($query['count'] > 0) {
                $unionQuery.=$this->setQuery($form_tablename, $id, $search_value, $others);
                // if($ctr<count($forms_array)-1){
                $unionQuery.=" UNION ALL ";
                // } 
            }
            // $ctr++;
        }
        $end = "10";
        if ($_POST['limit'] != "") {
            $end = $post_end;
        }
        if ($post_start != "") {
            $limit = " LIMIT $post_start, $end";
        }
        $orderBy = " ORDER BY DateCreated DESC";
        if ($_POST['column-sort']) {
            $orderBy = " ORDER BY " . $column_sort . " " . $column_sort_type;
        }
        $unionQuery = substr($unionQuery, 0, strlen($unionQuery) - 11);


        if ($view_type == "2") { // for page only
            if ($redis_cache) {
                /*
                  Cache Format
                  1. User Id.
                  2. Type
                 */

                $cachedFormat = "$userID::$search_value";

                // $memcache->delete("my_request_list_page_count");
                $cache_my_request_count = json_decode($redis_cache->get("my_request_list_page_count"), true);

                $cached_query_result = $cache_my_request_count['' . $cachedFormat . ''];

                if ($cached_query_result) {
                    // echo "from cache";

                    $countTotal = $cached_query_result;
                } else {
                    // echo "from db";

                    $countTotal = $this->query($unionQuery, "numrows");


                    $cache_my_request_count['' . $cachedFormat . ''] = $countTotal;
                    $redis_cache->set("my_request_list_page_count", json_encode($cache_my_request_count));
                }
            } else {
                $countTotal = $this->query($unionQuery, "numrows");
            }
        }
        $unionQuery = $unionQuery . $orderBy . $limit;

        // echo $unionQuery;

        $return = $this->query($unionQuery, "array");


        return array("request" => $return, "count" => $countTotal);
    }

}

if (!Auth::hasAuth('current_user')) {
    return false;
}

$auth = Auth::getAuth('current_user');

$db = new Database;
$fs = new functions;
$search = new Search();
$myrequest = new MyRequest();
$userID = $auth['id'];
$redis_cache = getRedisConnection();

if (isset($_POST['action'])) {

    $date = $fs->currentDateTime();

    switch ($_POST['action']) {

        case "loadData":
            $returnJson = array();
            $search_value = $_POST['search_value'];
            $start = $_POST['start'];
 
            if ($redis_cache) {

                /*
                  Cache Format
                  1. User Id.
                  2. Type
                 */

                $cachedFormat = "$userID";

                // $memcache->delete("my_request_list_widget");
                $cache_my_request_list = json_decode($redis_cache->get("my_request_list_widget"), true);

                $cached_query_result = $cache_my_request_list['' . $cachedFormat . ''];

                if ($cached_query_result) {
                    // echo "from cache";

                    $return = $cached_query_result;
                } else {
                    // echo "from db";

                    $result = $myrequest->getMyRequest($start, "", "", "", $search_value, " ORDER BY form.id DESC", "1");
                    $return = $result['request'];


                    $cache_my_request_list['' . $cachedFormat . ''] = $return;
                    $redis_cache->set("my_request_list_widget", json_encode($cache_my_request_list));
                }
            } else {
                $result = $myrequest->getMyRequest($start, "", "", "", $search_value, " ORDER BY form.id DESC", "1");
                $return = $result['request'];
            }

            foreach ($return as $request) {
                $request['user_image'] = post::avatarPic("tbuser", $request['requestor_id'], "30", "30", "small", "avatar");
                array_push($returnJson, $request);
            }


            echo json_encode($returnJson);
            // echo count($returnJson);
            break;
        case "loadDataDatatable":

            $returnJson = array();
            $search_value = $_POST['search_value'];
            $start = $_POST['iDisplayStart'];
            $end = $_POST['limit'];
            $column_sort = $_POST['column-sort'];
            $column_sort_type = $_POST['column-sort-type'];


            if ($redis_cache) {

                /*
                  Cache Format
                  1. User Id.
                  2. Page Number.
                  3. Page Limit.
                  4. Search Filter.
                  5. Column Sort.
                  6. Type of Sort.
                 */

                $cachedFormat = "$userID::$start::$end::$search_value::$column_sort::$column_sort_type";

                // $memcache->delete("my_request_list_page");
                $cache_my_request_list = json_decode($redis_cache->get("my_request_list_page"), true);

                $cached_query_result = $cache_my_request_list['' . $cachedFormat . ''];

                if ($cached_query_result) {
                    // echo "from cache";

                    $result = $cached_query_result;
                } else {
                    // echo "from db";

                    $result = $myrequest->getMyRequest($start, $end, $column_sort, $column_sort_type, $search_value, " ORDER BY form.id DESC", "2");


                    $cache_my_request_list['' . $cachedFormat . ''] = $result;
                    $redis_cache->set("my_request_list_page", json_encode($cache_my_request_list));
                }
            } else {
                $result = $myrequest->getMyRequest($start, $end, $column_sort, $column_sort_type, $search_value, " ORDER BY form.id DESC", "2");
            }
            $return = $result['request'];
            $output = array(
                "sEcho" => intval($_POST['sEcho']),
                "iTotalRecords" => $result["count"],
                "iTotalDisplayRecords" => $result["count"],
                "start" => $start,
                "aaData" => array(),
                "column-sort" => $column_sort,
                "column-sort-type" => $column_sort_type,
            );

            foreach ($return as $request) {
                $aaData = array();
                $request['user_image'] = post::avatarPic("tbuser", $request['requestor_id'], "30", "30", "small", "avatar");
                $button = '<a class="linkTrigger display" href="/user_view/workspace?view_type=request&formID=' . $request['form_id'] . '&requestID=' . $request['request_id'] . '&trackNo=' . $request['TrackNo'] . '"></a>';
                array_push($aaData, $request['form_name'] . " " . $button);
                // array_push($returnJson, $request);
                //array_push($aaData, $request['form_name']);
                array_push($aaData, $request['TrackNo']);
                array_push($aaData, $request['Status']);
                array_push($aaData, $request['DateCreated']);
                $returnJson[] = $aaData;
            }

            $output['aaData'] = $returnJson;
            echo json_encode($output);
            break;
        case 'countData':
            echo $myrequest->countMyRequest();
            break;

        case 'calendar_view':
            // $arr = array(array("title"=>"sample","start"=>"2014-09-10 16:44:49"),array("title"=>"sample","start"=>"2014-09-09"),array("title"=>"sample","start"=>"2014-09-10"),array("title"=>"sample","start"=>"2014-09-11"));
            // echo json_encode($arr);
            $start_date = $_POST['start_date'];
            $end_date = $_POST['end_date'];

            // if(MY_REQUEST_CUSTOM_VIEW=="1"){
            //   $other_condition = " and my_request_visible = '1'"; //my request visibility
            //  }
            // $forms_array = $search->getCompaniesForm("AND is_active = 1".$other_condition); //my request visibility
            // foreach ($forms_array as $forms) {
            //   $id =  $forms['id'];
            //   $form_tablename = $forms['form_table_name'];
            //   $sqlStr = "SELECT count(*) as count FROM information_schema.tables WHERE table_schema = '". DB_NAME ."' AND table_name = '". $form_tablename ."'";
            //   $query = $this->query($sqlStr,"row");
            //   if($query['count']>0){
            //     $unionQuery.=$myrequest->setQuery($form_tablename,$id,$search_value," AND (form.DateCreated between '$start_date' AND '$end_date') LIMIT 0,10 ");
            //     $unionQuery.=" UNION ALL ";
            //   }
            // }
            // $unionQuery = substr($unionQuery, 0,strlen($unionQuery)-11);
            // // echo $unionQuery;
            // $getRequest = $db->query($unionQuery,"array");

            if ($redis_cache) {

                /*
                  Cache Format
                  1. User Id.
                  2. Start Date.
                  3. End Date.
                 */

                $cachedFormat = "$userID::$start_date::$end_date";

                // $memcache->delete("my_request_calendar");
                $cache_my_request_calendar = json_decode($redis_cache->get("my_request_calendar"), true);

                $cached_query_result = $cache_my_request_calendar['' . $cachedFormat . ''];

                if ($cached_query_result) {
                    // echo "from cache";

                    $result = $cached_query_result;
                } else {
                    // echo "from db";

                    $result = $myrequest->getMyRequest("", "", "", "", "", " AND (form.DateCreated between '$start_date' AND '$end_date') LIMIT 0,10 ", "3");


                    $cache_my_request_calendar['' . $cachedFormat . ''] = $result;
                    $redis_cache->set("my_request_calendar", json_encode($cache_my_request_calendar));
                }
            } else {
                $result = $myrequest->getMyRequest("", "", "", "", "", " AND (form.DateCreated between '$start_date' AND '$end_date') LIMIT 0,10 ", "3");
            }

            $getRequest = $result['request'];


            $return_array = array();

            foreach ($getRequest as $key => $value) {
                // array_push(array("title"=>$value['TrackNo'],"start"=>$value['DateCreated']), $return_array);
                $return_array[] = array("title" => $value['TrackNo'], "start" => $value['DateCreated'], "link" => "/user_view/workspace?view_type=request&formID=" . $value['form_id'] . "&requestID=" . $value['request_id'] . "&trackNo=" . $value['TrackNo'] . "");
            }

            echo json_encode($return_array);
            break;
    }
}



?>