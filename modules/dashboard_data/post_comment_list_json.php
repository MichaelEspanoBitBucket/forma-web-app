<?php

error_reporting(E_ERROR);
define("API_LIBRARY_PATH","../../APILibraries/");
include_once('dashbaord_dependencies.php');
// include_once '../../library/portal/PortalDataReaderFacade.php';
// include_once '../../library/APILibraries/APIUtilities.php';

include_once API_LIBRARY_PATH . 'API.php';
include '../../library/portal/PortalPostCommentsDAO.php';

$current_user = (new Auth())->getAuth("current_user");
$post_id      = filter_input(INPUT_GET, "post_id");
$from_index   = filter_input(INPUT_GET, "from_index");
$fetch_count  = filter_input(INPUT_GET, "fetch_count");

try {
    $database          = new APIDatabase();
    $database->connect();
    $post_comments_dao = new PortalPostCommentsDAO($database);

    $comment_count                    = $post_comments_dao->getPostCommentCount($post_id);
    $post_comments                    = $post_comments_dao->getPostComments($post_id, $current_user['id'], $from_index, $fetch_count);
    $post_comments_with_author_images = array();

    foreach ($post_comments AS $comment) {
        if ($comment["author_image_extension"] == null || $comment["author_image_extension"] == "") {
            $image_url = "";
        } else {
            $encrypted_id = md5(md5($comment['author_id']));
            $path         = $encrypted_id . "." . "/small_" . $encrypted_id . "." . $comment["author_image_extension"];
            $image_url    = "/images/formalistics/tbuser/" . $path;
        }

        $comment["author_image_url"] = $image_url;
        unset($comment["author_image_extension"]);

        array_push($post_comments_with_author_images, $comment);
    }
header('Cache-Control: no-cache, must-revalidate');
    header('Content-type: application/json');
    echo json_encode($post_comments_with_author_images);

    // $this->current_user          = $current_user;
    // $this->post_id               = $post_id;
    // $this->current_fetched_index = $from_index;
    // $this->current_fetch_count   = $fetch_count;
    // $this->post_comment_count    = $comment_count;
    // $this->post_comments         = $post_comments_with_author_images;

    $database->disconnect();
} catch (Exception $e) {
    $this->error         = $e->getCode();
    $this->error_message = $e->getMessage();
}

include(dirname(__FILE__) . "/../" . "/view/post_comment_list.phtml");

error_reporting(0);
