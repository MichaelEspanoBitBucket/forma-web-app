<?php

error_reporting(1);
define("API_LIBRARY_PATH","../../APILibraries/");
include_once('dashbaord_dependencies.php');
include_once '../../library/portal/PortalDataReaderFacade.php';
include_once '../../library/APILibraries/APIUtilities.php';


$filter_post_type = filter_input(INPUT_POST, "filter_post_type");
$from_index = filter_input(INPUT_POST, "from_index");
$fetch_count = filter_input(INPUT_POST, "fetch_count");
$fetch_type = filter_input(INPUT_POST, "fetch_type");
$from_date = filter_input(INPUT_POST, "from_date");

if($fetch_type==null){
    $fetch_type="Previous";
}



$current_user = (new Auth())->getAuth("current_user");
$data_reader_facade = new PortalDataReaderFacade();

if (!$current_user) {
    echo "Session lost, please login again";
    return;
}

//back up
// $posts_with_total_count = $data_reader_facade->getPostsWithTotalCount(
//         $current_user, $filter_post_type, $from_index, $fetch_count
// );
// $total_post_count       = $posts_with_total_count["total_post_count"];
// $posts                  = $posts_with_total_count["posts"];
//with memcache
$redis_cache = false;//getRedisConnection();
if ($redis_cache) {

    $auth = Auth::getAuth('current_user');
    $user_id = $auth['id'];
    $cachedFormat = "$user_id::$from_index::$filter_post_type";

    // $memcache->delete("post_list");
    $cache_post_list = json_decode($redis_cache->get("post_list"), true);
    $cache_post_list_count = json_decode($redis_cache->get("post_list_count"), true);

    $cached_query_result = $cache_post_list ['' . $cachedFormat . ''];
    $cached_query_result_count = $cache_post_list_count ['' . $cachedFormat . ''];

    if ($cached_query_result) {
        // echo "from cache";

        $total_post_count = $cached_query_result_count;

        $posts = $cached_query_result;
    } else {
        // echo "from db";

        $posts_with_total_count = $data_reader_facade->getPostsWithTotalCount($current_user, $filter_post_type, $from_index, $fetch_count
        );

        $posts = $posts_with_total_count["posts"];
        $total_post_count = $posts_with_total_count["total_post_count"];


        $cache_post_list ['' . $cachedFormat . ''] = $posts;
        $cache_post_list_count ['' . $cachedFormat . ''] = $total_post_count;

        $redis_cache->set("post_list", json_encode($cache_post_list));
        $redis_cache->set("post_list_count", json_encode($cache_post_list_count));
    }
} else {
    if($fetch_type=="Previous"){
        $posts_with_total_count = $data_reader_facade->getPostsWithTotalCount($current_user, $filter_post_type, $from_index, $fetch_count
        );
        $total_post_count = $posts_with_total_count["total_post_count"];
        $posts = $posts_with_total_count["posts"];
    }else{
        $posts_with_total_count = $data_reader_facade->getRecentPostsWithTotalCount($current_user, $filter_post_type, $from_date
        );
        $total_post_count = $posts_with_total_count["total_post_count"];
        $posts = $posts_with_total_count["posts"];

    }
    


     
    
}

$posts_with_author_images = array();


$replace_src = '<img[^\>]*>'; //regex for image lazy load
foreach ($posts AS $post) {
    if ($post["author_image_extension"] == null || $post["author_image_extension"] == "") {
        $image_url = "";
    } else {
        $encrypted_id = md5(md5($post['author_id']));
        $path = $encrypted_id . "." . "/small_" . $encrypted_id . "." . $post["author_image_extension"];
        $image_url = "/images/formalistics/tbuser/" . $path;
    }

    $post["author_image_url"] = $image_url;
    unset($post["author_image_extension"]);

        // $post_str = preg_replace_callback($replace_src, function($a) {
        //         $temp = str_replace("src=\"", "src=\"", $a[0]);
        //         return $temp;
        //     }, $post['announcement']);

        // $survey_question_str = preg_replace_callback($replace_src, function($a) {
        //     $temp = str_replace("src=\"", "src=\"", $a[0]);
        //     return $temp;
        // }, $post['survey_question']);

    // $post_str = preg_replace_callback($replace_src, function($a) {
    //     $temp = str_replace("src=\"", "src=\"". substr(MAIN_PAGE, 0,strlen(MAIN_PAGE)-1) , $a[0]);
    //     return $temp;
    // }, $post['announcement']);


    // $survey_question_str = preg_replace_callback($replace_src, function($a) {
    //     $temp = str_replace("src=\"", "data-src=\"".substr(MAIN_PAGE, 0,strlen(MAIN_PAGE)-1),$a[0]);
    //     return $temp;
    // }, $post['survey_question']);

    
    if($post["type"]=="1"){
       // preg_match_all('/<img[^>]+>/i',$post['announcement'], $images);
        //preg_match_all('/(alt|title|src)=("[^"]*")/i',$post['announcement'], $images);
        $xpath = new DOMXPath(@DOMDocument::loadHTML($post['announcement']));
        $src = $xpath->evaluate("string(//img/@src)");
        $nodelist = $xpath->query("//img"); // find your image

        foreach ($nodelist as $key => $value) {
            # code...

            $post["images"][] = $value->attributes->getNamedItem('src')->nodeValue;
        }
       // $node = $nodelist->item(0); // gets the 1st image
        

        //$post["images"] = $node->attributes->getNamedItem('src')->nodeValue;

    }else{
      // preg_match_all('/<img[^>]+>/i',$post['survey_question'], $images);
       //preg_match_all('/(alt|title|src)=("[^"]*")/i',$post['survey_question'], $images);
        $xpath = new DOMXPath(@DOMDocument::loadHTML($post['survey_question']));
        $src = $xpath->evaluate("string(//img/@src)");
        $nodelist = $xpath->query("//img"); // find your image

        foreach ($nodelist as $key => $value) {
            # code...

            $post["images"][] = $value->attributes->getNamedItem('src')->nodeValue;
        }
        //$post["images"] = $images[0];
    }

    //replace values
   
    

    $post['announcement'] = preg_replace("/<img[^>]+\>/i", "", $post['announcement']); 
    $post['survey_question'] = preg_replace("/<img[^>]+\>/i", "", $post['survey_question']);
    // if($post['survey_my_responses']!=null){
    //     $post['survey_total_response'] = $data_reader_facade->getSurveysTotalResponse($post["id"]);
    // }
    

    array_push($posts_with_author_images, $post);
}


header('Cache-Control: no-cache, must-revalidate');
    header('Content-type: application/json');


    // $APIUtil = new APIUtilities();
    // $APIUtil->addAPIHeaders();


   echo json_encode($posts_with_author_images);


error_reporting(0);

?>