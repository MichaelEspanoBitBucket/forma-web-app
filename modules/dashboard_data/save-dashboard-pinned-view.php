<?php 

error_reporting(0);

include_once('dashbaord_dependencies.php');
require_once("../../library/dashboard-repositories/DashboardRepository.php");


$auth = Auth::getAuth('current_user');
$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);


if(!$auth){

	http_response_code(401);
	echo "Failed to load data sources. User not logged in.";
}else{
	
	$dbRep = new DashboardRepository($conn, $auth);
	header('Cache-Control: no-cache, must-revalidate');
	header('Content-type: application/json');

	//var_dump($_POST['data']);	
	$dbPref = $dbRep->appendPinnedView($_POST['data']);
	

	$dbPrefJson = json_encode($dbPref);
	echo $dbPrefJson;
}
?>