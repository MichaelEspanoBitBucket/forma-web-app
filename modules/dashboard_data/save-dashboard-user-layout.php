<?php

// Saves dashbaord Layout

error_reporting(0);

include_once('dashbaord_dependencies.php');
require_once("../../library/dashboard-repositories/DashboardRepository.php");


$auth = Auth::getAuth('current_user');
$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);


if(!$auth){
	http_response_code(401);
	echo "Failed to load data sources. User not logged in.";
}else{

	$auth = Auth::getAuth('current_user');
	$dbRep = new DashboardRepository($conn, $auth);

	if(isset($_POST["Layout"])){
		$dbRep->saveUserLayout($_POST["Layout"]);
	}else{
		echo "No layout parameter has been pass.";
	}

}

?>
