<?php
error_reporting(0);
//check if session is alive if not.. retun false;
include_once('dashbaord_dependencies.php');
// include_once('../../library/upload.php');
// include_once('../../library/userQueries.php');
// include_once('../../library/post.php');
// include_once('../../library/notifications.php');
// include_once('../../library/ListViewFormula.php');
include_once('../../library/Search.php');
include_once('../../library/DatabaseConnection.php');
include_once('../../library/MySQLDatabase.php');




if (!Auth::hasAuth('current_user')) {
    return false;
}
$conn = getCurrentDatabaseConnection();
$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions;
$search = new Search();
//$listViewFormula = new ListViewFormula();
$user_id = $auth['id'];
$mod = unserialize(ENABLE_COMPANY_MOD);
// $notifications = new notifications();
$post_data = filter_input_array(INPUT_POST);
$redis_cache = false;//getRedisConnection();

if (isset($post_data['action'])) {

    $date = $fs->currentDateTime();
    switch ($post_data['action']) {
        case "getRequestDataTableAngular":
            $conn->query("START TRANSACTION");
            $id = $post_data['id'];
            $search_value = trim($post_data['search']['value']);
            $field = $post_data['field'];
            $limit = $post_data['length'];
            $start = $post_data['start']; //iDisplayStart to start
            $date_field = $post_data['date_field'];
            $date_from = $post_data['date_from'];
            $date_to = $post_data['date_to'];
            $isAdvanceSearch = $post_data['isAdvanceSearch'];
            $columns = $post_data['columns'];
            $obj = array();
            if ($date_field != "0" && $date_from != "" && $date_to != "") {
                $obj = array("date_range" => "1",
                    "date_field" => $date_field,
                    "date_from" => $date_from,
                    "date_to" => $date_to);
                $obj = json_decode(json_encode($obj), true);
            }
            if ($post_data['column-sort'] != "") {
                $obj['column-sort'] = $post_data['column-sort'];
                $obj['column-sort-type'] = $post_data['column-sort-type'];
            }

            if (isset($post_data['multi_search'])) {
                $multi_search = "";
                $ctrSpec = 0;
                $ctr = 0;
                $multi_search_arr = json_decode($post_data['multi_search'], true);
                foreach ($multi_search_arr as $value) {
                    $ctr++;
                    if ($value['search_field'] == "0") {
                        $search_value = $value['search_value'];
                        $field = $value['search_field'];
                    } else {
                        $ctrSpec++;
                        if ($ctrSpec == 1) {
                            $multi_search .= " AND (";
                        }
                        if ($value['search_field'] == "Requestor") {
                            if ($value['search_operator'] == "%") {
                                $multi_search .= " (request.requestor_display_name LIKE '%" . $value['search_value'] . "%')";
                            } else {
                                $multi_search .= "request.requestor_display_name " . $value['search_operator'] . "  '" . $value['search_value'] . "'";
                            }
                        } else if ($value['search_field'] == "Processor") {
                            if ($value['search_operator'] == "%") {
                                $multi_search .= " (processer.display_name LIKE '%" . $value['search_value'] . "%' OR processer.first_name LIKE '%" . $value['search_value'] . "%' OR processer.last_name LIKE '%" . $value['search_value'] . "%' OR CONCAT_WS( '', processer.first_name, processer.last_name) LIKE  '%" . $value['search_value'] . "%')";
                            } else {
                                $multi_search .= "processer.display_name " . $value['search_operator'] . "  '" . $value['search_value'] . "'";
                            }
                        } else if ($value['search_field'] == "ID") {
                            $multi_search .= " (request.ID " . $value['search_operator'] . " " . $value['search_value'] . ")";
                        } else {
                            if ($value['search_operator'] == "%") {
                                $multi_search .= " (request." . $value['search_field'] . " LIKE '%" . $value['search_value'] . "%')";
                            } else {
                                $multi_search .= " (request." . $value['search_field'] . " " . $value['search_operator'] . " '" . $value['search_value'] . "')";
                            }
                        }
                        if ($value['search_condition'] != "" || $value['search_condition'] != undefined || $value['search_condition'] != "undefined") {
                            if ($value['search_condition'] == "OR") {
                                $multi_search .= " OR ";
                            } else {
                                $multi_search .= " AND ";
                            }
                        } else {
                            $multi_search .= " AND ";
                        }
                    }
                }
                if ($ctrSpec > 0) {
                    $multi_search = substr($multi_search, 0, strlen($multi_search) - 4);
                    $multi_search.= ")";
                }
                $obj['multi_search'] = $multi_search;
            }
            $obj['isAdvanceSearch'] = $isAdvanceSearch;
           // $getDeleteAccess = $db->query("SELECT FIND_IN_SET({$auth["id"]}, getFormUsers({$id},6)) as delete_access_forma", "row");
            if ($redis_cache && ($isAdvanceSearch == "0" || $isAdvanceSearch == "")) {
                /*
                  Cache Format
                  1. User Id.
                  2. Page Number.
                  3. Page Limit.
                  4. Search Filter.
                  5. Column Sort.
                  6. Type of Sort.
                 */

                $cachedFormat = "$user_id::$start::$limit::$search_value::" . $obj['column-sort'] . "::" . $obj['column-sort-type'] . "";

                // $memcache->delete("request_list");
                $cache_request_list = json_decode($redis_cache->get("request_list"), true);

                $cached_query_result = $cache_request_list['form_id_' . $id]['' . $cachedFormat . ''];
                if ($cached_query_result) {
                    // echo "from cache";

                    $result = $cached_query_result;
                } else {
                    // echo "from db";

                    $result = $search->getManyRequestV2($search_value, $field, $id, $start, $limit, $obj);
                    $cache_request_list['form_id_' . $id]['' . $cachedFormat . ''] = $result;
                    $redis_cache->set("request_list", json_encode($cache_request_list));
                }
            } else {
                ///echo "Here";
                $result = $search->getManyRequestV2($search_value, $field, $id, $start, $limit, $obj);
            }


            if (REQUEST_QUERY_DEBUGGER == "1") {
                print_r($result);
                return;
            }

            $countRequest = $result[0][3]['count'];
            if (is_null($countRequest)) {
                $countRequest = 0;
            }
            // $forms = $db->query("SELECT * FROM tb_workspace WHERE id = {$db->escape($id)} AND is_delete = 0", "row");
            // $form_json = json_decode($forms['form_json']);
            // $fields_headerObj = $form_json->{'form_json'}->{'collected_data_header_info'}->{'listedchoice'};
            // $fields_headerObjType = $form_json->{'form_json'}->{'headerInfoType'};
            // $fields_headerObjType_allowReorder = $form_json->{'form_json'}->{'allowReorder'};
            // $enable_deletion = $forms['enable_deletion'];
            $output = array(
                "draw" => intval($post_data['draw']),
                "iTotalRecords" => $countRequest,
                "iTotalDisplayRecords" => $countRequest,
                "start" => $start,
                "aaData" => array(),
                "column-sort" => $post_data['column-sort'],
                "column-sort-type" => $post_data['column-sort-type'],
                "aDataSort" => $post_data['column-sort']
               // "enable_deletion" => $enable_deletion
            );
            // $getCustomView = $db->query("SELECT * FROM tbcustomview WHERE user_id = $user_id AND form_id = $id ", "array");
            // if ($fields_headerObjType_allowReorder == "0") {
            //     $getCustomView = array();
            // }


            //generate print
            // $getGeneratePrint = $db->query("SELECT * FROM tb_generate WHERE form_id={$db->escape($id)}", "row");
            // $getGeneratePrintNumrows = $db->query("SELECT * FROM tb_generate WHERE form_id={$db->escape($id)}", "numrows");
            // $print_form_id = $getGeneratePrint['id'];


            //TEMPORARY REMOVED because it is not used
            // $isJunctionFields = $db->query("SELECT * FROM tbfields where form_id = $id", "numrows");
            // if ($isJunctionFields > 0) {
            //     $fields_headerObj_array = array();
            //     $fields_headerObj = json_decode(json_encode($fields_headerObj), true);
            //     foreach ($fields_headerObj as $key => $value) {
            //         $field_name = str_replace("[]", "", $value['field_name']);
            //         $getFieldsData = $db->query("SELECT * FROM tbfields where form_id = $id AND field_name = '" . $field_name . "'", "row");
            //         if ($getFieldsData['id'] > 0) {
            //             $getFieldsData['field_label'] = $getFieldsData['data_field_label'];
            //             array_push($fields_headerObj_array, $getFieldsData);
            //         } else {
            //             array_push($fields_headerObj_array, $value);
            //         }
            //     }
            //     $fields_headerObj = $fields_headerObj_array;
            // }

            // foreach ($result as $value) {
            //     $otherFieldsStartPos = 3; // start of index
            //     $aaData = array();
            //     $button = "<ul class='fl-table-actions'>";
            //     $starred_class = "fa fa-star-o fl-custom-starred";
            //     $starred_title = "Starred Request";
            //     $starred = 0;
            //     if ($value[0]['starred'] > 0) {
            //         $starred_class = "fa fl-custom-starred fa-star";
            //         $starred = $value[0]['starred'];
            //         $starred_title = "Remove Starred";
            //     }
            //     //$button .='<a href="/user_view/workspace?view_type=request&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '"><button class="tip fa fa-eye btn-basicBtn fl-basicsecondary-btn padding_5 cursor" style="margin-right:3px" form-id = "' . $id . '" request-id="' . $value['request_id'] . '" data-original-title="View Form"></button></a>';
            //     $form_name = str_replace(" ", "_", $forms['form_name']);
            //     //$button .='<button class="fa fa-caret-right" style="background:transparent!important; border: 0!important; font-size:15px;"></button> ';

            //     if ($value['form_status'] != "Cancelled" && $value['form_status'] != "Draft" && $value[2]['buttons'] != '') {
            //         $button .= "<li><input type='checkbox' form-status='" . $value['form_status'] . "' class='appCheckSingle " . $form_name . " css-checkbox' data-form-name='" . $form_name . "' style='margin-top: 5px;margin-right: 10px;' button-action-data='" . $value[2]['buttons'] . "' button-request-id='" . $value['ID'] . "'button-workflow-id='" . $value['Workflow_ID'] . "'  button-requestor-id='" . $value['Requestor'] . "' button-processor-id='" . $value['Processor'] . "' id='" . $value['TrackNo'] . "'><label for='" . $value['TrackNo'] . "' class='css-label' style='margin-top:-5px;'></label></li>";
            //     } else {
            //         // $button .= "<li>&nbsp;&nbsp;&nbsp;&nbsp;</li>"; //"<input type='checkbox' form-status='disabled' disabled=disabled class='appCheckSingle " . $form_name . " css-checkbox' data-form-name='" . $form_name . "' style='margin-top: 5px;margin-right: 10px;' button-action-data='" . $value[2]['buttons'] . "' button-request-id='" . $value['ID'] . "'button-workflow-id='" . $value['Workflow_ID'] . "'  button-requestor-id='" . $value['Requestor'] . "' button-processor-id='" . $value['Processor']."' id='". $value['TrackNo'] ."'><label for='". $value['TrackNo'] ."' class='css-label' style='margin-right:7px;margin-top:-5px;'></label>";
            //         $button .= "<li><input type='checkbox' disabled='disabled' form-status='disabled' class='appCheckSingle " . $form_name . " css-checkbox' data-form-name='" . $form_name . "' style='margin-top: 5px;margin-right: 10px;' button-action-data='" . $value[2]['buttons'] . "' button-request-id='" . $value['ID'] . "'button-workflow-id='" . $value['Workflow_ID'] . "'  button-requestor-id='" . $value['Requestor'] . "' button-processor-id='" . $value['Processor'] . "' id='" . $value['TrackNo'] . "'><label for='" . $value['TrackNo'] . "' class='css-label' style='margin-top:-5px;'></label></li>";
            //     }
            //     if ($mod['others']['starred'] == "1") {
            //         $button .='<li><a class="tip ' . $starred_class . ' btn-basicBtn padding_5 cursor fl-basicsecondary-btn starred" style="background:transparent!important; border: 0!important; font-size:15px;" data-id="' . $value['request_id'] . '" id="starred_' . $value['request_id'] . '" data-starred="' . $starred . '" data-original-title="' . $starred_title . '"></a></li>';
            //     }
            //     if ($getGeneratePrintNumrows > 0) {
            //         $button .='<li><a class="tip fa fa-print" href="/user_view/workspace?view_type=update&ID=' . $print_form_id . '&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '&embed_type=viewEmbedOnly&print_form=true" data-original-title="Print Customized Form"></a></li>';
            //     } else {
            //         $button .='<li><a class="tip noCustomPrint tip btn-basicBtn padding_5 cursor fl-basicsecondary-btn fa fa-print" href="#" data-original-title="No Customize Print Available" style="background:transparent!important; border: 0!important; font-size:15px;opacity: 0.3;"></a></li>';
            //     }

            //     // Annotation
            //     // Temporary Hide
            //     if (ALLOW_FORM_ANNOTATION == "1") {
            //         $annotation = $db->query("SELECT * FROM tb_annotation WHERE Anno_FormID={$db->escape($id)} AND Anno_RequestID={$db->escape($value['request_id'])} AND Anno_TrackNo={$db->escape($value['TrackNo'])} AND Anno_is_active={$db->escape('1')}", "row");
            //         $annotation_count = $db->query("SELECT * FROM tb_annotation WHERE Anno_FormID={$db->escape($id)} AND Anno_RequestID={$db->escape($value['request_id'])} AND Anno_TrackNo={$db->escape($value['TrackNo'])} AND Anno_is_active={$db->escape('1')}", "numrows");
            //         if ($annotation_count != "0") {

            //             // $button .='<li><a class="tip  tip btn-basicBtn padding_5 cursor fl-basicsecondary-btn fa fa-list-alt" href="/user_view/workspace?view_type=request&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '&annoID=' . $annotation['Anno_UniqueID'] . '" data-original-title="View Annotation" style="background:transparent!important; border: 0!important; font-size:15px;"></a></li>';
            //             $button .= '<li><a class="linkTrigger display" href="/user_view/workspace?view_type=request&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '&printID=' . (trim($print_form_id) == "" ? 0 : $print_form_id) . '&annoID=' . $annotation['Anno_UniqueID'] . '">' . "<div class='fl-table-ellip'>" . $value['TrackNo'] . "</div></a></li>";
            //         } else {
            //             $button .= '<li><a class="linkTrigger display" href="/user_view/workspace?view_type=request&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '&printID=' . (trim($print_form_id) == "" ? 0 : $print_form_id) . '">' . "<div class='fl-table-ellip'>" . $value['TrackNo'] . "</div></a></li>";
            //         }
            //     } else {
            //         $button .= '<li><a class="linkTrigger display" href="/user_view/workspace?view_type=request&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '&printID=' . (trim($print_form_id) == "" ? 0 : $print_form_id) . '">' . "<div class='fl-table-ellip'>" . $value['TrackNo'] . "</div></a></li>";
            //     }





            //     //COMMENT_NOTIFICATION
            //     if (COMMENT_NOTIFICATION == "1") {
            //         $noti_condition = " AND tbc.post_id = '" . $value['request_id'] . "' AND tbc.fID = '" . $id . "'";

            //         $notiCount = $notifications->countNewNotiForRequest($noti_condition);
            //         $newNoti = "";
            //         if ($notiCount > 0) {
            //             $newNoti = '<span class="notiCommentCounterContainer">' . $notiCount . '</span>
            //                             <i class="fa fa-comment-o hasCommentNotification"></i>';
            //         } else {
            //             $newNoti = '<i class="fa fa-comment-o"></i>';
            //         }
            //         //comment
            //         $button .= '<li class="isCursorPointer showReply tip" count-active-noti="' . $notiCount . '" data-original-title="Add / View Comment" data-type="1" data-f-id="' . $id . '" data-id="' . $value['request_id'] . '" active-comment="true">
            //                         <a data-original-title="" title="" style="position:relative">
            //                             ' . $newNoti . '
            //                         </a>
            //                     </li>';
            //     }

            //     // $button .='<button class="fa fa-comment btn-basicBtn padding_5 fl-basicsecondary-btn cursor showReply tip" data-type="1" data-f-id="' . $id . '" data-id="' . $value['request_id'] . '" data-original-title="Add/View Comments" ></button> ';
            //     //$button .='<button class="tip fa fa-book btn-basicBtn padding_5 fl-basicsecondary-btn cursor viewAuditLogs" style="margin-right:3px" data-request-id="' . $value['request_id'] . '" data-form-id="' . $id . '" data-original-title="Audit Logs"></button>';
            //     if ($value[2]['buttons'] != '' && $value[2]['buttons'] != 'null' && $value[2]['buttons'] != null) {
            //         //$button .= "<button class='tip fa fa-gear btn-basicBtn padding_5 fl-basicsecondary-btn cursor' button-action-data='" . $value[2]['buttons'] . "' button-request-id='" . $value['ID'] . "'button-workflow-id='" . $value['Workflow_ID'] . "'  button-requestor-id='" . $value['Requestor'] . "' button-processor-id='" . $value['Processor'] . "' button-type='action' data-original-title='Please select an Action'></button>";
            //     }
            //     if ($getDeleteAccess['delete_access_forma'] > 0 && ENABLE_DELETION == "1" && $enable_deletion == "1") {
            //         $button .='<li><a class="tip fa fa-remove btn-basicBtn padding_5 cursor fl-basicsecondary-btn trash-record-single" style="margin-left:2px;color: #F95353;background:transparent!important; border: 0!important; font-size:15px;" data-id="' . $value['request_id'] . '" title="Delete"></a></li>';
            //     }
            //     $button.= '</ul>';
            //     //reorder
            //     if (count($getCustomView) > 0) {
            //         // $customViewObject = json_decode($getCustomView[0]['json'],true);
            //         $customViewObject = getValidHeaderFields($fields_headerObj, $getCustomView, $fields_headerObjType);
            //         array_push($aaData, $button);
            //         foreach ($customViewObject as $customView) {
            //             if ($customView['field_name'] == "Requestor") {
            //                 $vl = "requestorName";
            //             } else {
            //                 $vl = $customView['field_name'];
            //             }
            //             // if($vl=="TrackNo"){
            //             //  array_push($aaData, '<a href="/user_view/workspace?view_type=request&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '">'."<div class='fl-table-ellip'>".$value['TrackNo']."</div></a>");
            //             // }else if($vl!=""){

            //             $data = $value['' . $vl . ''];


            //             if ($isJunctionFields > 0) {
            //                 $customView['value'] = $value['' . $vl . ''];
            //                 $data = $listViewFormula->setValue($customView);
            //             }
            //             $style = "";
            //             if ($customView['field_input_type'] == "Currency" || $customView['field_input_type'] == "Number") {
            //                 $style.="text-align:right;";
            //             }
            //             if ($customView['field_type'] == "dateTime" || $customView['field_type'] == "time") {
            //                 $data = substr($data, 0, strlen($data) - 3);
            //             }
            //             array_push($aaData, "<div class='fl-table-ellip' style='" . $style . "'>" . $data . "</div>");
            //             // }
            //         }
            //     } else {
            //         $fields_headerObj = json_decode(json_encode($fields_headerObj));
            //         //default column
            //         $aaData = array($button,
            //             // '<a href="/user_view/workspace?view_type=request&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '">'."<div class='fl-table-ellip'>".$value['TrackNo']."</div></a>",
            //             "<div class='fl-table-ellip'>" . $value['TrackNo'] . "</div>",
            //             "<div class='fl-table-ellip'>" . $value['requestorName'] . "</div>",
            //             "<div class='fl-table-ellip'>" . $value['form_status'] . "</div>",
            //             "<div class='fl-table-ellip'>" . $value['DateCreated'] . "</div>",
            //         );
            //         //old shit
            //         //push to array $aadata in specific location
            //         foreach ($fields_headerObj as $fields_header) {
            //             if ($value['' . $fields_header->{'field_name'} . ''] == "") {
            //                 array_insert($aaData, "", $otherFieldsStartPos);
            //             } else {
            //                 array_insert($aaData, "<div class='fl-table-ellip'>" . $value['' . $fields_header->{'field_name'} . ''] . "</div>", $otherFieldsStartPos);
            //             }
            //             $otherFieldsStartPos++;
            //         }


            //         //Updated by AARON TOLENTINO
            //         // - For having a specific and default header information
            //         if ($fields_headerObjType == "1") {
            //             $aaData = array($button);
            //             foreach ($fields_headerObj as $fields_header) {
            //                 if ($value['' . $fields_header->{'field_name'} . ''] == "") {
            //                     array_push($aaData, "");
            //                 } else {
            //                     if ($fields_header->{'field_name'} == "Requestor") {
            //                         $vl = "requestorName";
            //                     } else {
            //                         $vl = $fields_header->{'field_name'};
            //                     }

            //                     $data = $value['' . $vl . ''];

            //                     $style = "";

            //                     if ($isJunctionFields > 0) {
            //                         $fields_header->{'value'} = $value['' . $vl . ''];
            //                         $data = $listViewFormula->setValue($fields_header);

            //                         if ($fields_header->{'field_input_type'} == "Currency" || $customView['field_input_type'] == "Number") {
            //                             $style.="text-align:right;";
            //                         }
            //                         if ($fields_header->{'field_type'} == "dateTime" || $fields_header->{'field_type'} == "time") {
            //                             $data = substr($data, 0, strlen($data) - 3);
            //                         }
            //                     }
            //                     array_push($aaData, "<div class='fl-table-ellip' style='" . $style . "'>" . $data . "</div>");
            //                 }
            //             }
            //         } else if ($fields_headerObjType == "0") {
            //             // FOR DEFAULT COLUMN
            //             $aaData = array($button,
            //                 // '<a href="/user_view/workspace?view_type=request&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '">'."<div class='fl-table-ellip'>".$value['TrackNo']."</div></a>",
            //                 "<div class='fl-table-ellip'>" . $value['TrackNo'] . "</div>",
            //                 "<div class='fl-table-ellip'>" . $value['requestorName'] . "</div>",
            //                 "<div class='fl-table-ellip'>" . $value['form_status'] . "</div>",
            //                 "<div class='fl-table-ellip'>" . $value['DateCreated'] . "</div>",
            //             );
            //         }
            //     }
            //     //$output['aaData'][] = $aaData;
            // }
            //var_dump($columns);
            $aaData = [];
            foreach ($result as $resKey => $res) {
                 $adata = [];
                foreach ($columns as $CColkey => $col) {
                    if ($col["data"]=="") {
                        continue;
                    }
                    //echo $value["data"];
                    $adata[0] = $res[0];
                    $adata[1] = $res[1];
                    $adata[2] = $res[2];
                    $adata[3] = $res[3];
                    $adata["ID"] = $res["ID"];
                    $adata["TrackNo"] = $res["TrackNo"];
                    $adata["request_id"] = $res["request_id"];
                    
                    

                   $adata[$col["data"]] = $res[$col["data"]];
                }
                $aaData[] = $adata;
            }
           
            $output['aaData'] = $aaData;    
            $conn->query("COMMIT");
            echo json_encode($output);
            break;
     case "saveCustomView":
        $customView = $post_data['customView'];
        $form_id = $post_data['form_id'];
        $forms = $db->query("SELECT form_json FROM tb_workspace WHERE id = {$db->escape($form_id)} AND is_delete = 0", "row");
        $form_json = json_decode($forms['form_json']);

        $fields_headerObjType_allowReorder = $form_json->{'form_json'}->{'allowReorder'};

        if ($fields_headerObjType_allowReorder != "") {
            if ($fields_headerObjType_allowReorder == "0") {
                // return true;
            }
        }
        $fields = array("user_id" => $user_id,
            "form_id" => $form_id,
            "json" => $customView);
        $count = $db->query("SELECT * FROM tbcustomview WHERE user_id = $user_id AND form_id = $form_id ", "numrows");
        if ($count > 0) {
            $where = array("form_id" => $form_id,
                "user_id" => $user_id);
            $db->update("tbcustomview", $fields, $where);
        } else {
            $db->insert("tbcustomview", $fields);
        }
        break;
    }
}
?>
