<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
@session_start();
$path = $_SERVER["DOCUMENT_ROOT"];
include_once $path . "/config/database_url_config.php";
include_once $path . "/library/Common.php";
$dbh = Common::getDatabaseConnection();
$current_date = Common::getCurrentDateTime();
$get_data = filter_input_array(INPUT_GET);
$session_id = session_id();
$current_user = $_SESSION["current_user"]["display_name"];
$user_id = $_SESSION["current_user"]["id"];
$form_id = $get_data["form_id"];
$request_id = $get_data["request_id"];
//deactivate existing sessions that exceed 30mins
$deactivate_sql = "DELETE FROM tbrequest_lock_session WHERE TIMESTAMPDIFF(MINUTE, DATECREATED, :current_date) >= 30";
$deactivate_statement = $dbh->prepare($deactivate_sql);
$deactivate_statement->bindParam(":current_date", $current_date);
$deactivate_statement->execute();

$str_sql = "SELECT ls.user_id, user.display_name FROM tbrequest_lock_session ls LEFT JOIN tbuser user ON user.id = ls.user_id WHERE ls.form_id = :form_id AND ls.request_id = :request_id "
        . " AND ls.is_active = 1 ORDER BY ls.datecreated ASC LIMIT 1";

$sth = $dbh->prepare($str_sql);
$sth->bindParam(":form_id", $form_id);
$sth->bindParam(":request_id", $request_id);
$sth->execute();
$result = $sth->fetch(PDO::FETCH_ASSOC);

if (!$result) {
    //ready for editing, insert lock session
    $insert_sql = "INSERT INTO tbrequest_lock_session (form_id, request_id, user_id, is_active, datecreated, dateupdated, createdby, updatedby)"
            . " VALUES (:form_id, :request_id, :user_id, 1, :current_date, :current_date, :current_user, :current_user)";
    $insert_statement = $dbh->prepare($insert_sql);
    $insert_params_arr = array(
        ":form_id" => $form_id,
        ":request_id" => $request_id,
        ":user_id" => $user_id,
        ":current_date" => $current_date,
        ":current_user" => $current_user
    );
    $insert_statement->execute($insert_params_arr);
    $response_array = array("status" => "success", "message" => "Document ready for editing.");
} else {
    $lock_user_id = $result["user_id"];

    if ($user_id != $lock_user_id) {
        //doc being edited by other user
        $response_array = array("status" => "warning", "message" => "Locked Document\n\nDocument is currently open to user {$result["display_name"]}. You will not be able to process this document.\n\nPlease wait unti the user has closed the request then refresh this page.");
    } else {
        //ready for editing, as is
        $response_array = array("status" => "success", "message" => "Document ready for editing.");
    }
}

print_r(json_encode($response_array));
