<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
@session_start();
$path = $_SERVER["DOCUMENT_ROOT"];
include_once $path . "/config/database_url_config.php";
include_once $path . "/library/Common.php";
$dbh = Common::getDatabaseConnection();
$get_data = filter_input_array(INPUT_POST);
$user_id = $_SESSION["current_user"]["id"];
$form_id = $get_data["form_id"];
$request_id = $get_data["request_id"];

$delete_sql = "DELETE FROM tbrequest_lock_session WHERE form_id = :form_id AND request_id=:request_id AND user_id=:user_id AND is_active = 1";
$delete_statement = $dbh->prepare($delete_sql);
$delete_params_arr = array(":form_id" => $form_id,
    ":request_id" => $request_id,
    ":user_id" => $user_id);

$delete_statement->execute($delete_params_arr);
