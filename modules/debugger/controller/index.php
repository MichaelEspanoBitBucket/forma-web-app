<?php
	//TEST
	//ADD CODE HERE FOR SCRATCH DEBUG
	// error_reporting(E_ALL);
	// var_dump($_POST);
	function getMACAddress ($ip) {
		$value = '';
		$temp = '';
		if(!$ip){
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
		    $temp = exec("arp -a ".$ip);
		    if($temp == 'No ARP Entries Found.'){
		    	// return $temp;
		    	$temp = '';
		    }else{
			    // preg_match('/[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}.*/', $temp, $matches);
			    // preg_split('/[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}.*/', $temp, $matches);
			    $output=preg_split('/\s+/', trim($temp));
			    $value = array(
			    	'ip' => $output[0],
			    	'mac' => $output[1],
			    	'type' => $output[2],
			    );
		    }
		} else {
		    // $temp = 'Not available for '.PHP_OS;
		    $temp = '';
		}
		return $value;
	}
	var_dump( getMACAddress() );
?>

<script type="text/javascript" src="/js/library/angular/a_angular.min.js"></script>
<script>
	var myDebugger = angular.module('myDebugger',[]);
	var controllers = {
		'Debugger':['$scope',function($scope){
			
			$scope.initializeData = function(dataJSON){
				$scope.tokenData = [];
				$scope.sourceType = 'query';
				$scope.showRemoveToken = false;
				$scope.showAddToken = true;
				$scope['sourceType'] = dataJSON['sourceType']||'manual';
				$scope.formula = dataJSON['formula']||'';
				
				delete dataJSON['formula'];
				delete dataJSON['query'];
				delete dataJSON['sourceType'];

				for(key in dataJSON){
					$scope.tokenData.push({'tokenName':key, 'tokenValue':dataJSON[key]});
				}

				if($scope.tokenData.length <= 0){
					$scope.tokenData.push({'tokenName':'', 'tokenValue':''});
				}else if($scope.tokenData.length >= 2){
					$scope.showRemoveToken = true;
				}
			}
			$scope.addTokenData = function(key,value){
				if(typeof key == "string"){
					var find_token = $scope.tokenData.find(function(a){ return a.tokenName == key });
					if(find_token){
						find_token.tokenValue = value||find_token.tokenValue;
					}else{
						$scope.tokenData.splice( 0 , 0, {'tokenName':key, 'tokenValue':''});
						if($scope.tokenData.length >= 2){
							$scope.showRemoveToken = true;
						}
					}
				}else{
					value = key;
					$scope.tokenData.splice( $scope.tokenData.indexOf(value) + 1 , 0, {'tokenName':'', 'tokenValue':''});
					if($scope.tokenData.length >= 2){
						$scope.showRemoveToken = true;
					}
				}
			};
			$scope.removeTokenData = function(value){
				delete $scope.tokenData[$scope.tokenData.indexOf(value)];
				$scope.tokenData = $scope.tokenData.filter(Boolean);
				if($scope.tokenData.length <= 1){
					$scope.showRemoveToken = false;
				}
			};
			$scope.getTokens = function (){
				var system_key = [
					'@StrRight', '@StrLeft', '@StrCount', '@StrConcat',
			        '@StrGet', '@ThisDate', '@AdjustDate', '@DiffDate',
			        '@GivenIf', '@Lookup', '@RecordCount', '@LookupWhere',
			        '@Total', '@LookupGetMax', '@LookupGetMin', '@LookupSDev',
			        '@LookupAVG', '@GetRound', '@LookupCountIf', '@Sum',
			        '@Requestor', '@RequestID', '@Status', '@CurrentUser', '@Department', '@TrackNo', '@Today', '@Now',
			        '@GetAuth', "@FormatDate", "@NumToWord",
			        "@Head", "@AssistantHead", "@UserInfo", "@GetData",
			        "@LookupAuthInfo",
			        "@GetAVG", "@GetMin", "@SDev", "@GetMax",
			        "@StrSplitBy","@CustomScript","@LoopIn","@TimeStamp"
		        ];
				var fields = (($scope.formula||"").match(/@[0-9a-zA-Z_]*/g)||[]).map(function(a){
					if(system_key.indexOf(a) <= -1){
						$scope.addTokenData(a.replace("@","") , "");
					}else{
						return false;
					}
				});

			};
		}]
	}
	myDebugger.controller(controllers);
	
</script>
<div ng-app="myDebugger" >
	<form method="POST" ng-controller="Debugger" action ng-init="initializeData(<?php echo htmlentities(json_encode($_POST,JSON_PRETTY_PRINT),ENT_QUOTES);?>)">
		<label>
			Formula:
			<textarea ng-model="formula" name="formula" style="width: 100%; height: 157px;"><?php echo $_POST['formula']?></textarea>
		</label>
		<br/><br/>
		<div>
			<label>Formula Data Source [OPTIONAL]:</label>
			<label><input type="radio" ng-model="sourceType" name="sourceType" value="query" checked="checked"/> Query</label>
			<label><input type="radio" ng-model="sourceType" name="sourceType" value="manual" /> Manual</label>
			
			<div ng-switch="sourceType">
				<label ng-switch-when="query">
					Note: <span>Please use only the SELECT clause, this page is for developers only</span>
					<textarea name="query" style="width: 100%;"><?php echo $_POST['query']?></textarea>
				</label>
				<table ng-switch-when="manual">
					<thead><tr>
						<th colspan="2"><span ng-click="getTokens()" style="padding: 2px 5px; cursor:pointer;border-radius: 5px; color: white; background-color: black; width: 20px; height: 20px; text-align: center;">Get Formula Tokens</span></th>
					</tr></thead>
					<thead><tr>
						<th>Token Name</th><th>Value</th><th></th>
					</tr></thead>
					<tbody>
						<tr ng-repeat="(key, value) in tokenData">
							<td><input type="text" ng-model="value.tokenName" /></td>
							<td><input type="text" name="{{value.tokenName}}" ng-model="value.tokenValue"/></td>
							<td>
								<span style="cursor:pointer;border-radius: 5px; color: white; background-color: black; width: 20px; height: 20px; display: inline-block; text-align: center;" ng-click="addTokenData(value)" ng-if="showAddToken">+</span>
								<span style="cursor:pointer;border-radius: 5px; color: white; background-color: black; width: 20px; height: 20px; display: inline-block; text-align: center;" ng-click="removeTokenData(value)" ng-if="showRemoveToken">-</span>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

		</div>
		<br/><br/>
		<label>
			<input type="submit" />
		</label>
		<br/><br/>
		<label>
			<span style="width: 100%; display: block; background-color: black; color: white; padding: 5px;">Result:</span>
			<pre><div contenteditable="true" style=" width: 100%; min-height: 100px;"><?php
					$test = new Formula($_POST['formula']);
					$db = new Database();
					if($_POST['sourceType'] == 'query'){
						if($_POST['query']){
							$result = $db->query($_POST['query'],"row");
							$test->DataFormSource[0] = $result;
						}
					}else if($_POST['sourceType'] == 'manual'){
						$tokens = $_POST;
						unset($tokens['formula']);
						unset($tokens['sourceType']);
						unset($tokens['query']);
						$test->DataFormSource[0] = $tokens;
					}
					if(function_exists('var_debug')){
						var_debug($test->evaluate());
					}else{
						var_dump($test->evaluate());
					}
			?></div></pre>
		</label>
		<label>
			<span style="width: 100%; display: block; background-color: black; color: white; padding: 5px;">Tracker:</span>
			<div  style=" width: 100%; min-height: 100px;">
				PARSED FORMULA:
				<div contenteditable="true" style="border:1px solid gray;border-radius:10px;">
					<?php
						var_dump($test->replaceFormulaFieldNames());
					?>
				</div>
				RAW:
				<div contenteditable="true" style="border:1px solid gray;border-radius:10px;">
					<?php
						var_dump($GLOBALS['formula_executed_data_collector']);
					?>
				</div>
			</div>
		</label>
	</form>
</div>