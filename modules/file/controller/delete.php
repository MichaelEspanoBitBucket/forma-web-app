<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once API_LIBRARY_PATH . 'API.php';
include_once APPLICATION_PATH . '/library/APILibraries/APIUtilities.php';

$auth = new Auth();
$current_user = $auth->getAuth("current_user");
$response_type = filter_input(INPUT_POST, "response_type");
$imagefile = filter_input(INPUT_POST, "imagefile");
//var_dump("ETOLOOB", $imagefile);
// $testpath = "http://192.168.0.129/images/announcement_attachments/9b3b0a80332688320c66e2b6765e4651/463aafa947b38188e5de5197bd82cc75.jpg";

$filteredpath = str_replace('http://', '', $imagefile);
$finalpath = explode('/', $filteredpath);

$string_path_concat = '';
$counter = count($finalpath);
for($x = 1; $x < $counter; $x++) {
  $string_path_concat .= '/'.$finalpath[$x];
}
//var_dump($string_path_concat);

$path = APPLICATION_PATH . $string_path_concat;
if (!empty($string_path_concat)) {
  if (!@file_exists($path)) {
    // var_dump($path);
    var_dump("file not exists");
  } else {
    // var_dump($path);
    var_dump("file exists");
    @unlink($path);
  }
}


if($response_type=="JSON"){
    $utilities = new APIUtilities();
    $utilities->addAPIHeaders();
    echo json_encode(array("status"=>$this->status));


}else{
    include dirname(__FILE__) . "/../view/api_response.phtml";
}

