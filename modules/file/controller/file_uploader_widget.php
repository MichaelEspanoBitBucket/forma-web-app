<?php

error_reporting(E_ALL);

if (Auth::hasAuth('current_user')) {
    $current_user = Auth::getAuth('current_user');

    //  get parameters from post or partial view
    if ($this->loaded_as_partial_view && $this->partial_view_data) {
        $click_trigger_class = $this->partial_view_data["click_trigger_class"];
        $target_directory    = $this->partial_view_data["target_directory"];
        $file_uploader_id    = $this->partial_view_data["file_uploader_id"];
    } else {
    	$parameters			 = filter_input_array(INPUT_POST);
    	$click_trigger_class = $parameters["click_trigger_class"];
    	$target_directory	 = $parameters["target_directory"];
    	$file_uploader_id  	 = $parameters["file_uploader_id"];
        // throw new Exception("Illegal state, no \"partial_view_data\" provided for file uploader widget");
    }
}

$this->click_trigger_class = $click_trigger_class;
$this->target_directory    = $target_directory;
$this->file_uploader_id    = $file_uploader_id;

include_once(dirname(__FILE__) . "/../" . "/view/file_uploader_widget.phtml");
error_reporting(0);
