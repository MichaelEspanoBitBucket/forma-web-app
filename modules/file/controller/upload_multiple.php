<?php

$auth      = new Auth();
$files_dao = new FilesDAO();

$current_user = $auth->getAuth("current_user");

$valid_formats    = unserialize(FILES_EXTENSION);
$target_directory = filter_input(INPUT_POST, "target_directory");

$uploaded_files = array();

$file_count                  = count($_FILES['file']['tmp_name']);
$user_temporary_files_folder = md5(md5($current_user["id"]));
$temporary_files_path        = "images/{$target_directory}/temporary_files/{$user_temporary_files_folder}";
$already_uploaded_files      = array();

//  Create the path if it is not yet existing
if (!is_dir($temporary_files_path)) {
    mkdir($temporary_files_path);
}

for ($i = 0; $i < $file_count; $i ++) {
    $uploaded_file     = $_FILES['file']['tmp_name'][$i];
    $original_filename = $_FILES['file']['name'][$i];
    $splitted_filename = explode(".", $original_filename);
    $extension         = $splitted_filename[count($splitted_filename) - 1];

    if (is_uploaded_file($original_filename)) {
        array_push($already_uploaded_files, $original_filename);
        continue;
    }

    $image_extensions = unserialize(IMG_EXTENSION);

    if (in_array($extension, $image_extensions)) {
        $new_filename = md5(md5(time() + " " + $i)) . "." . $extension;
    } else {
        $new_filename = $original_filename;
    }

    $target_file_path = "{$temporary_files_path}/{$new_filename}";

    move_uploaded_file($uploaded_file, $target_file_path);
    array_push($uploaded_files, array(
        "original_filename_no_extension" => str_replace("." . $extension, "", $original_filename),
        "original_filename" => $original_filename,
        "new_filename" => $new_filename
    ));
}

$this->results = $uploaded_files;
include(dirname(__FILE__) . "/../" . "/view/api_response.phtml");
