<?php
error_reporting(E_ALL);

/* Redirect if not authenticated */
if(!Auth::hasAuth('current_user')){
	http_response_code(401);
	echo "Failed to load data sources. User not logged in.";
}

$auth = Auth::getAuth('current_user');

require_once(realpath('.') . "/library/gi-repositories/DataSourceRepository.php");

$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
$dsRepo = new DataSourceRepository($conn, $auth);

$dataSources = $dsRepo->getAllDataSources();

?>

<?php foreach($dataSources as $dataSource){ ?>
	<li class='data-source-item' data-id='<?php echo $dataSource["id"] ?>'>
		<i class="gi-ui-ms-pointer data-source-toggle fa fa-plus-square-o"></i>
		<span class="li-item-icon db-icon"></span>
		<span class='data-source-label'><?php echo $dataSource["name"] . " (" . $dataSource["type"]["name"] . ")" ?></span>
		<ul class='data-source-entities'></ul>
	</li>
<?php } ?>