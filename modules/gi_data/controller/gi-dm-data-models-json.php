<?php
error_reporting(E_ALL);

/* Redirect if not authenticated */
if(!Auth::hasAuth('current_user')){
	http_response_code(401);
	echo "Failed to load data models. User not logged in.";
}

$auth = Auth::getAuth('current_user');

require_once(realpath('.') . "/library/gi-repositories/DataModelRepository.php");

$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
$dmRepo = new DataModelRepository($conn, $auth);

$dataModels = $dmRepo->getAllDataModels();

echo json_encode($dataModels);
?>

