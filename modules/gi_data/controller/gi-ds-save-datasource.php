<?php
error_reporting(E_ALL);

/* Redirect if not authenticated */
if(!Auth::hasAuth('current_user')){
	http_response_code(401);
	echo "Failed to load chart data. User not logged in.";
} 

$auth = Auth::getAuth('current_user');

require_once(realpath('.') . "/library/gi-repositories/DataSourceRepository.php");
require_once(realpath('.') . "/library/Phinq/bootstrap.php");

$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
$rptRepo = new DataSourceRepository($conn, $auth);

	header('Cache-Control: no-cache, must-revalidate');
	header('Content-type: application/json');

if($_POST["type_id"]==2){
	$dsn = @odbc_connect($_POST["dsn"], "", "");
	if($dsn){
		$rptRepo->addDataSource($_POST["name"],$_POST["type_id"],$_POST["dsn"]);
	}else{
	
		$status['Error'] = "Connection doesn't exist.";
		echo json_encode($status);
	}
}






?>
