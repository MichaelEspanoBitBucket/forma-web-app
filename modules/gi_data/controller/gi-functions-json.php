<?php
error_reporting(E_ALL);
/* Redirect if not authenticated */
if(!Auth::hasAuth('current_user')){
	http_response_code(401);
	echo "Failed to load data sources. User not logged in.";
}

$auth = Auth::getAuth('current_user');

require_once(realpath('.') . "/library/gi-models/Models.php");
require_once(realpath('.') . "/library/gi-repositories/GiqFunctionsRepository.php");

$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
$funcRepo = new GiqFunctionsRepository($conn, $auth);

$functions = $funcRepo->getFunctions();

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

$functionsJson = json_encode($functions);

echo $functionsJson;

?>