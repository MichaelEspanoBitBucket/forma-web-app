<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);

require_once(realpath('.') . "/library/Phinq/bootstrap.php");

use Phinq\Phinq as Phinq;


/* Redirect if not authenticated */
if(!Auth::hasAuth('current_user')){
	http_response_code(401);
	echo "Failed to load chart data. User not logged in.";
} 

$auth = Auth::getAuth('current_user');
$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);

$giqsData = [ "timeGenerated" => null, "dataTable" => [], "giqs" => [] ];
$sqls = [];


$giqsData["giqs"][] = 
	[
		"aggregates"=> [["exp"=>[], "alias"=>"Count"]],
		"groupings"=> [["groupExp"=>[], "dispExp"=>[], "alias"=>"Option"]],
		"optionalGroupings"=> [],
		"filters"=> [["exp"=>[]]],
		"dataSource"=> [ "id"=>77, "name"=>"Formalistics", "type"=>"formalistics" ],
		"entities"=> []
	];
	
$sqls[] = 
	"select o.response as `Option`, count(r.id) as `Count`
	from tb_message_board_survey_responses as r
		inner join 
		tb_message_board_survey_selectable_responses as o
		on r.response_id = o.id 
		inner join 
		tb_message_board_surveys as s
		on r.survey_id = s.id
	where s.id = ?
	group by o.response with rollup";


for($i=0;$i<count($giqsData["giqs"]);$i++){
	
	$stm = $conn->prepare($sqls[$i]);
	$stm->bind_param('i', $_GET["surveyId"]);
	$stm->execute();
	$sqlResult = $stm->get_result()->fetch_all(MYSQLI_ASSOC);
	

	$giqsData["dataTable"][] = $sqlResult;
}

$giqsData["timeGenerated"] = time();

echo json_encode($giqsData);

?>
