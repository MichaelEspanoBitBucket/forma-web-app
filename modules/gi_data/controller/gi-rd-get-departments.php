<?php
	//error_reporting(E_ALL);

	/* Redirect if not authenticated */
	if(!Auth::hasAuth('current_user')){
		http_response_code(401);
		echo "Failed to load data models. User not logged in.";
	}
	
	$auth = Auth::getAuth('current_user');
	
	require_once(realpath('.') . "/library/gi-repositories/ReportRepository.php");
	
	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
	//$personObj = new Person($conn, $auth['id']);
	$rdRepo = new ReportRepository($conn, $auth);
	$search = new Search();
	$data = null;
	
	
	header('Cache-Control: no-cache, must-revalidate');
	header('Content-type: application/json');

	$data = $search->getAllDepartment();
	//var_dump($personObj);
	$data = json_encode($data);
	//$data = $auth['department_id'];
	echo $data;

?>
