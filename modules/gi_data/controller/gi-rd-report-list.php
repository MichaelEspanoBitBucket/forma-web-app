<?php
	error_reporting(E_ALL);

	/* Redirect if not authenticated */
	if(!Auth::hasAuth('current_user')){
		http_response_code(401);
		echo "Failed to load data models. User not logged in.";
	}

	$auth = Auth::getAuth('current_user');
	
	require_once(realpath('.') . "/library/gi-repositories/ReportRepository.php");
	
	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
	$rdRepo = new ReportRepository($conn, $auth);
	//$data = null;
	
	
	header('Cache-Control: no-cache, must-revalidate');
	header('Content-type: application/json');
	$dataTable = [];
	$displayStart = $_POST['iDisplayStart'];
	$displayLength = $_POST['iDisplayLength'];
	$sort = $_POST['iSortCol_0'];
	$sortDir = $_POST['sSortDir_0'];
	$search_value = $_POST['search_value'];
	//$reports = $rdRepo->getAllReports();
	
	switch($_POST['iSortCol_0']){
		
		case 1 : $sort = "name";
			break;
		case 2 : $sort = "creator_name";
			break;
		case 3 : $sort = "date_created";
			break; 
		case 4 : $sort = "date_modified";
			break;
		case 0 : $sort = "date_modified";
			break;
	}
	
	$reports = $rdRepo->getReportsDataTable($displayStart, $displayLength, $sort, $sortDir, $search_value);
	$dataTable['sEcho'] = $_POST['sEcho'];
	$dataTable['data'] = $reports['data'];
	$dataTable['totalReports'] =  $reports['totalReports'];
	$dataTable['totalDisplayRecords'] =  $reports['totalDisplayRecords'];
	
	
	echo json_encode($dataTable);
	// ;

?>
