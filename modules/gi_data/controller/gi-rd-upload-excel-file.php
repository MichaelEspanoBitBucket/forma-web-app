<?php
error_reporting(E_ALL);

/* Redirect if not authenticated */
if(!Auth::hasAuth('current_user')){
	http_response_code(401);
	echo "Failed to load chart data. User not logged in.";
} 

require_once(realpath('.') . "/library/gi-repositories/DataSourceRepository.php");
//require_once(realpath('.') . "/library/gi-libraries/ExcelLib/PHPExcel.php");
// require_once(realpath('.') . "/library/Phinq/bootstrap.php");


$auth = Auth::getAuth('current_user');

$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
$rptRepo = new DataSourceRepository($conn, $auth);



header('Content-Type: text/plain');
$data = array();

if(isset($_GET['files']))
{  
    $error = false;
    $files = array();



    $uploaddir = './images/gi/attachments/';

    foreach($_FILES as $file)
    {
        if(move_uploaded_file($file['tmp_name'], $uploaddir .basename($file['name'])))
        {
            $files[] = $uploaddir .$file['name'];
        }
        else
        {
            $error = true;
        }   
    }

    $data = ($error) ? array('error' => 'There was an error uploading your files') : array('files' => $files);




 }
else
{
    $data = array('success' => 'Form was submitted', 'formData' => $_POST);
   // var_dump($_POST["dsname"]);
   $rptRepo->addDataSource($_POST["dsname"],5,"",$_POST["filenames"][0]);
   
}

echo json_encode($data);

?>
