<?php
	error_reporting(0);
	include_once('forma_dependencies.php');
	/* Redirect if not authenticated */

	if(!Auth::hasAuth('current_user')){
		http_response_code(401);
		echo "Failed to load data models. User not logged in.";
	}

	$auth = Auth::getAuth('current_user');

	$path = $_SERVER["DOCUMENT_ROOT"];

	//include_once($path . "/library/gi-repositories/DataWarehouse.php");
	include_once($path . "/library/gi-repositories/ReportRepository.php");

	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
	$rdRepo = new ReportRepository($conn, $auth);
	//$data = null;
	
	
	header('Cache-Control: no-cache, must-revalidate');
	header('Content-type: application/json');
	//var_dump($_GET['id'])

	$reports = $rdRepo->getReportByUserId($auth['id']);
	//$reports = $rdRepo->getUserDepartmentById($_GET['id']);
	$data = json_encode($reports);
	
	echo $data;
	// ;

?>
