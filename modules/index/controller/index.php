<?php
/*code here*/
$db = new Database();
$fs = new functions();
$date = $fs->currentDateTime();
    /* Redirect user if they already logged */
    if(Auth::hasAuth('current_user'))
    {
        $auth = Auth::getAuth('current_user');
        //$this->view->current_user = Auth::getAuth('current_user');
        if(isset($_GET['redirect_url'])){
            $decode_url = functions::encode_decode_url($_GET['redirect_url'],"decode");
            header('Location: /' . $decode_url); 
        }else if($_GET['type']=="go"){
            $location = $_GET['redirect'];
            $redirect = str_replace("|=%=|", "&", $location);
            header('Location: ' . USER_VIEW .$redirect);
        }else{
            header('Location: ' . USER_VIEW); 
        }
    }
    //remove application cache
    //setcookie('application','');
    /* For user login if correct redirect to home */
    if($_SERVER['REQUEST_METHOD'] == "POST")
    {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $coords = $_POST['coords'];
        $session = new Auth();
        $login = $session->login($username,$password,'email','password','tbuser');
        $login = $db->query("SELECT *
                                FROM tbuser
                                WHERE 
                                    (email={$db->escape($username)} 
                                OR 
                                    username={$db->escape($username)}) 
                                AND 
                                    is_active = 1",
                                "row");
        if($login){
            // error_log($auth_user."SELECT * FROM tbuser WHERE (email={$db->escape($username)} OR username={$db->escape($username)}) AND is_active = 1", 3 , "C:/wamp/www/FormaSysLogRule/logs/est.log");
            if(isset($_GET['redirect_url'])){
                $decode_url = functions::encode_decode_url($_GET['redirect_url'],"decode");
                header('Location: /' . $decode_url); 
            }else if($_GET['type']=="go"){
                $location = $_GET['redirect'];
                $redirect = str_replace("|=%=|", "&", $location);
                header('Location: ' . USER_VIEW .$redirect);
            }else{
                header('Location: /'); 
            }

            $action_description = Logs::createMessage("2","");
                                                        
            $fs->save_audit_logs($db,$login['id'],"2","tbuser",$date,$login['id'],$action_description);
            
            // Save location of user logged in
               
                $insert = array("location"	=>	$coords,
                                "date"		=>	$date,
                                "userID"	=>	$login['id'],
                                "is_active"	=>	1);
                $db->insert("tblocation",$insert);
                
                // Set Available
                $personDoc = new Person($db,$login['id']);
                $personDoc->is_available = '1';
                $personDoc->update();
                
                // Reset Timeout idle
                $personDoc = new Person($db,$login['id']);
                $personDoc->timeout = '0';
                $personDoc->update();
        }else{
            if($_GET['type']=="go"){
                $path = functions::curPageURL("");
                $get_module_param = explode("?",$path);
                header('Location: /login?' . $get_module_param[1] . '?' . $get_module_param[2] . '&login_attempt=1');
            }else if(isset($_GET['redirect_url'])){
                $decode_url = functions::encode_decode_url($_GET['redirect_url'],"decode");
                header('Location: /login?' . $decode_url . '&login_attempt=1');
            }else{
                header('Location: /login?login_attempt=1'); 
            }
        }
    }   
/*code here(end)*/
$this->setLayout();