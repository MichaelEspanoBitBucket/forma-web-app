<?php
/*code here*/
$db = new Database();
$fs = new functions();
$date = $fs->currentDateTime();
    /* Redirect user if they already logged */
    if(Auth::hasAuth('current_user'))
    {
        $auth = Auth::getAuth('current_user');
        $params = $fs->curPageURL("module");
        //$this->view->current_user = Auth::getAuth('current_user');
        if(isset($_GET['redirect_url'])){
            $decode_url = functions::encode_decode_url($_GET['redirect_url'],"decode");
            header('Location: /' . $decode_url); 
        }else if($params == "activation"){
            header('Location: /'); 
        }else if($_GET['type']=="go"){
            $location = $_GET['redirect'];
            $redirect = str_replace("|=%=|", "&", $location);
            header('Location: ' . USER_VIEW . $redirect);
        }else{
            
                $sql = "SELECT * FROM page_settings WHERE userID = {$db->escape($auth['id'])}";
                $page_settings = $db->query($sql,"row");
                $page_settings_nums = $db->query($sql,"numrows");
                if($page_settings_nums == 0){
                    // header('Location: ' . USER_VIEW);
                    if(APP_LANDING_PAGE == "home"){
                        header('Location: ' . USER_VIEW . APP_LANDING_PAGE);
                    }else{
                        if(APP_LANDING_PAGE == "portal" || APP_LANDING_PAGE == "faq" ){
                            header('Location: ' . APP_LANDING_PAGE);
                        }else{
                            //header('Location: ' . '/');
							if(APP_OTHERS_LANDING_PAGE == "others"){
								header('Location: ' . APP_LANDING_PAGE);
							}else{
								header('Location: ' . USER_VIEW . APP_LANDING_PAGE);
							}
							
                        }
                    }
                }else{
                    if($page_settings['page'] == "portal" || $page_settings['page'] == "faq" ){
                        header('Location: ' . $page_settings['page']);
                    }else if($page_settings['page'] == "" && APP_LANDING_PAGE == "home" ){
                        header('Location: ' .  APP_LANDING_PAGE);
                    }else if($page_settings['page'] == "" && (APP_LANDING_PAGE == "portal" || APP_LANDING_PAGE == "faq") ){
                        header('Location: ' . APP_LANDING_PAGE);
                    }else if($page_settings['type'] == "others"){
                        header('Location: ' . $page_settings['page']);
                    }else if($page_settings['type'] == ""){
                        header('Location: ' . APP_LANDING_PAGE);
                    }else{
                        header('Location: ' . USER_VIEW .  $page_settings['page']);
                    }
                    
                }
        }
    }
    //remove application cache
    //setcookie('application','');
    /* For user login if correct redirect to home */

    // For Guest
    // Walang gagamit ng User_level_id = 7
    ///login?trackNo=L0010&requestID=10&formID=607&type=go&user_type=guest&log_type=user&autologged=yes
    if(isset($_GET['user_type']) && isset($_GET['log_type']) && isset($_GET['autologged']) && isset($_GET['formID'])){
        

        $user_type = $_GET['user_type'];
        $log_type = $_GET['log_type'];
        $autologged = $_GET['autologged'];
        $form_id = $_GET['formID'];

        if($autologged == "yes" && $user_type == "guest" && $log_type == "user"){

            $session = new Auth();

            $get_company_guest_info = $db->query("SELECT * FROM tb_workspace w
                LEFT JOIN tbuser u ON w.company_id=u.company_id
                WHERE w.id={$db->escape($form_id)} AND email={$db->escape(GUEST_EMAIL)}
            ","row");

            $company_id = $get_company_guest_info['company_id'];
            $username = $get_company_guest_info['email'];
            $password = $get_company_guest_info['password'];

            $new_url = USER_VIEW . 'workspace?view_type=request&trackNo='.$_GET['trackNo'].'&formID='.$_GET['formID'].'&requestID='.$_GET['requestID'].'&authentication='.$_GET['authentication'].'&type='.$_GET['type'].'&log_type='.$_GET['log_type'].'&autologged='.$_GET['autologged'];
     
            $login = $session->auto_login($username,$password,'email','password','tbuser',array("url"=>$new_url));

           header('Location: ' . $new_url);

        }
    }

    


    if($_SERVER['REQUEST_METHOD'] == "POST")
    {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $coords = $_POST['coords'];
        $session = new Auth();
        Auth::destroyAuth('getLeftBar'); // destroy leftbar session
        Auth::destroyAuth('captcha_code_proceed');
        if(trim($username) != "" && trim($password) != ""){
        
        // Active Directory
        if(ALLOW_ACTIVE_DIRECTORY == "1"){
            $ldapMan = LDAPManager::getInstance();
            $ldapMan->setHost(AD_IP_ADDRESS);
            $results = $ldapMan->login($username, $password);
        }
                     
                    $login = $session->login($username,$password,'email','password','tbuser');

                    if($login == 'need register'){
                        if(ALLOW_ACTIVE_DIRECTORY == "1"){
                            // Active Directory
                            $pw = $fs->base_encode_decode('encrypt',$password);
                            $login_num = $db->query("SELECT * FROM tbuser WHERE email={$db->escape($username)} OR username={$db->escape($username)}","numrows");
                            $ldapMan = LDAPManager::getInstance();
                            $ldapMan->setHost(AD_IP_ADDRESS);
                            if($login_num == "0"){
                                
                                //header('Location: /ad_registration?email=' . urlencode($username) . '&cred=' . $pw . '&type=ad_user_reg');
                                header('Location: /login?login_attempt=1&login_type=ad'); 


                            }else{
                                $results = $ldapMan->login($username, $password);
                                if($results['results'] == "Login Success"){
                                    $login_1 = $db->query("SELECT * FROM tbuser WHERE email={$db->escape($username)} OR username={$db->escape($username)}","row");
                                    $user_password = functions::encrypt_decrypt("decrypt",$login_1['password']);
                                    $login = $session->login($login_1['email'],$user_password,'email','password','tbuser');
                                    header('Location: /');
                                }else{
                                    if(isset($_GET['redirect_url'])){
                                        $decode_url = functions::encode_decode_url($_GET['redirect_url'],"encode");
                                        header('Location: /login?redirect_url=' . $decode_url); 
                                    }else if($_GET['type']=="go"){
                                        $location = $_GET['redirect'];
                                        $redirect = str_replace("|=%=|", "&", $location);
                                        header('Location: ' . USER_VIEW .$redirect);
                                    }else{
                                        header('Location: /login'); 
                                    }
                                   
                                }
                            }
                        }
                        
                    }elseif($login == 'error'){    
                        if(isset($_GET['redirect_url'])){
                            $decode_url = functions::encode_decode_url($_GET['redirect_url'],"encode");
                            header('Location: /login?redirect_url=' . $decode_url); 
                        }else if($_GET['type']=="go"){
                            $location = $_GET['redirect'];
                            $redirect = str_replace("|=%=|", "&", $location);
                            header('Location: ' . USER_VIEW .$redirect);
                        }else{
                            header('Location: /login'); 
                        }
                    }else{
                        // Inactive
                            $user_password = functions::encrypt_decrypt("encrypt",$password);
                            $inactive = $db->query("SELECT *
                                FROM tbuser
                                WHERE email={$db->escape($username)} AND
                                password={$db->escape($user_password)} AND is_active = 0","row");
                            $auth_user = $db->query("SELECT *
                                FROM tbuser
                                WHERE email={$db->escape($username)} AND
                                password={$db->escape($user_password)} AND is_active = 1","row");
                            
                        if($login){
                            
                            // For Guest
                            $url = $fs->encode_decode_url($fs->curPageURL("module_parameter"),"encode");
                            
                            $query_str = parse_url($url, PHP_URL_QUERY);
                            parse_str($query_str, $query_params);
                            if(ALLOW_GUEST == "1"){
                                if(isset($query_params) && isset($query_params['authentication']) && isset($query_params['type']) && isset($query_params['log_type'])){
                                    if($query_params['authentication'] == "login" && $query_params['type'] == "guest" && $query_params['log_type'] == "user" ){
                                        $json_encode = json_encode(array("data_action"  =>  "guest",
                                                                        "guest_email"  =>  $login['email'],
                                                                        "guest_url"  =>  $url));
                                                        
                                                        $insert = array("guest_user_id"     =>  $login['id'],
                                                                        "guest_json"        =>  $json_encode,
                                                                        "guest_type"        =>  "guest",
                                                                        "guest_date"        =>  $date,
                                                                        "guest_added_by"    =>  $login['id'],
                                                                        "guest_formID"      =>  $query_params['formID'],
                                                                        "guest_requestID"   =>  $query_params['requestID'],
                                                                        "guest_trackNo"     =>  "",
                                                                        "guest_is_active"   =>  "1");
                                                        $db->insert("tb_guest",$insert);
                                    }
                                }
                            }
                            
                            
                            
                            if(isset($_GET['redirect_url'])){
                                $decode_url = functions::encode_decode_url($_GET['redirect_url'],"encode");
                                header('Location: /login?redirect_url=' . $decode_url); 
                            }else if($_GET['type']=="go"){
                                $location = $_GET['redirect'];
                                $redirect = str_replace("|=%=|", "&", $location);
                                header('Location: ' . USER_VIEW . $redirect);
                            }else{
                                header('Location: /login'); 
                            }
                            
                            $action_description = Logs::createMessage("2","");
                                                        
                            $fs->save_audit_logs($db,$auth_user['id'],"2","tbuser",$date,$auth_user['id'],$action_description);
                            
                            // Save location of user logged in
                                
                                $insert = array("location"	=>	$coords,
                                                "date"		=>	$date,
                                                "userID"	=>	$login['id'],
                                                "is_active"	=>	1);
                                $db->insert("tblocation",$insert);
                                
                                // Set Available
                                $personDoc = new Person($db,$login['id']);
                                $personDoc->is_available = '1';
                                $personDoc->update();
                                
                                // Reset Timeout idle
                                $personDoc = new Person($db,$login['id']);
                                $personDoc->timeout = '0';
                                $personDoc->update();
                        }else{
                            if($inactive){
                                header('Location: /login?login_attempt=1&account=0');
                            }else{
                                if($_GET['type']=="go"){
                                    $path = functions::curPageURL("");
                                    $get_module_param = explode("?",$path);
                                    header('Location: /login?' . $get_module_param[1] . '?' . $get_module_param[2] . '&login_attempt=1&account=1');
                                }else if(isset($_GET['redirect_url'])){
                                    $decode_url = functions::encode_decode_url($_GET['redirect_url'],"encode");
                                    header('Location: /login?redirect_url=' . $decode_url . '&login_attempt=1&account=1');
                                }else{
                                    header('Location: /login?login_attempt=1&account=1'); 
                                }
                            }
                            
                        }
                    }
        }else{
            header('Location: /login?login_attempt=1&account=1'); 
        }
    }   
/*code here(end)*/
$this->setLayout();