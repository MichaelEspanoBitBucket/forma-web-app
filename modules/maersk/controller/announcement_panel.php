<?php

$get_announcements_query = "SELECT announcement FROM 20_tbl_messageboardannouncementmaintenance 
                            WHERE status = 'Published'
                            ORDER BY datecreated DESC LIMIT 1;";

$db = new Database();

$query_result = $db->query($get_announcements_query, "row");

$this->announcement = $query_result["announcement"];
$this->renderAsPartialView("/modules/maersk/view/announcement_panel.phtml");
