<?php

// error_reporting(E_ALL);


$current_user = Auth::getAuth("current_user");
$thread_id    = filter_input(INPUT_POST, "thread_id");
$thread_name  = filter_input(INPUT_POST, "thread_name");
$messaging_facade = new MessagingFacade();

$parameters_valid = true;

$this->results = null;

// <editor-fold desc="Validation" defaultstate="collapsed">
if (!$current_user) {
    $this->error         = "NOT_LOGGED_IN";
    $this->error_message = "The user must be logged in to use this service";

    $parameters_valid = false;
}

if (!$thread_id) {
    $this->error         = "INVALID_PARAMETERS";
    $this->error_message = "No thread id (thread_id) provided";

    $parameters_valid = false;
}

// </editor-fold>

if ($parameters_valid) {
    try {    
        $unsubscribe_results = $messaging_facade->changeGroupName($thread_id, $thread_name);
        $this->results       = $unsubscribe_results;
    } catch (Exception $e) {
        $this->error         = "CHANGE_GROUP_NAME_ERROR";
        $this->error_message = $e->getMessage();
    }
}

include_once(dirname(__FILE__) . "/../" . "/view/api_response.phtml");

error_reporting(0);
