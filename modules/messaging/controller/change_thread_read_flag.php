<?php

//error_reporting(E_ALL);

//  validate parameters
$parameters_are_valid = true;

$thread_id    = filter_input(INPUT_POST, "thread_id");
$read_flag    = filter_input(INPUT_POST, "read_flag");
$current_user = Auth::getAuth("current_user");

//  <editor-fold desc="Validation" defaultstate="collapsed">

$this->results       = null;
$this->error         = null;
$this->error_message = null;

if (!$thread_id || $read_flag == null || !$current_user) {
    $this->error          = "INVALID_PARAMETERS";
    $parameters_are_valid = false;
}

if (!$thread_id) {
    $this->error_message = "Missing thread id";
}

if (!$read_flag) {
    $this->error_message = "Missing read flag id";
} else if (!($read_flag == MessagingDAO::THREAD_READ || $read_flag == MessagingDAO::THREAD_UNREAD)) {
    $this->error_message  = "Invalid read flag";
    $parameters_are_valid = false;
}

if (!$current_user) {
    $this->error         = "NOT_LOGGED_IN";
    $this->error_message = "You have to be logged in to use this service";
}
// </editor-fold>

if ($parameters_are_valid) {
    $messaging_dao = new MessagingDAO();

    $this->current_user_id    = $current_user["id"];
    $this->thread_subscribers = $messaging_dao->getThreadSubscribers($thread_id);

//  check if the current user is a subscriber of this thread and collect other subscribers
    $current_user_is_subscribed = false;
    foreach ($this->thread_subscribers AS $subscriber) {
        if ($subscriber["id"] == $this->current_user_id) {
            $current_user_is_subscribed = true;
            break;
        }
    }    
    
    if ($current_user_is_subscribed) {
        $this->results = $messaging_dao->changeThreadReadFlag(
                $thread_id, $current_user["id"], MessagingDAO::THREAD_READ
        );
    } else {
        $this->error         = "NOT_AUTHORIZED";
        $this->error_message = "You cannot access this thread";
    }
    
}

include_once(dirname(__FILE__) . "/../" . "/view/api_response.phtml");

//error_reporting(0);