<?php

//error_reporting(E_ALL);
//  get parameters from post or partial view
if ($this->loaded_as_partial_view && $this->partial_view_data) {
    $thread_id          = $this->partial_view_data["thread_id"];
    $thread_fetch_start_index   = 0;
    $thread_fetch_end_index     = $thread_fetch_start_index + 10;
    $requested_response = "HTML";
} else {
    $parameters         = filter_input_array(INPUT_GET);
    $thread_id          = $parameters["threadId"];
    $thread_fetch_start_index = array_key_exists("index", $parameters) ?
            $parameters["index"] : 0;
    $thread_fetch_end_index = $thread_fetch_start_index + 10;
    $requested_response = array_key_exists("requested_response", $parameters) ?
            strtoupper($parameters["requested_response"]) : "HTML";
}

$this->thread_id    = $thread_id;
$this->thread_title = "";
$this->thread_fetch_start_index = $thread_fetch_start_index;
$this->thread_fetch_end_index = $thread_fetch_end_index;

if ($thread_id != 0) {

    $current_user  = Auth::getAuth("current_user");
    $messaging_dao = new MessagingDAO();

    $this->current_user_id                 = $current_user["id"];
    $this->thread_subscribers              = $messaging_dao->getThreadSubscribers($thread_id);
    $this->thread_subscriber_id_list       = [];
    $this->thread_other_subscriber_id_list = [];

    //  check if the current user is a subscriber of this thread and collect other subscribers
    $current_user_is_subscribed   = false;
    $other_subscribers_full_name  = array();
    $other_subscribers_first_name = array();

    foreach ($this->thread_subscribers AS $subscriber) {
        array_push($this->thread_subscriber_id_list, $subscriber["id"]);
        if ($subscriber["id"] == $this->current_user_id) {
            $current_user_is_subscribed = true;
        } else {
            array_push($other_subscribers_full_name, $subscriber["display_name"]);
            array_push($other_subscribers_first_name, $subscriber["first_name"]);

            array_push($this->thread_other_subscriber_id_list, $subscriber["id"]);
        }
    }

    $this->current_user_is_subscribed = $current_user_is_subscribed;

    if ($current_user_is_subscribed) {

        $thread_title = "";
        if (count($other_subscribers_full_name) > 1) {
            //  get the first name of the first two subscribers
            $subscriber_1 = $other_subscribers_first_name[1];
            $subscriber_2 = $other_subscribers_first_name[0];

            //  build the thread title with the 2 subscribers
            $thread_title = "{$subscriber_1}, {$subscriber_2}";

            //  if there are more, append an " and (n-2) other(s)" to the title 
            //  (where n is the number of subscribers)
            if (count($other_subscribers_full_name) > 2) {
                $recipient_count = (count($other_subscribers_first_name) - 2);
                $thread_title .= " and {$recipient_count} "
                        . ($recipient_count > 1 ? "others" : "other");
            }
        } else {
            $thread_title = implode(",", $other_subscribers_full_name);
        }        
        
        $this->thread_other_subscriber_names = $other_subscribers_full_name;
        $this->thread_title                  = $thread_title;
        $this->messages                      = $messaging_dao->getThreadMessages($thread_id, $current_user["id"]);        
        
        //  generate image url for each users
        for ($i = 0; $i < count($this->messages); $i ++) {
            $author_id              = $this->messages[$i]["author_id"];
            $author_image_extension = $this->messages[$i]["author_image_extension"];

            $this->messages[$i]["author_image_url"] = $messaging_dao->getProcessedImageURL($author_id, $author_image_extension);
        }
    } else {
        $this->error         = "NOT_AUTHORIZED";
        $this->error_message = "You are not subscribed to this message";
    }
} else {
    $this->error         = "NOT_FOUND";
    $this->error_message = "No thread has id of 0.";
}

if ($requested_response == "HTML") {
    include_once(dirname(__FILE__) . "/../" . "/view/chat_thread_messages.phtml");
} else if ($requested_response == "JSON") {
    if (!$this->error) {
        $this->results = array(
            "thread_title_full" => $this->thread_title_full,
            "thread_title" => $this->thread_title,
            "current_user" => $current_user['id'],
            "messages" => $this->messages
        );
    }
    echo json_encode($this->results,JSON_PRETTY_PRINT);
    // include(dirname(__FILE__) . "/../" . "/view/api_response.phtml");
}

//error_reporting(0);
