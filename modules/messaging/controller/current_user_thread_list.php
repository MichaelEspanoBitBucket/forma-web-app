<?php

//error_reporting(E_ALL);

$this->threads = array();

$parameters         = filter_input_array(INPUT_POST);
//$parameters         = filter_input_array(INPUT_GET);
$requested_response = array_key_exists("requested_response", $parameters) ?
        strtoupper($parameters["requested_response"]) : "HTML";

/* code here */
/* Redirect user if they already logged */
if (Auth::hasAuth('current_user')) {
    //$this->view->current_user = Auth::getAuth('current_user');
    //  load initial data
    $current_user = Auth::getAuth('current_user');

    //  get parameters from post or partial view
    if ($this->loaded_as_partial_view && $this->partial_view_data) {
        $thread_fetch_start_index = $this->partial_view_data["thread_fetch_start_index"];
        $thread_fetch_count       = $this->partial_view_data["thread_fetch_count"];
    } else {
        $thread_fetch_start_index = filter_input(INPUT_POST, "thread_fetch_start_index") ?
                filter_input(INPUT_POST, "thread_fetch_start_index") : 0;
        $thread_fetch_count       = filter_input(INPUT_POST, "thread_fetch_count") ?
                filter_input(INPUT_POST, "thread_fetch_count") : 20;
    }

    $messaging_dao = new MessagingDAO();
    $threads       = $messaging_dao->getUserThreadList(
            $current_user["id"], $thread_fetch_start_index, $thread_fetch_count
    );

    $this->thread_fetch_start_index   = $thread_fetch_start_index;
    $this->default_thread_fetch_count = $thread_fetch_count;
    $this->total_thread_count         = $messaging_dao->getUserTotalThreadCount($current_user["id"]);

    //  get all the users involved in each thread
    $all_subscribers = array();
    foreach ($threads AS $thread) {
        $thread_recipient_id_list = explode(",", $thread["subscribers"]);
        foreach ($thread_recipient_id_list AS $thread_subscriber) {
            $all_subscribers[] = $thread_subscriber;
        }
    }

    $all_subscribers_set = array_unique($all_subscribers);
    $subscribers         = $messaging_dao->getUsersByIdList($all_subscribers_set);

    //  map the subscribers by id for easy access
    $mapped_subscribers = array();
    foreach ($subscribers AS $subscriber) {
        $mapped_subscribers[$subscriber["id"]] = $subscriber;
    }

    //  assign the thread titles and images using the subscriber data
    $view_ready_threads = array();
    foreach ($threads AS $thread) {
        $view_ready_thread = $thread;

        if ($thread["recipients"]) {

            $thread_recipient_id_list = explode(",", $thread["recipients"]);
            if (count($thread_recipient_id_list) > 1) {
                //  get the first name of the first two subscribers
                $subscriber_1 = $mapped_subscribers[$thread_recipient_id_list[0]]["first_name"];
                $subscriber_2 = $mapped_subscribers[$thread_recipient_id_list[1]]["first_name"];

                $thread_title = "";
                //  if there are more, add their count
                if (count($thread_recipient_id_list) > 2) {
                    $recipient_count = (count($thread_recipient_id_list) - 2);
                    $thread_title    = "and {$recipient_count} "
                            . ($recipient_count > 1 ? "others" : "other");
                }

                $view_ready_thread["title"] = "{$subscriber_1}, {$subscriber_2} {$thread_title}";
            } else if (count($thread_recipient_id_list) == 1) {
                $recipient = $mapped_subscribers[$thread_recipient_id_list[0]];

                $view_ready_thread["title"]     = $recipient["display_name"];
                $view_ready_thread["image_url"] = $messaging_dao->getProcessedImageURL($recipient["id"], $recipient["image_extension"]);
            }
        } else {
            //  no recipients mean this message is sent to self
            $view_ready_thread["title"] = "You";
        }

        array_push($view_ready_threads, $view_ready_thread);
    }

    if ($requested_response === "HTML") {
        //  pass in data available to view
        $this->threads = $view_ready_threads;
        include_once(dirname(__FILE__) . "/../" . "/view/current_user_thread_list.phtml");
    } else if ($requested_response === "JSON") {
        $this->results = $view_ready_threads;
        include(dirname(__FILE__) . "/../" . "/view/api_response.phtml");
    }
} else {
    functions::url_redirect();
}
