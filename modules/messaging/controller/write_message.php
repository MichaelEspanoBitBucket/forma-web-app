<?php

header('Content-Type: text/plain');
//error_reporting(E_ALL);

if (Auth::hasAuth('current_user')) {
    //  load initial data
    $current_user    = Auth::getAuth('current_user');
    $current_user_id = $current_user["id"];

    $this->current_user_id = $current_user_id;

    $parameters = filter_input_array(INPUT_POST);
    // print_r($parameters);
    $message    = htmlentities($parameters["message"]);
    // $message = $parameters['message'];
    if (array_key_exists("attachments", $parameters)) {
        $attachments = $parameters["attachments"];
    } else {
        $attachments = null;
    }

    $files_dao        = new FilesDAO(); 
    $messaging_dao    = new MessagingDAO();
    $users_dao        = new UsersDAO();
    $messaging_facade = new MessagingFacade();
    // var_dump($parameters);

    if (array_key_exists("thread_id", $parameters)) {
        $thread_id = $parameters["thread_id"];

        //  TODO: move this to facade
        $new_message = $messaging_dao->writeMessage($message, $thread_id, $current_user_id);

        if ($attachments) {
            $moved_attachment_list = $files_dao->moveAttachments(
                    $new_message["id"], $current_user_id, $attachments, "message_attachments"
            );
            $messaging_dao->recordAttachments($new_message["id"], $moved_attachment_list);

            $attachment_url_list = array();
            foreach ($moved_attachment_list AS $moved_attachment) {
                array_push($attachment_url_list, $moved_attachment["url"]);
            }

            $new_message["attachment_url_list"] = implode(",", $attachment_url_list);
            // $parameters['author_image_url'] = $messaging_dao->getProcessedImageURL($author_id, $author_image_extension);
        }
    } else if (array_key_exists("recipients", $parameters)) {
        if (is_array($parameters["recipients"])) {
        //            $recipients = $parameters["recipients"];
            $raw_recipients = $parameters["recipients"];
        } else {
        //            $recipients = json_decode($parameters["recipients"]);
            $raw_recipients = json_decode($parameters["recipients"]);
        }
  
        $recipients  = $users_dao->extractUserIdListFromSubscribers($raw_recipients);
        // $parameters['author_image_url'] = $messaging_dao->getProcessedImageURL($author_id, $author_image_extension);
        //        var_dump($recipients);
        //        exit();
        $new_message = $messaging_facade->writeMessage($message, $current_user_id, $recipients, $attachments);
    } else {
        //  TODO: return error here
        echo "error, invalid parameters, no thread_id or recipients";
        exit();
    }

    $requested_response = array_key_exists("requested_response", $parameters) ?
            strtoupper($parameters["requested_response"]) : "HTML";

    if ($requested_response == "HTML") {

        $author_id              = $new_message["author_id"];
        $author_image_extension = $new_message["author_image_extension"];

        
        $new_message["author_image_url"] = $messaging_dao->getProcessedImageURL($author_id, $author_image_extension);

        $this->renderAsPartialView("/modules/messaging/view/thread_message_view.phtml", array(
            "message" => $new_message
        ));
    } else if ($requested_response == "JSON") {
        $author_id              = $new_message["author_id"];
        $author_image_extension = $new_message["author_image_extension"];
        $new_message["author_image_url"] = $messaging_dao->getProcessedImageURL($author_id, $author_image_extension);
        $this->results = $new_message;
        
        include(dirname(__FILE__) . "/../" . "/view/api_response.phtml");
    }
}

error_reporting(0);
