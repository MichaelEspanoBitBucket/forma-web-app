<?php

$db = new Database();

$formID = $_POST['FormID'];
$insert_result = $_POST['ID'];
$node_id = $_POST['Node_ID'];

$myRequest = new Request();
$myRequest->load($formID, $insert_result);

$requestedFields = array();
$fieldResults = $myRequest->fields;
$date = $this->currentDateTime();
$fieldEnabled = json_decode($_POST['fieldEnabled']);
$reservedFields = array("TrackNo", "Requestor", "Status", "Processor", "LastAction", "DateCreated", "DateUpdated", "CreatedBy", "UpdatedBy", "Unread", "Node_ID", "Workflow_ID"
    , "fieldEnabled", "fieldRequired", "fieldHiddenValues", "imported");


foreach ($fieldResults as $field) {

    if (is_array($_POST[$field['COLUMN_NAME']])) {
        //  arrays will be converted to serialized array before storage to database
        $requestedFields[$field['COLUMN_NAME']] = join(",", $_POST[$field['COLUMN_NAME']]);
    } else if (($field['COLUMN_NAME'] == 'DateCreated' && $insert_result == '0') || $field['COLUMN_NAME'] == 'DateUpdated') {
        $requestedFields[$field['COLUMN_NAME']] = $date;
    } else {
        //  only save non null fields
        if ($_POST[$field['COLUMN_NAME']] != "null" && $_POST[$field['COLUMN_NAME']] != null) {
            $requestedFields[$field['COLUMN_NAME']] = $_POST[$field['COLUMN_NAME']];
        }
    }
}

if ($insert_result == '0') {
    //  insert new request
    $myRequest->data = $requestedFields;
    $myRequest->save();
} else {
    //  update request
    if ($node_id == 'Cancel') {
        $requestedFields['Status'] = 'Cancelled';
        $requestedFields['Processor'] = '';
        $requestedFields['LastAction'] = '';
    }

    if ($node_id == 'Save' || $node_id == 'Cancel') {
        $logDoc = new Request_Log($db);
        $logDoc->form = $formID;
        $logDoc->request_id = $insert_result;

        if ($node_id == 'Save') {
            $logDoc->details = 'Saved';
        } else {
            $logDoc->details = 'Cancelled';
        }

        $logDoc->created_by_id = $auth['id'];
        $logDoc->date_created = $date;
        $logDoc->save();
    }

    $myRequest->data = $requestedFields;
    $myRequest->modify();
}

if ($node_id != 'Save' && $node_id != 'Cancel') {
    $myRequest->processWF();
}

$json = array("status" => "SUCCESS", "message" => 'Successfully executed action on server.');
echo json_encode($json);
//print_r($myRequest->data);