<?php
$db = new Database();
$auth = Auth::getAuth('current_user');
$companyID = $auth['company_id'];
$formID = $_POST['formId'];
$type = $_POST['type'];
// $formID = 1;
if(Auth::hasAuth('current_user')){
	$strSQL = "SELECT ws.date_updated as lastModified, ws.Company_ID as companyOwner, ws.ID as formId, ws.form_name as formName, 
				ws.MobileJsonData as fields, wf.id as workflow_id, ws.form_content as form_content, ws.form_json as form_json,
	            wfo.buttonStatus as actions, wfo.fieldEnabled as fieldEnabled, wfo.fieldRequired as fieldRequired, 
	            wfo.fieldHiddenValue as fieldHiddenValue,  ws.form_table_name as tableName, ws.form_buttons as buttons FROM tb_workspace as ws 
	            LEFT JOIN tbworkflow wf on ws.ID = wf.form_id  LEFT JOIN tbworkflow_objects wfo on wfo.workflow_id = wf.id
	            WHERE ws.ID = {$db->escape($formID)} AND ws.form_name <> '' and ws.Is_Active = 1 AND ws.Company_ID = {$db->escape($companyID)} 
	            AND wf.is_active = 1 AND wfo.type_rel= 1";
	$getForm = $db->query($strSQL,"row");
        
	$json_action =  json_decode($getForm['actions']);
	$json_toMobility = array();
	foreach ($json_action as $key => $value) {
		$array2 = array("label"=>$json_action->{$key}->{"button_name"},
						"action"=>$json_action->{$key}->{"child_id"});
		 array_push($json_toMobility, $array2);
		// echo $json_action->{$key}->{"button_name"};
		// echo $json_action->{$key}->{"child_id"};
	}
	$formID = $getForm['formId'];
	$folderPath = 'images/formLogo/'.md5(md5($formID));
	$filePath = "";
	if( is_dir( $folderPath ) ) {
		$filePath = '/images/formLogo/'. md5(md5($formID)) .'/'.'small.png';
	}

        $form_json = json_decode($getForm['form_json']);
        $fields_header = $form_json->{'form_json'}->{'collected_data_header_info'}->{'listedchoice'};
        
	$json_fields = json_decode($getForm['fields']);
	if($type=="web"){
		$json = array("lastModified"=>$getForm['lastModified'],
				"companyOwner"=>$getForm['companyOwner'],
				"tableName"=>$getForm['tableName'],
				"WorkflowId"=>$getForm['workflow_id'],
				"form_content"=>$getForm['form_content'],
				"formId"=>$formID,
				"formName"=>$getForm['formName'],
				"formImage"=>$filePath,
				"fields"=>$json_fields->{'fields'},
				"actions"=>$json_toMobility);
	}elseif($type=="mobile"){
		$json = array("lastModified"=>$getForm['lastModified'],
				"companyOwner"=>$getForm['companyOwner'],
				"tableName"=>$getForm['tableName'],
				"WorkflowId"=>$getForm['workflow_id'],
				"formId"=>$formID,
				"formName"=>$getForm['formName'],
				"formImage"=>$filePath,
				"fields"=>$json_fields->{'fields'},
				"fieldEnabled"=>$getForm['fieldEnabled'],
				"fieldRequired"=>$getForm['fieldRequired'],
				"fieldHiddenValue"=>$getForm['fieldHiddenValue'],
				"fieldsHeader"=>$fields_header,
				"actions"=>$json_toMobility);
	}
	echo "".json_encode($json);
}