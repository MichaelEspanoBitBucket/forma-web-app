<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$db = new Database();
$auth = Auth::getAuth('current_user');
if (Auth::hasAuth('current_user')) {
    //  get service parameters
    $userId = $_GET['userId'];
    // User Queries    
    $table = "tbuser";
    $id_encrypt = md5(md5($userId));
    $size = "small";
    //  get the user info
    $query = "SELECT id, email, display_name as displayName, extension as imageExtension FROM tbuser WHERE id = {$db->escape($userId)}";

    $user = $db->query($query, "row");
    $user['description'] = "";
    $user['imageURL'] = '/images/formalistics/' . $table . '/' . $id_encrypt . '/' . $size . '_' . $id_encrypt . '.' . $user['imageExtension'];

    echo json_encode($user);
}