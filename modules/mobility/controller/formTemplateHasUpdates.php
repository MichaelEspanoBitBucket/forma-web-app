<?php

$db = new Database();
$auth = Auth::getAuth('current_user');
if (Auth::hasAuth('current_user')) {
    $companyId = $auth['company_id'];
    $query = "SELECT w.date_updated as lastModified, w.id as formId, w.form_name as formName FROM tb_workspace w LEFT JOIN tbworkflow wf on wf.form_id = w.id  WHERE company_id = {$db->escape($companyId)} AND wf.is_active =1 AND MobileJsonData <> '' ";
    $getFormTemplates = $db->query($query, "array");
    $formTemplateUpdates = array("status" => "SUCCESS",
        "hasUpdates" => true, "downloadableForms" => array());
    foreach ($getFormTemplates as $data) {
        $json = array("formId" => $data['formId'],
            "formName" => $data['formName'],
            "lastModified" => $data['lastModified']);
        array_push($formTemplateUpdates['downloadableForms'], $json);
    }
    //print_r($json_return);
    echo json_encode(array(
        'status' => 'SUCCESS',
        'data' => $formTemplateUpdates
    ));
} else {
    echo json_encode(array(
        'status' => 'ERROR_USER_NOT_LOGGED_IN',
        'data' => NULL
    ));
}