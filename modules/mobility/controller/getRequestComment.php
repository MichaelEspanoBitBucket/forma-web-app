<?php
$db = new Database();
$auth = Auth::getAuth('current_user');
if(Auth::hasAuth('current_user')){
	$request_id = $_GET['request_id'];
	$form_id = $_GET['form_id'];
	if(isset($request_id) && isset($form_id)){
		$getComment = $db->query("SELECT u.id, u.first_name, u.last_name,c.comment as text, c.date_posted as dateCreated FROM tbcomment c
                                 LEFT JOIN tbuser u on c.postedBy=u.id
                                 WHERE (c.post_id={$db->escape($request_id)} AND c.fID={$db->escape($form_id)}) ORDER BY c.id DESC","array");
		$json = array();
		foreach ($getComment as $value) {
			array_push($json, array("authorId"=>$value['id'],
                                                "author"=>$value['first_name']." ".$value['last_name'],
						"dateCreated"=>$value['dateCreated'],
						"text"=>$value['text']));
		}
		echo json_encode($json);
	}else{
		$json = array("status"=>"ERROR");
		echo json_encode($json);
	}
}
?>