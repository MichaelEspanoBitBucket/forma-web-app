<?php
$db = new Database();
$auth = Auth::getAuth('current_user');
if(Auth::hasAuth('current_user')){

	$tblName = $_POST['tableName'];//"1_tbl_sampleform";
	$id = $_POST['formId'];
	// $id = 1;
	$userID = $auth['id'];
        
	$own_request = array();
	$forApprovalRequests = array();
	$viewerRequests = array();	

	$search = new Search();
	$getRequest = $search->getManyRequest("","0",$id);


	foreach ($getRequest as $request) {
		$processors = explode(",", $request['processors_id']);

        $processor_bool = array_search($userID, $processors)."";



        $hasAction = 0;
        if($request['LastAction']!="" && $request['LastAction']!="null"){
        	$hasAction = 1;
        }
        //get own request
        if($request['requestor_id'] == $userID){

            array_push($own_request, array("trackNo"=>$request['TrackNo'], "hasAction"=>$hasAction,"DateUpdated"=>$request['DateUpdated']));
        }

        //get processor request
        if($processor_bool != ""){
        	array_push($forApprovalRequests,array("trackNo"=>$request['TrackNo'], "hasAction"=>$hasAction,"DateUpdated"=>$request['DateUpdated']));	
        }

        //push all to viewers
        	array_push($viewerRequests, array("trackNo"=>$request['TrackNo'], "hasAction"=>$hasAction,"DateUpdated"=>$request['DateUpdated']));
	}

	$dataArray = array("ownRequests"=>$own_request,
						"forApprovalRequests"=>$forApprovalRequests,
						"viewerRequests"=>$viewerRequests);

	echo json_encode($dataArray);
}










// 	//$position = $auth['company_id'];
// 	//GET OWN REQUEST
// 	$strSqlOwnRequest = "SELECT * FROM $tblName WHERE Requestor = $userID";
// 	$queryOwnRequest = $db->query($strSqlOwnRequest,"array");
// 	$own_request = array();
// 	foreach ($queryOwnRequest as $data) {
// 		array_push($own_request, $data['TrackNo']);
// 	}
// 	//$strSqforApprovalRequests = "SELECT * FROM $tblName WHERE FIND_IN_SET($userID,Processor)";
// 	$strSqforApprovalRequests = "SELECT * FROM $tblName";
// 	//echo $strSqforApprovalRequests;
// 	$queryforApprovalRequests = $db->query($strSqforApprovalRequests,"array");
// 	//echo count(queryforApprovalRequests);
// 	$forApprovalRequests = array();
// 	foreach ($queryforApprovalRequests as $data) {
// 		array_push($forApprovalRequests, $data['TrackNo']);
// 	}
// 	$dataArray = array("ownRequests"=>$own_request,
// 						"forApprovalRequests"=>$forApprovalRequests);
// 	echo json_encode($dataArray);
