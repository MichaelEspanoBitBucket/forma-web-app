<?php

//mobility login

$email = $_POST['email'];
$password = $_POST['password'];
$session = new Auth();
$login = $session->login($email, $password, 'email', 'password', 'tbuser');

$db = new Database();

if ($login) {

    $person = new Person($db, $login['id']);

    //  compute the image link
    $encryptID = md5(md5($login['id']));
    $path = $encryptID . "." . "/small_" . $encryptID . "." .$person->extension;
    $imageURL = "/images/formalistics/tbuser/" . $path;    

    $user = array(
        "id" => $login['id'],
        "email" => $email,
        "displayName" => $person->display_name,
        "imageURL" => $imageURL,
        "description" => $person->position . "\n" . $person->company->name
    );

    $json = array("message" => "Login Success",
        "status" => "SUCCESS",
        "user" => $user);
} else {

    //  (ervinne) check if the user is existing
    $result = $db->query("SELECT 1 FROM tbuser WHERE email = {$db->escape($email)};");
    $message = "Unregistered email address";

    //  if the user exists but is not able to login, he probably entered an incorrect password
    if (count($result) > 0) {
        $message = "Incorrect password";
    }

    $json = array("message" => $message,
        "status" => "ERROR");
}
echo json_encode($json);
