<?php
$db = new Database();
$auth = Auth::getAuth('current_user');
if(Auth::hasAuth('current_user')){
	$text = $_POST['text'];
	$request_id = $_POST['request_id'];
	$form_id = $_POST['form_id'];
	if(isset($text) && isset($request_id) && isset($form_id)){
		post::createComment($text,1,$request_id,$auth,$form_id);
		$json = array("status"=>"SUCCESS");
		echo json_encode($json);
	}else{
		$json = array("status"=>"ERROR");
		echo json_encode($json);
	}
}
?>