<?php

$current_user = (new Auth())->getAuth("current_user");

$search_string           = filter_input(INPUT_GET, "search_string") . "%";
$data_type               = filter_input(INPUT_GET, "data_type");
$filter_search_type      = filter_input(INPUT_GET, "filter_search_type", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$exclude_users           = filter_input(INPUT_GET, "exclude_users", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$exclude_groups          = filter_input(INPUT_GET, "exclude_groups", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$exclude_departments     = filter_input(INPUT_GET, "exclude_departments", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$exclude_positions       = filter_input(INPUT_GET, "exclude_positions", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$exclude_personal_groups = filter_input(INPUT_GET, "exclude_personal_groups", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$current_user_company_id = $current_user["company_id"];
$current_user_id = $current_user['id'];

$user_exclusion_clause       = "";
$group_exclusion_clause      = "";
$department_exclusion_clause = "";
$position_exclusion_clause   = "";
$personal_groups_exclusion_clause = "";

if (count($exclude_users) > 0) {
    $user_exclusion_clause = "AND id NOT IN (" . join(",", $exclude_users) . ")";
}

if (count($exclude_groups) > 0) {
    $group_exclusion_clause = "AND g.id NOT IN (" . join(",", $exclude_groups) . ")";
}

if (count($exclude_departments) > 0) {
    $department_exclusion_clause = "AND id NOT IN ('" . join("','", $exclude_departments) . "')";
}

if (count($exclude_positions) > 0) {
    $position_exclusion_clause = "AND id NOT IN (" . join(",", $exclude_positions) . ")";
}

if (count($exclude_personal_groups) > 0) {
    $personal_groups_exclusion_clause = "AND id NOT IN (" . join(",", $exclude_personal_groups) . ")";
}


$select_users = "SELECT 
                display_name AS result, 'USER' AS type, id, extension AS image_extension
            FROM
                tbuser user
            WHERE
                display_name LIKE '%{$search_string}' 
                AND company_id = {$current_user_company_id}
                AND is_active = 1
                AND user_level_id <> 4
                AND id != $current_user_id
                {$user_exclusion_clause}";
// var_dump($select_users);

$select_global_groups = "SELECT 
                g.group_name AS result, 'GROUP' AS type, g.id, null
            FROM
                tbform_groups_users gu
                    LEFT JOIN
                tbform_groups g ON gu.group_id = g.id
            WHERE                
                g.group_name LIKE '{$search_string}'
                AND g.company_id = {$current_user_company_id}
                AND gu.is_active = 1
                AND g.is_active = 1                
                {$group_exclusion_clause}";

$select_positions = "SELECT 
                position AS result, 'POSITION' AS type, id, null
            FROM
                tbpositions
            WHERE
                position LIKE '{$search_string}' 
                AND company_id = {$current_user_company_id}
                AND is_active = 1
                {$position_exclusion_clause}";

$select_departments = "SELECT DISTINCT
                department AS result, 'DEPARTMENT' AS type, department_code, null
            FROM
                tborgchartobjects
            WHERE
                department LIKE '{$search_string}'
                {$department_exclusion_clause}
                    AND orgChart_id = (SELECT 
                        id
                    FROM
                        tborgchart
                    WHERE
                        company_id = {$current_user_company_id} AND status = 1)";

$select_personal_groups = "SELECT DISTINCT
                name AS result, 'PERSONAL_GROUP' AS type, id, NULL
            FROM
                tbgroups
            WHERE
                name LIKE '{$search_string}' AND user_id = {$current_user["id"]}
                    AND is_active = 1";

if ($filter_search_type && count($filter_search_type) > 0) {

    foreach ($filter_search_type AS $search_type) {
        switch ($search_type) {
            case 1:
                $query = $query . $select_users;
                break;
            case 2:
                $query = $query . $select_global_groups;
                break;
            case 3:
                $query = $query . $select_positions;
                break;
            case 4:
                $query = $query . $select_departments;
                break;
            case 5:
                $query = $query . $select_personal_groups;
                break;
        }

        $query = $query . " UNION ";
    }

    // remove trailing " UNION "
    $query = substr($query, 0, strlen($query) - 6);
} else {
    $query = $select_users . " UNION " .
            $select_global_groups . " UNION " .
            $select_positions . " UNION " .
            $select_departments . " UNION " .
            $select_personal_groups;
}

//echo $query;

$database    = new Database();
$raw_results = $database->query($query);
$results     = array();

foreach ($raw_results AS $result) {

    if ($result["type"] == 'USER') {
        //  get the image url of the user
        if ($result["image_extension"] == null || $result["image_extension"] == "") {
            $imageURL = "/images/avatar/small.png";
        } else {
            $encryptID = md5(md5($result['id']));
            $path      = $encryptID . "." . "/small_" . $encryptID . "." . $result["image_extension"];
            $imageURL  = "/images/formalistics/tbuser/" . $path;
        }

        $result["image"] = $imageURL;
        unset($result["image_extension"]);
    }

    array_push($results, $result);
}

$this->results = $results;
$this->query   = $query;

if ($data_type == "JSON") {
    // echo json_encode($results, JSON_PRETTY_PRINT);
    include(dirname(__FILE__) . "/../" . "/view/api_response.phtml");
} else {
    // var_dump($results);
    include(dirname(__FILE__) . "/../" . "/view/search.phtml");
}
