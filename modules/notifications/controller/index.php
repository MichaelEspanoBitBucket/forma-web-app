<?php

error_reporting(E_ALL);

$current_user = (new Auth())->getAuth("current_user");

$database          = new Database();
//$notifications_DAO = new notifications();
$notifications_DAO = new NotificationsDAO($database);

$start_index = filter_input(INPUT_POST, "start_index");
$fetch_count = filter_input(INPUT_POST, "fetch_count");

if (!$start_index) {
    $start_index = 0;
}

if (!$fetch_count) {
    $fetch_count = 20;
}

try {
    $this->results = $notifications_DAO->getMobilitySupportedNotifications(
            $current_user["id"], $start_index, $fetch_count
    );
} catch (Exception $e) {
    $this->error = $e;
}

include(APPLICATION_PATH . "\\modules\\API\\view\\api_response.phtml");

error_reporting(0);
