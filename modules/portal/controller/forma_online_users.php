<?php

error_reporting(E_ERROR);

$returnType = 'HTML';
if (array_key_exists('return_type', $_GET)) {
    $returnType = filter_input(INPUT_GET, 'return_type');
}

include_once API_LIBRARY_PATH . 'API.php';

$onlineUsers = [];
$currentUser = (new Auth())->getAuth("current_user");

if ($currentUser) {
    $database = new APIDatabase();
    $database->connect();

    $query = "SELECT 
                u.id,
                display_name,
                extension,
                u.id AS recipients,
                (SELECT 
                                thread_id
                        FROM
                                tb_thread_subscribers
                        GROUP BY thread_id                
                        HAVING SUM(user_id IN (u.id , {$currentUser["id"]})) = COUNT(*)
                                AND COUNT(user_id) = 2 LIMIT 1) AS thread_id,
                (SELECT 
                                COUNT(*)
                        FROM
                                tb_thread_subscriber_messages sub_msg
                                        LEFT JOIN
                                tb_thread_messages msg ON sub_msg.thread_message_id = msg.id
                        WHERE
                                sub_msg.message_subscriber_user_id = {$currentUser["id"]}
                                        AND sub_msg.read = 0
                                        AND msg.thread_id = (SELECT 
                                                thread_id
                                        FROM
                                                tb_thread_subscribers
                                        GROUP BY thread_id
                                        HAVING SUM(user_id IN (u.id , {$currentUser["id"]})) = COUNT(*)
                                                AND COUNT(user_id) = 2 LIMIT 1)) AS unread_messages
        FROM
                tbuser u
        WHERE
                u.is_active = 1 
                AND u.id != {$currentUser["id"]}
                AND u.is_available = 1 
        ORDER BY display_name";

        /*    echo $query;*/

    $onlineUsers = $database->query($query);
    $database->disconnect();
}

if ($returnType == 'HTML') {
    include(dirname(__FILE__) . "/../" . "/view/forma_online_users.phtml");
} else {

    $utilities = new APIUtilities();
    $utilities->addAPIHeaders();

    if (count($onlineUsers) <= 0 && !$currentUser) {
        header('Content-Type: text/plain');
        header('HTTP/1.0 403 Forbidden');
        echo "You must be logged in to access this service";
    } else {
        $parsedUsers = array();

        foreach ($onlineUsers AS $user) {
            $table      = "tbuser";
            $id_encrypt = md5(md5($user["id"]));
            $size       = "small";

            $userImage     = '/images/formalistics/' . $table . '/' . $id_encrypt . '/' . $size . '_' . $id_encrypt . '.' . $user["extension"];
            $newMessageURL = "/user_view/message-app?recp=" . $user["id"] . "&recpname=" . $user["display_name"];
            $viewThreadURL = "/user_view/message-app?threadId=" . $user["thread_id"];

            $user['image_url'] = $userImage;
            $user              = array_map('utf8_encode', $user);
            array_push($parsedUsers, $user);
        }

        echo json_encode($parsedUsers, JSON_PRETTY_PRINT);
    }
}

error_reporting(0);
