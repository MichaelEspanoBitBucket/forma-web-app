<?php

$view_type = "formalistics";

if (filter_has_var(INPUT_GET, "view_type")) {
    $view_type = filter_input(INPUT_GET, "view_type");
}

/* Redirect user if they already logged */
if (!Auth::hasAuth('current_user')) {
//    header('location: /portal/login');
    header('location: /login');
}

if ($view_type === "formalistics") {
    /* ========== Get Page Visit ========== */
    $this->get_page_visit(new Layout());

    $this->setLayout();
} else {

//    error_reporting(E_ALL);    
//    include(dirname(__FILE__) . "/../" . "/view/stand_alone_dependencies.phtml");
    include(dirname(__FILE__) . "/../" . "/view/stand_alone_index.phtml");

//    error_reporting(0);
}