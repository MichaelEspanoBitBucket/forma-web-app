<?php
include_once APPLICATION_PATH . '/library/portal/PortalDataReaderFacade.php';

// $from_index = filter_input(INPUT_POST, "from_index");
// $fetch_count = filter_input(INPUT_POST, "fetch_count");

$from_index = 0;
$fetch_count = 20;

$current_user = (new Auth())->getAuth("current_user");
$data_reader_facade = new PortalDataReaderFacade();

$posts = $data_reader_facade->getSurveys($current_user);
$this->posts = $posts;

include(dirname(__FILE__) . "/../" . "/view/message_board_survey_graph.phtml");
