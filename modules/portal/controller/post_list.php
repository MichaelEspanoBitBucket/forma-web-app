<?php

error_reporting(E_ERROR);

include_once APPLICATION_PATH . '/library/portal/PortalDataReaderFacade.php';
include_once APPLICATION_PATH . '/library/APILibraries/APIUtilities.php';

if (property_exists($this, "partial_view_data") && $this->partial_view_data != null) {
    $filter_post_type = $this->partial_view_data["filter_post_type"];
    $from_index = $this->partial_view_data["from_index"];
    $fetch_count = $this->partial_view_data["fetch_count"];
} else {
    $filter_post_type = filter_input(INPUT_POST, "filter_post_type");
    $from_index = filter_input(INPUT_POST, "from_index");
    $fetch_count = filter_input(INPUT_POST, "fetch_count");
}

$current_user = (new Auth())->getAuth("current_user");
$data_reader_facade = new PortalDataReaderFacade();
$response_type = filter_input(INPUT_POST, "response_type");
$post_id = filter_input(INPUT_POST, "post_id");

if(trim($post_id) != ""){
    $other_cond = array(array("field"=>"post.id","operator"=>"=","value"=>$post_id));
}

if (!$current_user) {
    echo "Session lost, please login again";
    return;
}

//back up
// $posts_with_total_count = $data_reader_facade->getPostsWithTotalCount(
//         $current_user, $filter_post_type, $from_index, $fetch_count
// );
// $total_post_count       = $posts_with_total_count["total_post_count"];
// $posts                  = $posts_with_total_count["posts"];
//with memcache
$redis_cache = getRedisConnection();
if ($redis_cache) {

    $auth = Auth::getAuth('current_user');
    $user_id = $auth['id'];
    $cachedFormat = "$user_id::$from_index::$filter_post_type::$post_id";

    // $memcache->delete("post_list");
    $cache_post_list = json_decode($redis_cache->get("post_list"), true);
    $cache_post_list_count = json_decode($redis_cache->get("post_list_count"), true);

    $cached_query_result = $cache_post_list ['' . $cachedFormat . ''];
    $cached_query_result_count = $cache_post_list_count ['' . $cachedFormat . ''];

    if ($cached_query_result) {
        // echo "from cache";

        $total_post_count = $cached_query_result_count;

        $posts = $cached_query_result;
    } else {
        // echo "from db";

        $posts_with_total_count = $data_reader_facade->getPostsWithTotalCount($current_user, $filter_post_type, $from_index, $fetch_count, $other_cond
        );

        $posts = $posts_with_total_count["posts"];
        $total_post_count = $posts_with_total_count["total_post_count"];


        $cache_post_list ['' . $cachedFormat . ''] = $posts;
        $cache_post_list_count ['' . $cachedFormat . ''] = $total_post_count;

        $redis_cache->set("post_list", json_encode($cache_post_list));
        $redis_cache->set("post_list_count", json_encode($cache_post_list_count));
    }
} else {
    $posts_with_total_count = $data_reader_facade->getPostsWithTotalCount($current_user, $filter_post_type, $from_index, $fetch_count, $other_cond
    );
    $total_post_count = $posts_with_total_count["total_post_count"];
    $posts = $posts_with_total_count["posts"];
}

$posts_with_author_images = array();


$replace_src = '<img[^\>]*>'; //regex for image lazy load
foreach ($posts AS $post) {
    if ($post["author_image_extension"] == null || $post["author_image_extension"] == "") {
        $image_url = BROKEN_IMAGE;
    } else {
        $encrypted_id = md5(md5($post['author_id']));
        $path = $encrypted_id . "." . "/small_" . $encrypted_id . "." . $post["author_image_extension"];
        $image_url = "/images/formalistics/tbuser/" . $path;
    }

    $post["author_image_url"] = $image_url;
    unset($post["author_image_extension"]);



    if($response_type=="JSON"){
        // no lazy load
        
        // $post_str = $post['announcement'];

        $post_str = preg_replace_callback($replace_src, function($a) {
            if(strpos($a[0], "://")){
                $temp = str_replace("src=\"", "src=\"" , $a[0]);
            }else{
                $temp = str_replace("src=\"", "src=\"". substr(MAIN_PAGE, 0,strlen(MAIN_PAGE)-1) , $a[0]);
            }
            return $temp;
        }, $post['announcement']);


        $survey_question_str = preg_replace_callback($replace_src, function($a) {
            if(strpos($a[0], "://")){
                $temp = str_replace("src=\"", "src=\"" , $a[0]);
            }else{
                $temp = str_replace("src=\"", "src=\"". substr(MAIN_PAGE, 0,strlen(MAIN_PAGE)-1) , $a[0]);
            }
            return $temp;
        }, $post['survey_question']);

    }else{
        // for image lazy load
        $post_str = preg_replace_callback($replace_src, function($a) {
            $temp = str_replace("src=\"", "data-original=\"", $a[0]);
            return $temp;
        }, $post['announcement']);

        $survey_question_str = preg_replace_callback($replace_src, function($a) {
            $temp = str_replace("src=\"", "data-original=\"", $a[0]);
            return $temp;
        }, $post['survey_question']);
    }

    //replace values
    $post['announcement'] = $post_str; //$post['announcement'];
    $post['survey_question'] = $survey_question_str; //$post['survey_question'];

    
    //delete privilage
    $delete_privilage = false;

    if($current_user["id"] == $post["author_id"] ||
        $current_user["user_level_id"] == 1 ||
        $current_user["user_level_id"] == 2){

        $delete_privilage = true;
    }
    $post['delete_privilage'] = $delete_privilage;


    //push to array all the changes
    array_push($posts_with_author_images, $post);
}

$this->current_user = $current_user;
$this->current_fetch_index = $from_index;
$this->fetch_count = $fetch_count;
$this->total_post_count = $total_post_count;
$this->time_elapsed_secs = $posts_with_total_count["time_elapsed_secs"];
$this->posts = $posts_with_author_images;

// echo json_encode($this->posts);

if($response_type=="JSON"){
    $APIUtil = new APIUtilities();
    $APIUtil->addAPIHeaders();
    echo json_encode($this->posts);
}else{
    //default html based
    include ( dirname(__FILE__) . "/../" . "/view/post_list.phtml");
}

error_reporting(0);
