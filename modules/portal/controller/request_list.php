<?php

error_reporting(E_ERROR);
//error_reporting(E_ALL);

include_once API_LIBRARY_PATH . 'API.php';
include_once APPLICATION_PATH . '/library/request/RequestsDataReaderFacade.php';

$this->current_user = (new Auth())->getAuth("current_user");
$form_id            = filter_input(INPUT_POST, "form_id");
$start_index        = filter_input(INPUT_POST, "start_index");
$fetch_count        = filter_input(INPUT_POST, "fetch_count");


$start_time = microtime(true);

$database = new APIDatabase();

$data_reader         = new RequestsDataReaderFacade();
$database->connect();
//$database->query("SET SESSION query_cache_type=0;");    //  for test only, caching is disabled
$this->requests      = $data_reader->searchRequestsAsPosts($database, $this->current_user, $form_id, $start_index, $fetch_count);
$this->request_count = $data_reader->getRequestCount($database, $this->current_user, $form_id);

$this->requests_left       = $this->request_count - ($start_index + $fetch_count);
$this->current_fetch_index = $from_index;
$this->fetch_count         = $fetch_count;

$database->disconnect();
$this->time_elapsed_secs = microtime(true) - $start_time;
$this->total_post_count  = $start_index + $fetch_count;

include(dirname(__FILE__) . "/../" . "/view/request_list.phtml");

error_reporting(0);
