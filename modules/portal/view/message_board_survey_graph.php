<div id="fl-ui-announcement-graph-wrapper">
    <div class="fl-widget-head fl-small-widgetv2 fl-small-widgetv3 isDisplayNone">
      
        <svg class="icon-svg icon-svg-announcement svg-icon-survey-graph-portal" viewBox="0 0 100.264 100.597">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-survey-graph-portal"></use>
        </svg>
        <span class="widgetv3-widget-title droidsansbold">Survey Graph</span>
     
    </div>
    <div class=" fl-ui-announcement-graph-content-wrapper fl-scroll-overflow"> <!-- fl-widget-wrapper-scroll fl-scroll-overflow -->
         <?php

            $this->renderAsPartialView("/modules/portal/controller/survey_list.php", null);
            
        ?>
    </div>
</div>
<Script type="text/javascript">

    

</Script>
