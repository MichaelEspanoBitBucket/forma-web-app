<?php

// error_reporting(E_ALL);

include_once API_LIBRARY_PATH . 'API.php';
include APPLICATION_PATH . '/library/portal/PortalPostDAO.php';
include_once APPLICATION_PATH . '/library/APILibraries/APIUtilities.php';

// $current_user = (new Auth())->getAuth("current_user");
$post_id = filter_input(INPUT_GET, "post_id");
$response_type        = filter_input(INPUT_GET, "response_type");

if($response_type=="JSON"){
    $current_user           = filter_input(INPUT_GET, "author_id");
}else{
    $current_user           = (new Auth())->getAuth("current_user");
}


try {
    $database = new APIDatabase();
    $database->connect();
    $post_dao = new PortalPostDAO($database);

    $this->result = $post_dao->deletePost($post_id, $current_user);
    $this->status = APIStatus::STATUS_SUCCESS;


    //refresh memcache
    $fs = new functions();
    $announcement_memcached = array("post_list","post_list_count");

    $deleteMemecachedKeys = array_merge($announcement_memcached);

    $fs->deleteMemcacheKeys($deleteMemecachedKeys);
    
    $database->disconnect();
} catch (Exception $e) {
    $this->error = $e->getCode();
    $this->error_message = $e->getMessage();
    $this->status = APIStatus::ERROR_API_ERROR;
}
if($response_type=="JSON"){
    $utilities = new APIUtilities();
    $utilities->addAPIHeaders();
    echo json_encode(array("status"=>$this->status));
}else{
    include dirname(__FILE__) . "/../view/api_response.phtml";
}
//error_reporting(0);
