<?php

include_once API_LIBRARY_PATH . 'API.php';
include APPLICATION_PATH . '/library/portal/PortalPostCommentsDAO.php';
include_once APPLICATION_PATH . '/library/APILibraries/APIUtilities.php';

// $current_user = (new Auth())->getAuth("current_user");
$comment_id = filter_input(INPUT_POST, "comment_id");
$response_type        = filter_input(INPUT_POST, "response_type");

// if($response_type=="JSON"){
//     $current_user           = filter_input(INPUT_POST, "author_id");;
// }else{
    $current_user           = (new Auth())->getAuth("current_user");
// }

try {
    $database = new APIDatabase();
    $database->connect();
    $post_comments_dao = new PortalPostCommentsDAO($database);

    $this->result = $post_comments_dao->deleteComment($comment_id, $current_user);
    $this->status = APIStatus::STATUS_SUCCESS;

    //refresh memcache
    $fs = new functions();
    $announcement_memcached = array("post_list","post_list_count");

    $deleteMemecachedKeys = array_merge($announcement_memcached);

    $fs->deleteMemcacheKeys($deleteMemecachedKeys);

    $database->disconnect();
} catch (Exception $e) {
    $this->error = $e->getCode();
    $this->error_message = $e->getMessage();
    $this->status = APIStatus::ERROR_API_ERROR;
}

if($response_type=="JSON"){
    $utilities = new APIUtilities();
    $utilities->addAPIHeaders();
    echo json_encode(array("status"=>$this->status));
}else{
    include dirname(__FILE__) . "/../view/api_response.phtml";
}

