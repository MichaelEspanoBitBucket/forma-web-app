<?php

include_once API_LIBRARY_PATH . 'API.php';
include APPLICATION_PATH . '/library/portal/PortalLikesDAO.php';

$current_user = (new Auth())->getAuth("current_user");
$post_id = filter_input(INPUT_GET, "post_id");
$comment_id = filter_input(INPUT_GET, "comment_id");

try {
    $this->results = array(
        "total_likes" => 500
    );
    $this->status = APIStatus::STATUS_SUCCESS;

    //refresh memcache
    $fs = new functions();
    $announcement_memcached = array("post_list","post_list_count");

    $deleteMemecachedKeys = array_merge($announcement_memcached);

    $fs->deleteMemcacheKeys($deleteMemecachedKeys);

    
} catch (Exception $e) {
    $this->error = $e->getCode();
    $this->error_message = $e->getMessage();
    $this->status = APIStatus::ERROR_API_ERROR;
}

include dirname(__FILE__) . "/../view/api_response.phtml";
