<?php
//error_reporting(E_ALL);
include_once API_LIBRARY_PATH . 'API.php';
include APPLICATION_PATH . '/library/portal/PortalLikesDAO.php';

$currentUser = (new Auth())->getAuth("current_user");
$postId      = filter_input(INPUT_POST, "post_id");

try {
    $database = new APIDatabase();
    $database->connect();
    $likesDAO = new PortalLikesDAO($database);

    $likesDAO->likePost($currentUser["id"], $postId);

    $this->result = array();
    $this->status = APIStatus::STATUS_SUCCESS;

    $database->disconnect();

    //refresh memcache
    $fs = new functions();
    $announcement_memcached = array("post_list","post_list_count");

    $deleteMemecachedKeys = array_merge($announcement_memcached);

    $fs->deleteMemcacheKeys($deleteMemecachedKeys);

    
} catch (Exception $e) {
    $this->error         = $e->getCode();
    $this->error_message = $e->getMessage();
    $this->status        = APIStatus::ERROR_API_ERROR;
}

include dirname(__FILE__) . "/../view/api_response.phtml";

//error_reporting(0);