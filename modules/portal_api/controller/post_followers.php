<?php

//error_reporting(E_ERROR);

include_once API_LIBRARY_PATH . 'API.php';
include APPLICATION_PATH . '/library/portal/PortalPostDAO.php';
include_once APPLICATION_PATH . '/library/APILibraries/APIUtilities.php';

$post_id = filter_input(INPUT_POST, "post_id");
$response_type = filter_input(INPUT_POST, "response_type");

if($response_type=="JSON"){
    
    $utilities = new APIUtilities();
    $utilities->addAPIHeaders();

    $main_page = substr(MAIN_PAGE, 0,strlen(MAIN_PAGE)-1);

    try {
        $database = new APIDatabase();
        $portal_post_dao = new PortalPostDAO($database);

        $database->connect();
        $followers = $portal_post_dao->getPostFollowers($post_id);
        

        foreach ($followers AS $follower) {
            $encrypted_id = md5(md5($follower['follower']));
            $path         = $encrypted_id . "." . "/small_" . $encrypted_id . "." . $follower["image_extension"];
            $image_url    = "/images/formalistics/tbuser/" . $path;
            $follower["image_url"] = $image_url;
            $followerResults[] = array_map('utf8_encode', $follower);

        }

        $this->results = $followerResults;
        $this->status  = APIStatus::STATUS_SUCCESS;
        $database->disconnect();


    }catch(Exception $e) {
        $this->error         = $e->getCode();
        $this->error_message = $e->getMessage();
        $this->status        = APIStatus::ERROR_API_ERROR;
    }
    
    include dirname(__FILE__) . "/../view/api_response.phtml";

   
}else {

    $database        = new APIDatabase();
    $portal_post_dao = new PortalPostDAO($database);

    $database->connect();
    $followers = $portal_post_dao->getPostFollowers($post_id);
    $database->disconnect();

    $this->followers = array();
    foreach ($followers AS $follower) {
        if ($follower["image_extension"] == null || $follower["image_extension"] == "") {
            $image_url = "";
        } else {
            $encrypted_id = md5(md5($follower['follower']));
            $path         = $encrypted_id . "." . "/small_" . $encrypted_id . "." . $follower["image_extension"];
            $image_url    = "/images/formalistics/tbuser/" . $path;
        }

        $follower["image_url"] = $image_url;
        unset($follower["image_extension"]);

        array_push($this->followers, $follower);
    }
        include dirname(__FILE__) . "/../view/post_followers.phtml";
}


