<?php

// error_reporting(E_ALL);
// <editor-fold defaultstate="collapsed" desc="Dependencies">
include APPLICATION_PATH . '/library/portal/PortalAnnouncement.php';
include APPLICATION_PATH . '/library/portal/PortalDataWriterFacade.php';
include_once APPLICATION_PATH . '/library/APILibraries/APIUtilities.php';


/** @var PortalPost */
$announcement       = new PortalAnnouncement();
$data_writer_facade = new PortalDataWriterFacade();

// </editor-fold>

$author = (new Auth())->getAuth("current_user");

//  requires authentication
if (!$author) {
    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = APIStatus::ERROR_UNAUTHORIZED;
    $response['error_message'] = 'You must be logged in to access this service';


    echo json_encode($response);
    return;
}

$post_id              = filter_input(INPUT_POST, "post_id");
$post_visibility_type = filter_input(INPUT_POST, "post_visibility_type");
$post_type            = filter_input(INPUT_POST, "post_type");
$post_text_contents   = filter_input(INPUT_POST, "post_text_contents");
$post_followers       = filter_input(INPUT_POST, "post_followers", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$response_type        = filter_input(INPUT_POST, "response_type");

// only add a date updated if this is a new post
if (!$post_id) {
    $announcement->date_posted = "NOW()";
} else {
    $announcement->id = $post_id;
}

$announcement->author_id           = $author["id"];
$announcement->date_updated        = "NOW()";
$announcement->date_last_commented = "NOW()";
$announcement->visibility_type     = $post_visibility_type;
$announcement->type                = $post_type;
$announcement->followers           = $post_followers;
$announcement->text_content        = $post_text_contents;

//var_dump($announcement);
//exit();

try {
    $this->results = $data_writer_facade->saveAnnouncement($announcement);    
    $this->status = APIStatus::STATUS_SUCCESS;
    
    //refresh memcache
    $fs = new functions();
    $announcement_memcached = array("post_list","post_list_count");

    $deleteMemecachedKeys = array_merge($announcement_memcached);

    $fs->deleteMemcacheKeys($deleteMemecachedKeys);

} catch (Exception $e) {
    $this->error         = $e->getCode();
    $this->error_message = $e->getMessage();
    $this->status        = APIStatus::ERROR_API_ERROR;
}

if($response_type=="JSON"){
    $utilities = new APIUtilities();
    $utilities->addAPIHeaders();
    echo json_encode(array("status"=>$this->status,"post-details"=>$this->results));
}else {
    include dirname(__FILE__) . "/../view/api_response.phtml";
}




//error_reporting(0);
