<?php

// error_reporting(E_ALL);

include APPLICATION_PATH . '/library/portal/PortalPostComment.php';
include APPLICATION_PATH . '/library/portal/PortalDataWriterFacade.php';
include_once APPLICATION_PATH . '/library/APILibraries/APIUtilities.php';

$current_user = (new Auth())->getAuth("current_user");

$comment_id          = filter_input(INPUT_POST, "comment_id");
$post_id             = filter_input(INPUT_POST, "post_id");
$reply_to_comment_id = filter_input(INPUT_POST, "reply_to_comment_id");
$text_content        = filter_input(INPUT_POST, "text_content");
$response_type        = filter_input(INPUT_POST, "response_type");

// if($response_type=="JSON"){
//     $author_id           = filter_input(INPUT_POST, "author_id");;
// }else{
    $author_id           = $current_user["id"];
// }

$post_comment = new PortalPostComment();

if ($reply_to_comment_id != null) {
    $post_comment->reply_to_comment_id = $reply_to_comment_id;
}

$post_comment->author_id    = $author_id;
$post_comment->post_id      = $post_id;
$post_comment->text_content = $text_content;
$post_comment->date_posted  = "NOW()";

try {
    $data_writer_facade = new PortalDataWriterFacade();

    $this->result = $data_writer_facade->savePostComment($post_comment);
    $this->status = APIStatus::STATUS_SUCCESS;

    //refresh memcache
    $fs = new functions();
    $announcement_memcached = array("post_list","post_list_count");

    $deleteMemecachedKeys = array_merge($announcement_memcached);

    $fs->deleteMemcacheKeys($deleteMemecachedKeys);
    
    // $database->disconnect();
} catch (Exception $e) {
    $this->error         = $e->getCode();
    $this->error_message = $e->getMessage();
    $this->status        = APIStatus::ERROR_API_ERROR;
}

if($response_type=="JSON"){
    $utilities = new APIUtilities();
    $utilities->addAPIHeaders();
    echo json_encode(array("status"=>$this->status,"comment-id"=>$this->result));
}else{
    include dirname(__FILE__) . "/../view/api_response.phtml";
}
//error_reporting(0);
