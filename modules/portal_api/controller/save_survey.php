<?php

//error_reporting(E_ALL);
// <editor-fold defaultstate="collapsed" desc="Dependencies">
include APPLICATION_PATH . '/library/portal/PortalSurveyResponse.php';
include APPLICATION_PATH . '/library/portal/PortalDataWriterFacade.php';


/** @var PortalPost */
$survey = new PortalSurvey();
$data_writer_facade = new PortalDataWriterFacade();

// </editor-fold>

$author = (new Auth())->getAuth("current_user");

$post_id = filter_input(INPUT_POST, "post_id");
$post_visibility_type = filter_input(INPUT_POST, "post_visibility_type");
$post_type = filter_input(INPUT_POST, "post_type");
$post_text_contents = filter_input(INPUT_POST, "post_text_contents");
$post_survey_responses = filter_input(INPUT_POST, 'survey_responses', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$post_followers = filter_input(INPUT_POST, "post_followers", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$enable_multiple_responses = filter_input(INPUT_POST, "enable_multiple_responses");
$publish_results = filter_input(INPUT_POST, "publish_results");

// only add a date updated if this is a new post
if (!$post_id || $post_id == 0) {
    $survey->date_posted = "NOW()";
} else {
    $survey->id = $post_id;
}
$survey->author_id = $author["id"];
$survey->date_updated = "NOW()";
$survey->date_last_commented = "NOW()";
$survey->visibility_type = $post_visibility_type;
$survey->type = $post_type;
$survey->text_content = $post_text_contents;
$survey->survey_responses = $post_survey_responses;
$survey->followers = $post_followers;
$survey->enable_multiple_responses = $enable_multiple_responses;
$survey->publish_results = $publish_results;

// exit();
try {
    $this->result = $data_writer_facade->saveSurvey($survey);
    $this->status = APIStatus::STATUS_SUCCESS;

    //refresh memcache
    $fs = new functions();
    $announcement_memcached = array("post_list","post_list_count");

    $deleteMemecachedKeys = array_merge($announcement_memcached);

    $fs->deleteMemcacheKeys($deleteMemecachedKeys);
} catch (Exception $e) {
    $this->error = $e->getCode();
    $this->error_message = $e->getMessage();
    $this->status = APIStatus::ERROR_API_ERROR;
}

include dirname(__FILE__) . "/../view/api_response.phtml";
// error_reporting(0);
