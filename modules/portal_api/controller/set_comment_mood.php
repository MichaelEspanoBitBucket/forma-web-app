<?php

//error_reporting(E_ALL);

include APPLICATION_PATH . '/library/portal/PortalPostMoodsFacade.php';

$currentUser = (new Auth())->getAuth("current_user");
$mood        = filter_input(INPUT_POST, "mood");
$postId      = filter_input(INPUT_POST, "post_id");
$commentId   = filter_input(INPUT_POST, "comment_id");
$response_type        = filter_input(INPUT_POST, "response_type");

try {

    $moodsFacade = new PortalPostMoodsFacade();
    $moodId      = $moodsFacade->setCommentMood($currentUser['id'], $mood, $postId, $commentId);

    $this->result = array(
        'mood_id' => $moodId
    );
    $this->status = APIStatus::STATUS_SUCCESS;

    //refresh memcache
    $fs = new functions();
    $announcement_memcached = array("post_list","post_list_count");

    $deleteMemecachedKeys = array_merge($announcement_memcached);

    $fs->deleteMemcacheKeys($deleteMemecachedKeys);
} catch (Exception $e) {
    $this->error         = $e->getCode();
    $this->error_message = $e->getMessage();
    $this->status        = APIStatus::ERROR_API_ERROR;
}
if($response_type=="JSON"){
    $utilities = new APIUtilities();
    $utilities->addAPIHeaders();
    echo json_encode(array("status"=>$this->status,"mood"=>$mood,"postId"=>$postId));
}else{
    include dirname(__FILE__) . "/../view/api_response.phtml";
}


//error_reporting(0);
