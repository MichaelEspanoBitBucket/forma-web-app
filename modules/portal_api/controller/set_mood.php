<?php

//error_reporting(E_ALL);

include APPLICATION_PATH . '/library/portal/PortalPostMoodsFacade.php';

$current_user = (new Auth())->getAuth("current_user");
$mood         = filter_input(INPUT_POST, "mood");
$postId       = filter_input(INPUT_POST, "post_id");
$commentId    = filter_input(INPUT_POST, "comment_id");

try {

    $moodsFacade = new PortalPostMoodsFacade();
    $moodId      = 0;

    if ($commentId <= 0) {
        $moodId = $moodsFacade->setPostMood($current_user['id'], $mood, $postId);
    } else {
        $moodId = $moodsFacade->setCommentMood($current_user['id'], $mood, $postId, $commentId);
    }

    $this->results = array(
        'mood_id' => $moodId,
        'post_id' => $postId,
        'comment_id' => $commentId,
    );
    $this->status  = APIStatus::STATUS_SUCCESS;

    //refresh memcache
    $fs = new functions();
    $announcement_memcached = array("post_list","post_list_count");

    $deleteMemecachedKeys = array_merge($announcement_memcached);

    $fs->deleteMemcacheKeys($deleteMemecachedKeys);
} catch (Exception $e) {
    $this->error         = $e->getCode();
    $this->error_message = $e->getMessage();
    $this->status        = APIStatus::ERROR_API_ERROR;
}

include dirname(__FILE__) . "/../view/api_response.phtml";
//error_reporting(0);
