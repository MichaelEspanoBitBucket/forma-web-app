<?php

//error_reporting();
// <editor-fold defaultstate="collapsed" desc="Dependencies">
include APPLICATION_PATH . '/library/portal/PortalSurveyResponse.php';
include APPLICATION_PATH . '/library/portal/PortalDataWriterFacade.php';

/** @var PortalPost */
$surveyRes       = new PortalSurveyResponse();
$data_writer_facade = new PortalDataWriterFacade();

// </editor-fold>

$author = (new Auth())->getAuth("current_user");
$survey_id              = filter_input(INPUT_POST, "survey_id");
$publish_results = filter_input(INPUT_POST, 'publish_results');
$chart_type = filter_input(INPUT_POST, 'chart_type');


// only add a date updated if this is a new post
// if (!$post_id) {
//     $survey->date_posted = "NOW()";
// }

$surveyRes->survey_id       = $survey_id;
$surveyRes->responder_id       = $author["id"];
$surveyRes->publish_results    = $publish_results;
$surveyRes->chart_type    = $chart_type;

// var_dump($surveyRes);
try {
    $this->result = $data_writer_facade->updateSurveyPublish($surveyRes);
    $this->status = APIStatus::STATUS_SUCCESS;

    //refresh memcache
    $fs = new functions();
    $announcement_memcached = array("post_list","post_list_count");

    $deleteMemecachedKeys = array_merge($announcement_memcached);

    $fs->deleteMemcacheKeys($deleteMemecachedKeys);
} catch (Exception $e) {
    $this->error         = $e->getCode();
    $this->error_message = $e->getMessage();
    $this->status        = APIStatus::ERROR_API_ERROR;
}

include dirname(__FILE__) . "/../view/api_response.phtml";
//error_reporting(0);
