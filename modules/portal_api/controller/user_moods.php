<?php

//error_reporting(E_ERROR);

include APPLICATION_PATH . '/library/portal/PortalPostMoodsDAO.php';
include APPLICATION_PATH . '/library/user/ImageUtilities.php';
include_once APPLICATION_PATH . '/library/APILibraries/APIUtilities.php';

$ImageUtilities = new ImageUtilities();

$APIUtil = new APIUtilities();
$APIUtil->addAPIHeaders();


$current_user = (new Auth())->getAuth("current_user");
$postId       = filter_input(INPUT_POST, "post_id");
$commentId    = filter_input(INPUT_POST, "comment_id");

$from_index = filter_input(INPUT_POST, "from_index");
$fetch_count = filter_input(INPUT_POST, "fetch_count");

$main_page = substr(MAIN_PAGE, 0,strlen(MAIN_PAGE)-1);

try {
    $database = new APIDatabase();
    $moodsDAO = new PortalPostMoodsDAO($database);

    $database->connect();
    $results = $moodsDAO->getUserMoods($postId, $commentId, $current_user["id"],$from_index,$fetch_count);
    
    foreach ($results AS $result) {
        $user_image = $main_page.$ImageUtilities->getAvatarImageURL($result['user_id'],$result['user_image_extension']);
        $result['user_image'] = $user_image;
        $moods[] = array_map('utf8_encode', $result);
    }
    $this->results = $moods;
    $this->status  = APIStatus::STATUS_SUCCESS;

    $database->disconnect();
} catch (Exception $e) {
    $this->error         = $e->getCode();
    $this->error_message = $e->getMessage();
    $this->status        = APIStatus::ERROR_API_ERROR;
}

include dirname(__FILE__) . "/../view/api_response.phtml";
//error_reporting(0);
