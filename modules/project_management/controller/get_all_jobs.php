<?php

// var params = {
//                 key: value,
//                 key: value,
//                 key: value
//             };




// error_reporting(E_ALL);
include_once('dashbaord_dependencies.php');
include_once('../../library/JobTaskDAO.php');
$current_user = Auth::getAuth("current_user");
// var_dump($current_user);
// return;
$JobTaskDAO = new JobTaskDAO();

$parameters_valid = true;

$this->results = null;

// <editor-fold desc="Validation" defaultstate="collapsed">
if (!$current_user) {
    $this->error         = "NOT_LOGGED_IN";
    $this->error_message = "The user must be logged in to use this service";

    $parameters_valid = false;
}


// </editor-fold>

if ($parameters_valid) {
    try {    
        $result = $JobTaskDAO->getalljobs();
        $this->results       = $result;
    } catch (Exception $e) {
        $this->error         = "CONNECT_JOB_TASK_ERROR";
        $this->error_message = $e->getMessage();
    }
}
// var_dump($this->results);
echo json_encode($this->results,JSON_PRETTY_PRINT);
// include_once(dirname(__FILE__) . "/../" . "/view/api_response.phtml");

// error_reporting(0);
