<?php
include_once('dashbaord_dependencies.php');
include_once('../../library/JobTaskDAO.php');
// error_reporting(E_ALL);
$params       = $_GET;

$current_user = Auth::getAuth("current_user");
// var_dump($current_user);
// return;
$user_id = $current_user['id'];
$user_name = $current_user['display_name'];
$field = $params['field'];
$field_value = $params['value'];


$JobTaskDAO = new JobTaskDAO();

$parameters_valid = true;



// <editor-fold desc="Validation" defaultstate="collapsed">
if (!$current_user) {
    $this->error         = "NOT_LOGGED_IN";
    $this->error_message = "The user must be logged in to use this service";

    $parameters_valid = false;
}


// </editor-fold>
if ($parameters_valid) {
    if($current_user['user_level_id'] == '2' || $current_user['user_level_id'] == '1'){
        $result = $JobTaskDAO->getJobTaskByAdminBy($field,$field_value);
        $this->results       = $result;
    }else{
        $result = $JobTaskDAO->getJobTaskBy ($user_id,$user_name,$field,$field_value);
        $this->results       = $result;
    }
}
// var_dump($this->results);
echo json_encode($this->results,JSON_PRETTY_PRINT);
//echo json_encode($user_name,JSON_PRETTY_PRINT);
// include_once(dirname(__FILE__) . "/../" . "/view/api_response.phtml");

// error_reporting(0);
