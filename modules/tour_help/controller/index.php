<?php
    header('Content-type: application/json');
    //$tour_json = array(
    //      "name" 		=> "tour_application",
    //      "bgcolor"		=> "black",
    //      "color"		=> "white",
    //      "position"	        => "T",
    //      "title"		=> "Choosing your application.",
    //      "text"		=> "This is the application portal section here you can choose your desired application. <ul><li><p class='tooltip_content'><font color='red'>*</font> Note you need to select an app to proceed. <br><br> Please select one.<p></li></ul>",
    //      "time" 		=> 5000
    //);
    //$tour_json = array(
    //      "name" 		=> "tour_modules",
    //      "bgcolor"		=> "black",
    //      "color"		=> "white",
    //      "position"	        => "L",
    //      "title"		=> "Using a Forms.",
    //      "text"		=> "After choosing an application you can now use the forms that has been set by the admin. <br><br> Click the module button to see a list of forms.",
    //      "time" 		=> 5000
    //);
    //$tour_json = array(
    //      "name" 		=> "fl-module-icon",
    //      "bgcolor"		=> "black",
    //      "color"		=> "white",
    //      "position"	        => "B",
    //      "title"		=> "Choose a Forms.",
    //      "text"		=> "Click the icon thumbnail button to see the list of submitted request.",
    //      "time" 		=> 5000
    //);
    //$tour_json = array(
    //      "name" 		=> "tour_button_create_request",
    //      "bgcolor"		=> "black",
    //      "color"		=> "white",
    //      "position"	        => "TL",
    //      "title"		=> "Create a Request",
    //      "text"		=> "Right now you don't have a requested form click create request button to make one.",
    //      "time" 		=> 5000
    //);
    $tour_json = array(
          "name" 		=> "fl-form-wrapper",
          "bgcolor"		=> "black",
          "color"		=> "white",
          "position"	        => "B",
          "title"		=> "Create Request",
          "text"		=> "Fill out all fields to complete the request then click the button <font color='red'>action</font> upper right corner to submit.",
          "time" 		=> 5000
    );
    
    $functions = new functions();
    echo $functions->view_json_formatter(json_encode($tour_json));
    
?>