<?php

/* code here */
$db = new Database();
$fs = new functions();

$date = $fs->currentDateTime();
$auth = Auth::getAuth('current_user');

setcookie('USERAUTH','',0,'/', COOKIE_URL);
setcookie('application', null, -1, substr(USER_VIEW,0,strlen(USER_VIEW)-1));
setcookie('application','',0,'/',COOKIE_URL);
setcookie('application','',0,'/',COOKIE_URL);
setcookie('application','');
/* If user click the button/link to logged out. */
$destroy = Auth::destroyAuth('current_user');
Auth::destroyAuth('Guest_AUTO_LOGGED');
Auth::destroyAuth('Guest_URL');
Auth::destroyAuth('captcha_code_proceed');
$action_description = Logs::createMessage("3","");
// Auth::destroyAuth('getLeftBar'); // destroy leftbar session

// Audit Logs
$insert_audit_rec = array("user_id" => $auth['id'], "audit_action" => "3", "table_name" => "tbuser",
    "record_id" => $auth['id'], "date" => $date, "ip" => $_SERVER["REMOTE_ADDR"], "is_active" => 1,"action_description" => $action_description);
$audit_log = $db->insert("tbaudit_logs", $insert_audit_rec);

$personDoc = new Person($db, $auth['id']);
$personDoc->is_available = '0';
$personDoc->update();

// Reset Timeout idle
        $personDoc = new Person($db,$auth['id']);
        $personDoc->timeout = '0';
        $personDoc->update();

if ($destroy || !$destroy) {

    setcookie('USERAUTH','',0,'/', COOKIE_URL);
    header('location: /');
}
/* code here(end) */
exit;
$this->setLayout();
