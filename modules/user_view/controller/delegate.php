<?php
/*code here*/
/* Redirect user if they already logged */
if(Auth::hasAuth('current_user')){
    //$this->view->current_user = Auth::getAuth('current_user');
	functions::guest_redirection_auto_logged();
}
else{
    functions::url_redirect();
}

/*code here(end)*/
//if not admin
functions::setPageForAdmin();

$this->setLayout();