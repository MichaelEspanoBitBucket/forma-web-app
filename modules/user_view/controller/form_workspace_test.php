<?php

/* code here */

/* ========== Get Page Visit ========== */
    $this->get_page_visit(new Layout());
    
/* Redirect user if they already logged */


/* code here(end) */
$auth = Auth::getAuth('current_user');
if ($auth['user_level_id'] != "4") {
    if (isset($_GET['requestID'])) {
        $search = new Search();
        $requestID = $_GET['requestID'];
        $form_id = $_GET['formID'];
        // $forms = $db->query("SELECT * FROM tb_workspace WHERE id = {$db->escape($form_id)} AND is_delete = 0", "row");

        $getModules = $search->getModules(" AND tbw.id = $form_id","0");
        if (count($getModules) == 0) {
            header("location:/user_view/home");
        }

        if ($requestID == 0) {
            // $formPrivacy_authors = $search->getFormPrivacyUsers($forms['form_authors']);
            $formPrivacy_authors = $search->getModulesByAuthor(" AND tbw.id = $form_id");
            if (count($formPrivacy_authors) == 0) {
                header("location: /");
            }
        } else {
            $obj = array("condition" => " AND request.ID = " . $requestID . "");
            $obj = json_decode(json_encode($obj), true);
            $result = $search->getManyRequestV2("", 0, $form_id, 0, 1, $obj);
            if (count($result) == 0) {
                header("location: /");
            }
        }
    } else {
        // header("location:/user_view/error");
    }
}
if (Auth::hasAuth('current_user')) {
    //$this->view->current_user = Auth::getAuth('current_user');
    functions::guest_redirection_auto_logged();
} else {
    functions::url_redirect();
}
$this->setLayout();

function createRequestView($GET_URL_VAR) {
    $db = new Database();
    $auth = Auth::getAuth('current_user');
    $userCompany = new userQueries();
    $company = $userCompany->getCompany($auth['company_id']);
    $upload = new upload();

    $personDoc = new Person($db, $auth['id']);
    $userDepartmentName = $personDoc->department->name;

    // $YEAH_FORMULA = new Formula("@Department[Requestor] + @TrackNo + @DEPARTMENTFIELD");
    // print_r($YEAH_FORMULA->setSourceForm("DYNAMIC_TABLE_TESTING","*",array("TESTING3DIGIT"=>"JC000") ) );
    // print_r($YEAH_FORMULA->getProccessedFormula());
    // $YEAH_FORMULA = new Formula("@Lookup('DYNAMIC_TABLE_TESTING' , 'TrackNo')");
    // print_r($YEAH_FORMULA->getProccessedFormula());
    // $YEAH_FORMULA = new Formula("@Concat('DYNAMIC_TABLE_TESTING' , 'TrackNo','biscuites')");
    // print_r($YEAH_FORMULA->getProccessedFormula());
    // print_r(eval($YEAH_FORMULA->getProccessedFormula()));
    if ($GET_URL_VAR['view_type'] == "preview") {
        $formWidth = $GET_URL_VAR['formWidth'];
        $formHeight = $GET_URL_VAR['formHeight'];
        $form_container = "style='margin: 0px auto;width:" . $formWidth . "px;'";
        $form_size = "style=' background-color: #fff;height:" . $formHeight . "px;'";

        $form_w = $formWidth;
        $form_h = $formHeight;
    } elseif ($GET_URL_VAR['view_type'] == "request" || $GET_URL_VAR['view_type'] == "report") {
        // Get Workspace Information
        $getID = $GET_URL_VAR['formID'];
        $requestID = $GET_URL_VAR['requestID'];

        $str = "SELECT WS.*, WFO.workflow_id, WFO.buttonStatus,WFO.fieldEnabled, WFO.fieldRequired, WFO.fieldHiddenValue, WFO.json as wfo_json FROM tb_workspace WS 
	                           LEFT JOIN tbworkflow WF
	                            ON WS.Id = WF.form_id 
	                           LEFT JOIN tbworkflow_objects WFO
	                            ON WFO.workflow_id = WF.id 
	                            WHERE WS.id={$db->escape($getID)} AND WFO.type_rel = '1'  AND WF.Is_Active = '1' AND WS.is_delete = 0 AND WF.is_delete = 0";
        $form = $db->query($str, "row");

        $form_json = json_decode($form['form_json'], true);
        $wfo_json = json_decode($form["wfo_json"], true);

        $form_container = "style='margin: 0px auto;width:" . $form_json['WorkspaceWidth'] . "px;'";
        $form_size = "style=' background-color: #fff;height:" . $form_json['WorkspaceHeight'] . "px;'";

        $form_w = $form_json['WorkspaceWidth'];
        $form_h = $form_json['WorkspaceHeight'];

        $fieldEnabled = $form['fieldEnabled'];
        $fieldRequired = $form['fieldRequired'];
        $fieldHiddenValues = $form['fieldHiddenValue'];
        $draftButton = $wfo_json["workflow_draft_button"];
    } elseif ($GET_URL_VAR['view_type'] == "update") {
        // Get Workspace Information
        $getID = $GET_URL_VAR['ID'];
        $requestID = $GET_URL_VAR['requestID'];

        $str = "SELECT WS.*, WFO.workflow_id, WFO.buttonStatus,WFO.fieldEnabled, WFO.fieldRequired, WFO.fieldHiddenValue FROM tb_generate WS 
	                           LEFT JOIN tbworkflow WF
	                            ON WS.form_id = WF.form_id 
	                           LEFT JOIN tbworkflow_objects WFO
	                            ON WFO.workflow_id = WF.id 
	                            WHERE WS.id={$db->escape($getID)} AND WFO.type_rel = '1'  AND WF.Is_Active = '1' AND WF.is_delete = 0";
        $form = $db->query($str, "row");


        $form_json = json_decode($form['form_json'], true);

        $form_container = "style='margin: 0px auto;width:" . $form_json['WorkspaceWidth'] . "px;'";
        $form_size = "style=' background-color: #fff;height:" . $form_json['WorkspaceHeight'] . "px;'";

        $form_w = $form_json['WorkspaceWidth'];
        $form_h = $form_json['WorkspaceHeight'];

        $fieldEnabled = $form['fieldEnabled'];
        $fieldRequired = $form['fieldRequired'];
        $fieldHiddenValues = $form['fieldHiddenValue'];
    }
    // GETTING BUTTON ACTIONS


    $actionRequestButton = '';
    $buttonJSON = "";
    if (isset($form['buttonStatus'])) {
        $buttonJSON = json_decode($form['buttonStatus'], true);
        foreach ($buttonJSON as $key => $value) {
            $actionRequestButton .= '<li class="fl-action-submit" value="' . $buttonJSON[$key]["button_name"] . '" data-workflow-id="' . $form['workflow_id'] . '" action="' . $buttonJSON[$key]["child_id"] . '" message="' . $buttonJSON[$key]["message"] . ' " message_success="' . $buttonJSON[$key]["message_success"] . '" require_comment="' . $buttonJSON[$key]["require_comment"] . '"><a>' . $buttonJSON[$key]["button_name"] . '</a></li>';
        }


        if ($draftButton == 1) {
            $actionRequestButton .= '<li class="fl-action-submit" isDraft="true" value="draft" data-workflow-id="' . $form['workflow_id'] . '" action="Draft" message="Are you sure you want to save this request as DRAFT?" message_success="Request has been saved as DRAFT"><a>Save & Review Later</a></li>';
        }
    }

    //GETTING FORM SAVED HTML
    if ($GET_URL_VAR['view_type'] == "request" || $GET_URL_VAR['view_type'] == "update") {

        if ($form['company_id'] == "0" || $form['company_id'] == $auth['company_id']) {
            $workspace_content = str_replace('setObject cursor_move', 'setOBJ form_upload_photos', $form['form_content']);
            $remove_disabled = str_replace('disabled="disabled"', '', $workspace_content);
            $remove_disabled1 = str_replace('form_upload_builder_photos"', 'form_upload_builder_photos1', $remove_disabled);
            $removed_focus1 = str_replace('component-primary-focus', '', $remove_disabled1);
            $removed_focus2 = str_replace('component-ancillary-focus', '', $removed_focus1);
        }
    }


    $return_datas = array(
        "action_buttons" => $actionRequestButton,
        "form_content" => $removed_focus2,
        "form_size" => array(
            "width" => $form_w,
            "height" => $form_h
        ),
        "form_name" => $form['form_name'],
        "workflow_id" => $form['workflow_id'],
        "auth_id" => $auth['id'],
        "auth_display_name" => $auth['display_name'],
	"auth_first_name" => $auth['first_name'],
	"auth_last_name" => $auth['last_name'],
        "buttonStatus" => $form['buttonStatus'],
        "userDepartmentName" => $userDepartmentName,
        "fieldEnabled" => $fieldEnabled,
        "fieldRequired" => $fieldRequired,
        "fieldHiddenValues" => $fieldHiddenValues,
        "draftButton" => $wfo_json["workflow_draft_button"]
    );

    return $return_datas;
}

function echobig($string, $bufferSize = 1200) {
    $splitString = str_split($string, $bufferSize);

    foreach ($splitString as $chunk) {
        ob_start();
        echo $chunk;
        ob_flush();
    }
    ob_end_flush();
}
