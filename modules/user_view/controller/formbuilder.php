<?php

/* code here */

/* ========== Get Page Visit ========== */
    $this->get_page_visit(new Layout());
    
/* Redirect user if they already logged */
if(Auth::hasAuth('current_user')){
    //$this->view->current_user = Auth::getAuth('current_user');
	functions::guest_redirection_auto_logged();
}
else{
    functions::url_redirect();
}
/*code here(end)*/


$search = new Search();

//if not admin
functions::setPageForAdmin();

if(!isset($_GET['formID'])){
	header("location:/user_view/youre-not-allowed-to-visit-this-page");
}

if(isset($_GET['formID']) && $_GET['formID']!=0){
	$form_id = $_GET['formID'];
	// $obj = array("condition"=>" AND w.id = ". $id ." ");
	// $obj = json_decode(json_encode($obj),true);
	// $result = $search->getForms("",0,$obj);
	// $countForm = count(json_decode(setAdminViewer($result),true));
	// if($countForm==0){
	// 	header("location:/user_view/home");
	// }
	$getFormsByAdmin = $search->getFormsByAdmin(" AND tbw.id = $form_id");
	if(count($getFormsByAdmin)==0){
		header("location:/user_view/youre-not-allowed-to-visit-this-page");
	}
}

$this->setLayout();