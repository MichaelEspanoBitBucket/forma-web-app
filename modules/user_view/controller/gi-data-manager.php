<?php
/* Redirect if not authenticated */
if(!Auth::hasAuth('current_user'))
    header('location: /');

	
$auth = Auth::getAuth('current_user');
$user_level_id = $auth['user_level_id'];

// Allow User View
functions::redirection_user($user_level_id);

/*code here(end)*/
$this->setLayout();