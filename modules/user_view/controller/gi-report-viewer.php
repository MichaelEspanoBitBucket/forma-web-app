<?php
/*code here*/
/* Redirect user if they already logged */

$view_type = "formalistics";

if (filter_has_var(INPUT_GET, "view_type")) {
    $view_type = filter_input(INPUT_GET, "view_type");
}

if(Auth::hasAuth('current_user')){
    //$this->view->current_user = Auth::getAuth('current_user');
    functions::guest_redirection_auto_logged();
}
else{
    header('location: /');
}
functions::setPageForAdmin();

    $auth = Auth::getAuth('current_user');
    $user_level_id = $auth['user_level_id'];
        // Allow User View
        functions::redirection_user($user_level_id);
/*code here(end)*/
// $this->setLayout();




if ($view_type === "formalistics") {
    /* ========== Get Page Visit ========== */
    //$this->get_page_visit(new Layout());

    $this->setLayout();
} else {

//    error_reporting(E_ALL);    
//    include(dirname(__FILE__) . "/../" . "/view/stand_alone_dependencies.phtml");
    include(dirname(__FILE__) . "/../" . "/view/gi-report-viewer-sa.phtml");

//    error_reporting(0);
}