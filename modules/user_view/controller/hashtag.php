<?php

/* code here */

/* ========== Get Page Visit ========== */
    $this->get_page_visit(new Layout());
    
/* Redirect user if they already logged */
if(Auth::hasAuth('current_user')){
	functions::guest_redirection_auto_logged();
	
    //$this->view->current_user = Auth::getAuth('current_user');

}
else{
    functions::url_redirect();
}

/*code here(end)*/
$this->setLayout();