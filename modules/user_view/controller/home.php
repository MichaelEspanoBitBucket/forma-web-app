<?php

/* code here */

/* ========== Get Page Visit ========== */
    $this->get_page_visit(new Layout());
    
/* Redirect user if they already logged */
if(Auth::hasAuth('current_user')){
	functions::guest_redirection_auto_logged();
	
    //$this->view->current_user = Auth::getAuth('current_user');
    // Validate if page is not accessible
    $mod = unserialize(ENABLE_COMPANY_MOD);
    if($mod['gs3_insights']['gi-report-designer'] != "0" || $mod['gs3_insights']['gi-report-designer-list'] != "0"){
        header('Location: /');
    }
}
else{
    functions::url_redirect();
}
        
/*code here(end)*/
$this->setLayout();