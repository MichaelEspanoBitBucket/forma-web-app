<?php

/* code here */
    
/* Redirect user if they already logged */
$db = new Database();
$fs = new functions();
$mod = unserialize(ENABLE_COMPANY_MOD);
if(Auth::hasAuth('current_user'))
{
    functions::guest_redirection_auto_logged();
    
    $auth = Auth::getAuth('current_user');
    //$this->view->current_user = Auth::getAuth('current_user');
    if(isset($_GET['redirect_url'])){
        $decode_url = functions::encode_decode_url($_GET['redirect_url'],"decode");
        header('Location: /' . $decode_url); 
    }else if($_GET['type']=="go"){
        $location = $_GET['redirect'];
        $redirect = str_replace("|=%=|", "&", $location);
        header('Location: ' . USER_VIEW .$redirect);
    }else{
        if(ALLOW_REQUEST_FORM_GUEST != "1" || $auth['user_level_id'] != "4"){
            if($mod['gs3_insights']['gi-report-designer'] != "0" || $mod['gs3_insights']['gi-report-designer-list'] != "0"){
                header('Location: ' . USER_VIEW . 'gi-dashboard-home'); 
             }else{ 
                header('Location: ' . USER_VIEW . 'home'); 
             } 
        }else{
        $query_guest = $db->query("SELECT * FROM tb_guest WHERE guest_type={$db->escape('guest')} AND guest_user_id={$db->escape($auth['id'])} ORDER BY guest_id DESC","row");
                
        header("location: " . GUEST_MAIN_PAGE_1 . "user_view/workspace?view_type=request&formID=" . $query_guest['guest_formID'] . "&requestID=0");
        }
    }
}
//remove application cache
//setcookie('application','');
/* For user login if correct redirect to home */
if($_SERVER['REQUEST_METHOD'] == "POST")
{
    $username = $_POST['username'];
    $password = $_POST['password'];
    $coords = $_POST['coords'];
    $session = new Auth();
    $login = $session->login($username,$password,'email','password','tbuser');
   
    if($login){
        if(isset($_GET['redirect_url'])){
            $decode_url = functions::encode_decode_url($_GET['redirect_url'],"decode");
            header('Location: /' . $decode_url); 
        }else if($_GET['type']=="go"){
            $location = $_GET['redirect'];
            $redirect = str_replace("|=%=|", "&", $location);
            header('Location: ' . USER_VIEW .$redirect);
        }else{
            header('Location: /'); 
        }
        
        $fs->save_audit_logs($db,$login['id'],"2","tbuser",$date,$login['id']);
        
        // Save location of user logged in
            
            $insert = array("location"	=>	$coords,
                            "date"		=>	$date,
                            "userID"	=>	$login['id'],
                            "is_active"	=>	1);
            $db->insert("tblocation",$insert);
            
            // Set Available
            $personDoc = new Person($db,$login['id']);
            $personDoc->is_available = '1';
            $personDoc->update();
            
            // Reset Timeout idle
            $personDoc = new Person($db,$login['id']);
            $personDoc->timeout = '0';
            $personDoc->update();
    }else{
        if($_GET['type']=="go"){
            $path = functions::curPageURL("");
            $get_module_param = explode("?",$path);
            header('Location: /login?' . $get_module_param[1] . '?' . $get_module_param[2] . '&login_attempt=1');
        }else if(isset($_GET['redirect_url'])){
            $decode_url = functions::encode_decode_url($_GET['redirect_url'],"decode");
            header('Location: /login?' . $decode_url . '&login_attempt=1');
        }else{
            header('Location: /login?login_attempt=1'); 
        }
    }
}   
/*code here(end)*/
$this->setLayout();