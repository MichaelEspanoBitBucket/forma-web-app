<?php

/* code here */

/* ========== Get Page Visit ========== */
    $this->get_page_visit(new Layout());
    
/* Redirect user if they already logged */
if(Auth::hasAuth('current_user')){
    //$this->view->current_user = Auth::getAuth('current_user');
	functions::guest_redirection_auto_logged();
}
else{
    functions::url_redirect();
}

/*code here(end)*/

//if not admin
functions::setPageForAdmin();

$search = new Search();

if(isset($_GET['id'])){
	$id = $_GET['id'];
	$obj = array("condition"=>" AND id = ". $id ." ");
	$obj = json_decode(json_encode($obj),true);
	$result = $search->getOrgchart("",0,$obj);
	if(count($result)==0){
		header("location:/user_view/");
	}	
}

$this->setLayout();