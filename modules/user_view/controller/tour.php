<?php

/* code here */

/* ========== Get Page Visit ========== */
    $this->get_page_visit(new Layout());
    
/* Redirect user if they already logged */
if(Auth::hasAuth('current_user')){
    //$this->view->current_user = Auth::getAuth('current_user');
	functions::guest_redirection_auto_logged();
}
else{
    functions::url_redirect();
}

        
/*code here(end)*/
$this->setLayout();