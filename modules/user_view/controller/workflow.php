<?php

/* code here */

/* ========== Get Page Visit ========== */
    $this->get_page_visit(new Layout());
    
/* Redirect user if they already logged */
if(Auth::hasAuth('current_user')){
    //$this->view->current_user = Auth::getAuth('current_user');
	functions::guest_redirection_auto_logged();
}
else{
    functions::url_redirect();
}

/*code here(end)*/

//if not admin
functions::setPageForAdmin();

$search = new Search();

if(isset($_GET['form_id'])){
	// $id = $_GET['id'];
	// $obj = array("condition"=>" AND wf.id = ". $id ." ");
	// $obj = json_decode(json_encode($obj),true);
	// $result = $search->getWorkflows("",0,$obj);
	// $countWorkflow = count(json_decode(setAdminViewer($result),true));
	// if($countWorkflow==0){
	// 	header("location:/user_view/");
	// }
	// $id = $_GET['form_id'];
	// $obj = array("condition"=>" AND w.id = ". $id ." ");
	// $obj = json_decode(json_encode($obj),true);
	// $result = $search->getForms("",0,$obj);
	// $countForm = count(json_decode(setAdminViewer($result),true));
	// if($countForm==0){
	// 	header("location:/user_view/");
	// }
	$form_id = $_GET['form_id'];
	$id = $_GET['id'];
	$getFormsByAdmin = $search->getFormsByAdmin(" AND tbw.id = $form_id");
	$getWorkflow = $db->query("SELECT * FROM tbworkflow WHERE form_id = {$db->escape($_GET['form_id'])} AND id=$id ","numrows");
	if($getWorkflow==0 && $_GET['view_type']=="update"){
		header("location:/user_view/youre-not-allowed-to-visit-this-page");
	}
	if(count($getFormsByAdmin)==0){
		header("location:/user_view/youre-not-allowed-to-visit-this-page");
	}
}
if(isset($_GET['action'])){
	if($_GET['action']=="getActiveWorkflow"){
		$getWorkflow = $db->query("SELECT * FROM tbworkflow WHERE form_id = {$db->escape($_GET['form_id'])} AND is_active=1 AND is_delete = 0 ","row");
		header("location:". USER_VIEW ."workflow?view_type=edit&form_id=". $_GET['form_id'] ."&id=".$getWorkflow['id']);
	}
}
$this->setLayout();