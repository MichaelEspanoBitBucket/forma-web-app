<?php

/* code here */

/* ========== Get Page Visit ========== */
$this->get_page_visit(new Layout());

/* Redirect user if they already logged */
/* code here(end) */
$auth = Auth::getAuth('current_user');
$redis_cache = getRedisConnection();

$conn = getCurrentDatabaseConnection();

if ($auth['user_level_id'] != "4") {
    if (isset($_GET['requestID'])) {
        $search = new Search();
        $requestID = $_GET['requestID'];
        $form_id = $_GET['formID'];
        // $forms = $db->query("SELECT * FROM tb_workspace WHERE id = {$db->escape($form_id)} AND is_delete = 0", "row");
        $form_data = getFormWorkspaceData($conn, $form_id);
        $tblname = $form_data["form_table_name"];

        if ($requestID == 0) {
            // $formPrivacy_authors = $search->getFormPrivacyUsers($forms['form_authors']);
            if ($redis_cache) {
                $cache_is_author = json_decode($redis_cache->get("request_author_access_" . $form_id), true);
            }
            if ($cache_is_author[$auth["id"]]) {
                $formPrivacy_authors = $cache_is_author[$auth["id"]];
            } else {
                $formPrivacy_authors = $search->getModulesByAuthor(" AND tbw.id = $form_id");

                if ($redis_cache) {
                    if (!$cache_is_author) {
                        $cache_is_author = array();
                    }
                    $cache_is_author[$auth["id"]] = $formPrivacy_authors;
                    $redis_cache->set("request_author_access_" . $form_id, json_encode($cache_is_author));
                }
            }

            if (count($formPrivacy_authors) == 0) {
                // header("location: /");
                header("location:/user_view/youre-not-allowed-to-visit-this-page");
            }
        } else {
            $obj = array("condition" => " AND request.ID = " . $requestID . "");
            $obj = json_decode(json_encode($obj), true);

            if ($redis_cache) {
                $cache_controller_request_access = json_decode($redis_cache->get("request_access_" . $form_id), true);
            }

            if ($cache_controller_request_access[$requestID][$auth["id"]]) {
                $result = $cache_controller_request_access[$requestID][$auth["id"]];
            } else {
                $result = $search->getManyRequestV2("", 0, $form_id, 0, 1, $obj);

                if ($redis_cache) {
                    if (!$cache_controller_request_access) {
                        $cache_controller_request_access = array();
                    }
                    $cache_controller_request_access[$requestID][$auth["id"]] = $result;
                    $redis_cache->set("request_access_" . $form_id, json_encode($cache_controller_request_access));
                }
            }

            if (count($result) == 0) {
                //get if trash record
                $getTrash = TrashBin::getSpecificTrashRecord($form_id, $requestID);
                if (count($getTrash) > 0) {
                    header("location:/user_view/deleted-request-page");
                } else {
                    // header("location: /");
                    header("location:/user_view/youre-not-allowed-to-visit-this-page");
                }
            }
        }


        if ($redis_cache) {
            $cache_module_access = json_decode($redis_cache->get("form_access_" . $form_id), true);
        }

        if ($cache_module_access[$auth["id"]]) {
            $getModules = $cache_module_access[$auth["id"]];
        } else {
            $getModules = $search->getModules(" AND tbw.id = $form_id", "0");

            if ($redis_cache) {

                if (!$cache_module_access) {
                    $cache_module_access = array();
                }

                $cache_module_access[$auth["id"]] = $getModules;
                $redis_cache->set("form_access_" . $form_id, json_encode($cache_module_access));
            }
        }


        if (count($getModules) == 0) {
            // header("location:/user_view/home");
            header("location:/user_view/youre-not-allowed-to-visit-this-page");
        }
        $db = new Database();
        //dito ung update ng notification
//        $formData = new Form($db, $form_id);
        $setNoti = array("is_read" => 1,
            "user_read" => 1);
        $whereRequestNoti = array("userID" => $auth['id'],
            "type" => "5",
            "noti_id" => $requestID,
            "table_name" => $tblname);

        //update
        $db->update("tbnotification", $setNoti, $whereRequestNoti);
    } else {
        // header("location:/user_view/error");
    }
}
if (Auth::hasAuth('current_user')) {
    //$this->view->current_user = Auth::getAuth('current_user');
} else {
    functions::url_redirect();
}
$this->setLayout();

function createRequestView($GET_URL_VAR) {
    $db = new Database();
    $conn = getCurrentDatabaseConnection();
    $auth = Auth::getAuth('current_user');
    $userCompany = new userQueries();
    $company = $userCompany->getCompany($auth['company_id']);
    $upload = new upload();

    $personDoc = new Person($db, $auth['id']);
    $userDepartmentName = $personDoc->department->name;

    // $YEAH_FORMULA = new Formula("@Department[Requestor] + @TrackNo + @DEPARTMENTFIELD");
    // print_r($YEAH_FORMULA->setSourceForm("DYNAMIC_TABLE_TESTING","*",array("TESTING3DIGIT"=>"JC000") ) );
    // print_r($YEAH_FORMULA->getProccessedFormula());
    // $YEAH_FORMULA = new Formula("@Lookup('DYNAMIC_TABLE_TESTING' , 'TrackNo')");
    // print_r($YEAH_FORMULA->getProccessedFormula());
    // $YEAH_FORMULA = new Formula("@Concat('DYNAMIC_TABLE_TESTING' , 'TrackNo','biscuites')");
    // print_r($YEAH_FORMULA->getProccessedFormula());
    // print_r(eval($YEAH_FORMULA->getProccessedFormula()));
    if ($GET_URL_VAR['view_type'] == "preview") {
        $formWidth = $GET_URL_VAR['formWidth'];
        $formHeight = $GET_URL_VAR['formHeight'];
        $form_container = "style='margin: 0px auto;width:" . $formWidth . "px;'";
        $form_size = "style=' background-color: #fff;height:" . $formHeight . "px;'";

        $form_w = $formWidth;
        $form_h = $formHeight;
    } elseif ($GET_URL_VAR['view_type'] == "request" || $GET_URL_VAR['view_type'] == "report") {
        // Get Workspace Information
        $form_id = $GET_URL_VAR['formID'];
        $requestID = $GET_URL_VAR['requestID'];

        $form = getFormWorkspaceData($conn, $form_id);
        $form_json = json_decode($form['form_json'], true);
        $wfo_json = json_decode($form["wfo_json"], true);

        $form_container = "style='margin: 0px auto;width:" . $form_json['WorkspaceWidth'] . "px;'";
        $form_size = "style=' background-color: #fff;height:" . $form_json['WorkspaceHeight'] . "px;'";

        $form_w = $form_json['WorkspaceWidth'];
        $form_h = $form_json['WorkspaceHeight'];

        $fieldEnabled = $form['fieldEnabled'];
        $fieldRequired = $form['fieldRequired'];
        $fieldHiddenValues = $form['fieldHiddenValue'];
        $draftButton = $wfo_json["workflow_draft_button"];
    } elseif ($GET_URL_VAR['view_type'] == "update") {
        // Get Workspace Information
        $getID = $GET_URL_VAR['ID'];
        $requestID = $GET_URL_VAR['requestID'];

        $str = "SELECT WS.*, WFO.workflow_id, WFO.buttonStatus,WFO.fieldEnabled, WFO.fieldRequired, WFO.fieldHiddenValue FROM tb_generate WS 
	                           LEFT JOIN tbworkflow WF
	                            ON WS.form_id = WF.form_id 
	                           LEFT JOIN tbworkflow_objects WFO
	                            ON WFO.workflow_id = WF.id 
	                            WHERE WS.id={$db->escape($getID)} AND WFO.type_rel = '1'  AND WF.Is_Active = '1' AND WF.is_delete = 0";
        $form = $db->query($str, "row");


        $form_json = json_decode($form['form_json'], true);

        $form_container = "style='margin: 0px auto;width:" . $form_json['WorkspaceWidth'] . "px;'";
        $form_size = "style=' background-color: #fff;height:" . $form_json['WorkspaceHeight'] . "px;'";

        $form_w = $form_json['WorkspaceWidth'];
        $form_h = $form_json['WorkspaceHeight'];

        $fieldEnabled = $form['fieldEnabled'];
        $fieldRequired = $form['fieldRequired'];
        $fieldHiddenValues = $form['fieldHiddenValue'];
    }
    // GETTING BUTTON ACTIONS


    $actionRequestButton = '';
    $buttonJSON = "";
    if (isset($form['buttonStatus'])) {
        $buttonJSON = json_decode($form['buttonStatus'], true);

        foreach ($buttonJSON as $key => $value) {
            $field_required = join(",",$buttonJSON[$key]["fieldRequired"]);
            $actionRequestButton .= '<li class="fl-action-submit" value="' . $buttonJSON[$key]["button_name"] . '" data-workflow-id="' . $form['workflow_id'] . '" action="' . $buttonJSON[$key]["child_id"] . '" message="' . $buttonJSON[$key]["message"] . ' " message_success="' . $buttonJSON[$key]["message_success"] . '" field_required="' . $field_required .'" require_comment="' . $buttonJSON[$key]["require_comment"] . '" landing_page="' . $buttonJSON[$key]["wf_landing_page"] . '"><a>' . $buttonJSON[$key]["button_name"] . '</a></li>';
        }


        if ($draftButton == 1) {
            $actionRequestButton .= '<li class="fl-action-submit" isDraft="true" value="draft" data-workflow-id="' . $form['workflow_id'] . '" action="Draft" message="Are you sure you want to save this request as DRAFT?" message_success="Request has been saved as DRAFT"><a>Save & Review Later</a></li>';
        }
        $actionRequestButton .= '<li class="fl-action-close" value="Close" data-workflow-id="0" action="0" message="" message_success = ""><a href="#" data-original-title="" title="">Close</a></li>';
    }

    //GETTING FORM SAVED HTML
    if ($GET_URL_VAR['view_type'] == "request" || $GET_URL_VAR['view_type'] == "update") {

        if ($form['company_id'] == "0" || $form['company_id'] == $auth['company_id']) {
            $search = array(
                'setObject cursor_move',
                'setObject container-parent-accordion',
                'setObject accordion-position'
            );
            $replace = array(
                'setOBJ form_upload_photos',
                'setOBJ container-parent-accordion',
                'setOBJ accordion-position'
            );
            $workspace_content = str_replace($search, $replace, $form['form_content']);

            $remove_disabled = str_replace('disabled="disabled"', '', $workspace_content);
            $remove_disabled1 = str_replace('form_upload_builder_photos"', 'form_upload_builder_photos1', $remove_disabled);
            $removed_focus1 = str_replace('component-primary-focus', '', $remove_disabled1);
            $removed_focus2 = str_replace('component-ancillary-focus', '', $removed_focus1);
        }
    }


    $return_datas = array(
        "action_buttons" => $actionRequestButton,
        "form_content" => $removed_focus2,
        "form_size" => array(
            "width" => $form_w,
            "height" => $form_h
        ),
        "form_name" => $form['form_name'],
        "workflow_id" => $form['workflow_id'],
        "auth_id" => $auth['id'],
        "auth_display_name" => $auth['display_name'],
        "auth_first_name" => $auth['first_name'],
        "auth_last_name" => $auth['last_name'],
        "buttonStatus" => $form['buttonStatus'],
        "userDepartmentName" => $userDepartmentName,
        "fieldEnabled" => $fieldEnabled,
        "fieldRequired" => $fieldRequired,
        "fieldHiddenValues" => $fieldHiddenValues,
        "draftButton" => $wfo_json["workflow_draft_button"],
        "form_alias" => $form['form_alias']
    );

    return $return_datas;
}

function echobig($string, $bufferSize = 1200) {
    $splitString = str_split($string, $bufferSize);

    foreach ($splitString as $chunk) {
        ob_start();
        echo $chunk;
        ob_flush();
    }
    ob_end_flush();
}
