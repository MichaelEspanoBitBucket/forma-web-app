<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of webservice-form-documents
 *
 * @author Joshua Clifford Reyes
 */

error_reporting(E_ERROR);

include_once APPLICATION_PATH.'/WebServices/WebService.php';

use WebServices\WebServiceConstant;
use WebServices\WebServiceStatus;
use WebServices\WebServiceException;
use WebServices\WebServiceChecker;

use WebServiceUtilities\WebServiceUtilities; 
use WebServiceManager\WebServiceManager;

$utilities = WebServiceManager::webServiceUtilities();
$utilities::applyWebServiceHeader();

/*
	Authentication
*/
$currentUser = $utilities->applyThirdPartyAuth();

if (!$currentUser) {
    
    $response['status']        = WebServiceStatus::WEBSERVICE_STATUS_ERROR;
    $response['error']         = WebServiceStatus::WEBSERVICE_STATUS_UNAUTHORIZED;
    $response['error_message'] = 'You must be logged in to access this service';
    echo json_encode($response);
    return;
}

$formId          = $utilities::applyWebServiceSqlSanitizationAndEmptyValidation(filter_input(INPUT_GET, 'form_id'), 'form_id');
$offset          = $utilities::applyWebServiceSqlInjectionSanitization(filter_input(INPUT_GET, 'offset'));
$fetchCount      = $utilities::applyWebServiceSqlInjectionSanitization(filter_input(INPUT_GET, 'fetch_count'));
$sortType 		 = $utilities::applyWebServiceSqlSanitizationAndEmptyValidation(filter_input(INPUT_GET, 'sort_type'), 'sort_type');
$returnFields    = $utilities::applyWebServiceSqlInjectionSanitization(filter_input(INPUT_GET, 'return_fields'));

try {

    $offset 	= ($offset) 		? $offset : 0;
    $fetchCount = ($fetchCount) 	? $fetchCount : 50;
    $sortType 	= ($sortType) 		? $sortType : 'desc';

    if ($sortType) {

        $application = WebServiceManager::webServiceApplication('FormDocumentList');
        $objects = array(
            'formId'        => $formId,
            'returnFields'  => $returnFields,
            'sortType'      => $sortType,
            'offset'        => $offset,
            'fetchCount'    => $fetchCount
        );
        $application->initializeFormDocumentList($objects);
        $documents = $application->getFormDocumentList();

    } else {

        $documents = array();

    }

    $this->status      		= WebServiceStatus::WEBSERVICE_STATUS_SUCCESS;
    $this->results     		= $documents;

} catch (WebServiceException $e) {

    $this->status 			= WebServiceStatus::WEBSERVICE_STATUS_FAILED;
    $this->error 			= $e->getCode();
    $this->error_message 	= $e->getMessage();
    
}

include dirname(__FILE__) . "/../view/webservice-api-common-response.phtml";

error_reporting(0);