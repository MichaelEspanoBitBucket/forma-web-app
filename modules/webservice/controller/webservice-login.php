<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of webservice-login
 *
 * @author Joshua Clifford Reyes
 */

error_reporting(E_ERROR);

include_once APPLICATION_PATH.'/WebServices/WebService.php';

use WebServices\WebServiceConstant;
use WebServices\WebServiceStatus;
use WebServices\WebServiceException;
use WebServices\WebServiceChecker;

use WebServiceUtilities\WebServiceUtilities; 
use WebServiceManager\WebServiceManager;

$utilities = WebServiceManager::webServiceUtilities();
$utilities::applyWebServiceHeader();

/*
	For Presentation Purpose the WebService Login can be access via GET method
	or Access via URL in browser wtih proper parameters.

	$webServiceValidated = $webServiceUtil->applyStrictValidationOnWebServiceAccess(WebServiceConstant::WEBSERVICE_PRIVATE_ACCESS);
	if ($webServiceValidated['error']) {
		echo json_encode($webServiceValidated);
		return;
	}
*/

/* 
    allowed input is email and username
*/
try {
    
    $email      = WebServiceChecker::validateEmptyInput(filter_input(INPUT_GET, 'email'), 'email');
    $password   = WebServiceChecker::validateEmptyInput(filter_input(INPUT_GET, 'password'), 'password');

} catch (WebServiceException $e) {
    $response['error'] = $e->error;
    $response['error_message'] = $e->message;
    echo json_encode($response);
    return;
}

if (empty($email) || $email == 'null' || $email == 'Null' || $email == null) {

	$response['error']         = WebServiceStatus::WEBSERVICE_STATUS_INVALID_PARAM;
    $response['error_message'] = 'Email or Username cannot be empty';
    echo json_encode($response);
    return;
}

if (empty($password) || $password == 'null' || $password == 'Null' || $password == null) {

    $response['error']         = WebServiceStatus::WEBSERVICE_STATUS_INVALID_PARAM;
    $response['error_message'] = 'password cannot be empty';
    echo json_encode($response);
    return;
}

/*
    Double check again all the parameters supplied in the request.
*/
WebServiceChecker::validateEmptyInput($email, 'email');
WebServiceChecker::validateEmptyInput($password, 'password');

$session = new Auth();
$session->destroyAuth('current_user');
$encryptedPassword = functions::encrypt_decrypt("encrypt", $password);

try {

    $application = WebServiceManager::webServiceApplication('Login');
    $objects = array(
        'email'             => $email,
        'password'          => $password,
        'encryptedPassword' => $encryptedPassword
    );

    $application->initializeLogin($objects, $session);
    $response = $application->getLoginCredential();
    if (!empty($response['results'])) {
        $this->status = $response['status'];
        $this->results = $response['results'];
    } else {
        $this->status = $response['status'];
        $this->error = $response['error'];
        $this->error_message = $response['errorMessage'];
    }

} catch (WebServiceException $e) {
    echo 'ERROR' . $e;
}

include dirname(__FILE__) . "/../view/webservice-api-common-response.phtml";

error_reporting(0);