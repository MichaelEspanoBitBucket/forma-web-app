<!--<link href='http://fonts.googleapis.com/css?family=Raleway:100' rel='stylesheet' type='text/css'>-->
	<script type="text/javascript" src="/js/library/emotions/jquery.emotions.js"></script>

	<script src="/js/library/ezoom/e-smart-zoom-jquery.min.js"></script>
	<script src="/js/library/panzoom/jquery.panzoom.js"></script>
	
	<!-- Tour -->
	<script src="/js/library/highlight/jquery.highlight-4.js" type="text/javascript"></script>
	
	<!-- Tour -->
	<link rel="stylesheet" type="text/css" href="/css/tour/jquerytour.css" />
	<script src="/js/library/wizard/cufon-yui.js" type="text/javascript"></script>
	<script src="/js/library/wizard/ChunkFive_400.font.js" type="text/javascript"></script>
	<script type="text/javascript">
		Cufon.replace('h1',{ textShadow: '1px 1px #fff'});
		Cufon.replace('h2',{ textShadow: '1px 1px #fff'});
		Cufon.replace('.footer');
	</script>
	
	
	<!-- USER VIEW Javascript -->
	<?php echo $main_controller; ?>
	<script type="text/javascript" src="/js/library/homepage_animation.js"></script>
	<!-- Jquer Alert -->
	<script type="text/javascript" src="/js/library/modal/jquery.alert.js"></script>
	<!-- Jquer Notification -->
	<script type="text/javascript" src="/js/library/notification/jquery_notification_v.1.js"></script>
	<!-- Jquery Time To Go -->
	<script type="text/javascript" src="/js/library/timeTogo/jquery.timeago.js"></script>
	
	<!-- Jquery Functions -->
	<script type="text/javascript" src="/js/functions/functions.js"></script>
	
	<!-- Jquery ScrollBar -->
	<script src="/js/library/scrollbar/jquery.mousewheel.js"></script>
	<script src="/js/library/scrollbar/perfect-scrollbar.js"></script>
	
	<!-- Jquery Spectrum Color Picker -->
	<script src="/js/library/colorPicker/spectrum.js"></script>
	
	<!-- Jquery STEP WIZARD -->
	<script src="/js/library/stepywizard/jquery.stepy.js"></script>
	<link href="/css/stepywizard/jquery.stepy.css" rel="stylesheet">
	
	
	
	<!-- modernizr Functions -->
	<script type="text/javascript" src="/js/library/modernizr/modernizr.js"></script>
	<script type="text/javascript" src="/js/library/modernizr/modernizr.custom.js"></script>
        
	<!-- JCarousel Functions -->
	<script type="text/javascript" src="/js/library/carousel/myCarousel.js"></script>
	
	<!-- Colorbox -->
	<link href="/js/library/colorbox/colorbox.css" rel="stylesheet">
	<script type="text/javascript" src="/js/library/colorbox/jquery.colorbox-min.js"></script>
	
	    
	<!-- Jquery FlexSlider -->
	<link href="/css/flexslider/flexslider.css" rel="stylesheet">
	<script type="text/javascript" src="/js/library/flexslider/jquery.flexslider.js"></script>
	
	<!-- Jquery Snow -->
	<script type="text/javascript" src="/js/library/snow/snow.js"></script>
    <!-- CSS -->
	<!-- CSS ScrollBar -->
	<link href="/css/scrollbar/perfect-scrollbar.css" rel="stylesheet">
	<!-- CSS Notification -->
	<link rel="stylesheet" type="text/css" href="/css/notification/jquery_notification.css" />
	<!-- CSS Tooltip / Popover -->
	<link rel="stylesheet" type="text/css" href="/css/bootstrap/bootstrap.css" />
	<!-- Table / Popover -->
	<link rel="stylesheet" type="text/css" href="/css/table/table.css" />
	<!-- Spectrum Color Picker -->
	<link rel="stylesheet" type="text/css" href="/css/colorPicker/spectrum.css" />

	<link rel="stylesheet" type="text/css" href="/css/datepicker/jquery-ui-1.7.2.custom.css" />

	<!-- CSS customlogincss -->
	<link rel="stylesheet" type="text/css" href="/css/customlogin_<?php echo COMPANY_STYLES; ?>.css" />

	<!-- CSS OrgChart -->
	<link rel="stylesheet" type="text/css" href="/css/organizational_chart.css" />
	
	<!-- CSS Carousel -->
	<link rel="stylesheet" type="text/css" href="/css/carousel/myCarousel.css" />
	
    <!-- CSS Carousel -->
	<link rel="stylesheet" type="text/css" href="/css/mention.css" />
	<script type="text/javascript" src="/js/functions/mention.js"></script>
	<link rel="stylesheet" type="text/css" href="/css/mention_post.css" />

	<script type="text/javascript" src="/js/functions/mention_post.js"></script>
		
        <!--Chat-->
        <link rel="stylesheet" type="text/css" href="/css/chat.css">
        <!-- CSS Fullcalendar -->
	<!-- <link rel="stylesheet" type="text/css" href="/css/fullcalendar/fullcalendar.css" /> -->
	<link rel="stylesheet" type="text/css" href="/css/fullcalendar/updated/fullcalendar.min.css" />
	<!-- <link rel="stylesheet" type="text/css" href="/css/fullcalendar/updated/fullcalendar.print.css" /> -->

	<link rel="stylesheet" type="text/css" href="/css/ruler/ruler.css" />	


     <script type="text/javascript" src="/js/functions/form_builder/formbuilderExtension.js"></script>