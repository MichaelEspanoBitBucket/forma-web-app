var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var del = require('del');
var bower = require('gulp-bower');
var gulpUtil = require('gulp-util');
// include plug-ins
var jshint = require('gulp-jshint');
var runSequence = require('run-sequence');
// JS hint task
var imagemin = require('gulp-imagemin');
var connect = require('gulp-connect-php');
var express = require('express');
var proxy = require('http-proxy-middleware');
var app = express();

app.use('/pf_page', proxy({target: 'http://localhost:8383/', changeOrigin: true}));
app.listen(3030);

var config = {
     bowerDir: './bower_components'
}

gulp.task('bower', function() {
    return bower()
        .pipe(gulp.dest(config.bowerDir))
});


gulp.task('jshint', function() {
  gulp.src('js/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('hello', function() {
  console.log('Hello Zell');
});

gulp.task('fonts', function() {
  return gulp.src('app/bower_components/font-awesome/fonts/*')
  .pipe(gulp.dest('./fonts'))
})


gulp.task('clean:dist', function() {
  return del.sync('dist');
})

gulp.task('useref', function(){
  return gulp.src('app/**/**/*.html')
    .pipe(useref())
    // Minifies only if it's a JavaScript file
    .pipe(gulpIf('app/**/**/*.js', uglify().on('error', gulpUtil.log)))
    // Minifies only if it's a CSS file
    .pipe(gulpIf('app/**/**/*.css', cssnano()))
    .pipe(gulp.dest('.'))
});

gulp.task('images', function(){
  return gulp.src('app/assets/images/**/*.+(png|jpg|jpeg|gif|svg)')
  .pipe(imagemin({
      // Setting interlaced to true
      interlaced: true
    }))
  .pipe(gulp.dest('./assets/images'))
});


gulp.task('browserSync', function() {
// connect.server({
//   base:"pf_page/pfp-api",
//     keepalive: true,
//     port:8383
//   }, function (){
//      browserSync.init({
//       snippetOptions: {
//         ignorePaths: "pfp-api/*.php",
//       },
//       proxy: 'localhost:8383',w
//         port: 3000,
//         open: true,
//         notify: false
//       // server: {
//       //   baseDir: ['pfp-api', 'app']
//       // },
//     })

//   })
browserSync.init({
proxy: {
        target: 'localhost:8383',
        cookies: {stripDomain: true}
    }
// server:{
//   baseDir:'app',
// }

})


  
 
})
gulp.task('sass', function(){
  return gulp.src('app/scss/styles.scss')
    .pipe(sass()) // Converts Sass to CSS with gulp-sass
    .pipe(gulp.dest('app/assets/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});


gulp.task('watch', ["browserSync","sass"], function(){
  gulp.watch('app/scss/**/*.scss', ['sass']); 
  // Other watchers
  // Reloads the browser whenever HTML or JS files change
  gulp.watch('app/**/**/*.html', browserSync.reload); 
  gulp.watch('app/**/**/*.js', browserSync.reload); 
})


gulp.task('build', function (callback) {
  runSequence( 
    ['sass', 'useref','images','fonts'],
    callback
  )
})

gulp.task('default', function (callback) {
  runSequence('bower',['sass','browserSync', 'watch'],
    callback
  )
})