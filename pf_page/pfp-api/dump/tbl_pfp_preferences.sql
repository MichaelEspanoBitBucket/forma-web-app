CREATE TABLE IF NOT EXISTS `tbl_pfp_preferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CompanyId` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Forms` longtext NOT NULL,
  `EnableViewing` tinyint(1) NOT NULL,
  `LoginAnonymously` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;