<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of forma-company
 *
 * @author Joshua Clifford Reyes
 */

error_reporting(E_ERROR);

include_once realpath(dirname(__FILE__) . '/../..').'/WebServices/WebServiceDependencies.php';
include_once APPLICATION_PATH.'/WebServices/WebService.php';

use WebServices\WebServiceConstant;
use WebServices\WebServiceStatus;
use WebServices\WebServiceException;
use WebServices\WebServiceChecker;

use WebServices\WebServiceUtilities; 
use WebServices\WebServiceManager;

/**
    WEBSERVICE VALIDATION
        RULE:
        1. ACCESS METHOD only POST
        2. Auth() is not required
*/
WebServiceUtilities::setWebServiceHeader();
WebServiceUtilities::setWebServiceMethod('POST');

try {

    /**

     * forma-company.php
     *
     * @param String $companyName

     */

    $companyName = WebServiceChecker::validateEmptyInput(filter_input(INPUT_POST, 'companyName'), 'companyName');

    $database = WebServiceManager::webServiceDatabase();
    $db = $database->getConnection('MYSQL', array());
    
    $db->connect();

    $statement = "SELECT * FROM `tbcompany` where `tbcompany`.`name` = :companyName";
    $bindParams = array(
        ':companyName' => $companyName
    );
    $response = $db->query($statement, 'row', $bindParams);

    $db->disconnect();

    if (!empty($response)) {
        WebServiceUtilities::setWebServiceResponse(
            200,
            WebServiceStatus::WEBSERVICE_STATUS_SUCCESS,
            $response,
            null,
            null
        );
    } else {
        WebServiceUtilities::setWebServiceResponse(
            400,
            WebServiceStatus::WEBSERVICE_STATUS_ERROR,
            null,
            WebServiceStatus::WEBSERVICE_REQUEST_FAILED,
            'Failed getting company, response is Empty.'
        );
    }

} catch (WebServiceException $e) {
    WebServiceUtilities::setWebServiceResponse(
        400,
        WebServiceStatus::WEBSERVICE_STATUS_ERROR,
        null,
        WebServiceStatus::WEBSERVICE_REQUEST_FAILED,
        $e->message
    );
}

error_reporting(0);