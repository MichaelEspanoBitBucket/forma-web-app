<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of forma-guest-user-per-company
 *
 * @author Joshua Clifford Reyes
 */

error_reporting(E_ERROR);

include_once realpath(dirname(__FILE__) . '/../..').'/WebServices/WebServiceDependencies.php';
include_once realpath(dirname(__FILE__) . '/../..').'/library/powerForms.php';
include_once APPLICATION_PATH.'/WebServices/WebService.php';

use WebServices\WebServiceConstant;
use WebServices\WebServiceStatus;
use WebServices\WebServiceException;
use WebServices\WebServiceChecker;

use WebServices\WebServiceUtilities; 
use WebServices\WebServiceManager;

/**
    WEBSERVICE VALIDATION
        RULE:
        1. ACCESS METHOD only POST
        2. Auth() is not required
*/
WebServiceUtilities::setWebServiceHeader();
WebServiceUtilities::setWebServiceMethod('POST');

try {

    /**
    
     * forma-guest-user-per-company.php
     *
     * @param Integer $companyId

     */

    $companyId = WebServiceChecker::validateEmptyInput(filter_input(INPUT_POST, 'companyId'), 'companyId');

    $db = new Database();
    $pF = new powerForms();

    $resources = array();
    $resources['company_id'] = $companyId;
    $response = $pF->get_guest_user_per_company($db, $resources);

    if (!empty($response)) {
        WebServiceUtilities::setWebServiceResponse(
            200,
            WebServiceStatus::WEBSERVICE_STATUS_SUCCESS,
            $response,
            null,
            null
        );
    } else {
        WebServiceUtilities::setWebServiceResponse(
            400,
            WebServiceStatus::WEBSERVICE_STATUS_ERROR,
            null,
            WebServiceStatus::WEBSERVICE_REQUEST_FAILED,
            'Failed pf request, response is Empty.'
        );
    }

} catch (WebServiceException $e) {
    WebServiceUtilities::setWebServiceResponse(
        400,
        WebServiceStatus::WEBSERVICE_STATUS_ERROR,
        null,
        WebServiceStatus::WEBSERVICE_REQUEST_FAILED,
        $e->message
    );
}

error_reporting(0);