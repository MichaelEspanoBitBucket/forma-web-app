<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of forma-login
 *
 * @author Joshua Clifford Reyes
 */

error_reporting(E_ERROR);

include_once realpath(dirname(__FILE__) . '/../..').'/WebServices/WebServiceDependencies.php';
include_once APPLICATION_PATH.'/WebServices/WebService.php';

use WebServices\WebServiceConstant;
use WebServices\WebServiceStatus;
use WebServices\WebServiceException;
use WebServices\WebServiceChecker;

use WebServices\WebServiceUtilities; 
use WebServices\WebServiceManager;

/**
    WEBSERVICE VALIDATION
        RULE:
        1. ACCESS METHOD only POST
        2. Auth() is not required
*/
WebServiceUtilities::setWebServiceHeader();
WebServiceUtilities::setWebServiceMethod('POST');

try {

    /**

     * forma-login.php
     *
     * @param Integer $pfpId if Empty then do the common input [$email and $password]
     *
     * @param String $email
     * @param String $password

     */

    $pfpId = filter_input(INPUT_POST, 'pfpId');

    $database = WebServiceManager::webServiceDatabase();
    $db = $database->getConnection('MYSQL', array());
    
    $db->connect();
    
    $statement = "SELECT `tbl_pfp_preferences`.`LoginAnonymously` as LoginAnonymously, `tbl_pfp_preferences`.`LoginAuth` as LoginAuth FROM `tbl_pfp_preferences` WHERE id = :pfpId";
    $bindParams = array(
        ':pfpId' => $pfpId
    );
    $pfpRecord = $db->query($statement, 'row', $bindParams);
    
    if ($pfpRecord['LoginAnonymously'] == 1) {
        $loginAuth = json_decode($pfpRecord['LoginAuth'], true);
        $email    = $loginAuth['UserName'];
        $password = $loginAuth['Password'];
    } else {
        $email    = WebServiceChecker::validateEmptyInput(filter_input(INPUT_POST, 'email'), 'email');
        $password = WebServiceChecker::validateEmptyInput(filter_input(INPUT_POST, 'password'), 'password');
    }

} catch (WebServiceException $e) {
    $response['error'] = $e->error;
    $response['error_message'] = $e->message;
    echo json_encode($response);
    return;
}

if (empty($email) || $email == 'null' || $email == 'Null' || $email == null) {
	$response['error']         = WebServiceStatus::WEBSERVICE_STATUS_INVALID_PARAM;
    $response['error_message'] = 'Email or Username cannot be empty';
    echo json_encode($response);    
    return;
}

if (empty($password) || $password == 'null' || $password == 'Null' || $password == null) {
    $response['error']         = WebServiceStatus::WEBSERVICE_STATUS_INVALID_PARAM;
    $response['error_message'] = 'password cannot be empty';
    echo json_encode($response);
    return;
}

/*
    Third Party Library Class
*/
$session = new Auth();
$session->destroyAuth('current_user');
$encryptedPassword = functions::encrypt_decrypt("encrypt", $password);

try {

    $application = WebServiceManager::webServiceApplication('Login');
    $objects = array(
        'email'             => $email,
        'password'          => $password,
        'encryptedPassword' => $encryptedPassword
    );

    $application->initializeLogin($objects, $session);
    $response = $application->getLoginCredential();
    
    if (!empty($response['results'])) {
        WebServiceUtilities::setWebServiceResponse(
            200,
            WebServiceStatus::WEBSERVICE_STATUS_SUCCESS,
            $response,
            null,
            null
        );
    } else {
        WebServiceUtilities::setWebServiceResponse(
            400,
            WebServiceStatus::WEBSERVICE_STATUS_ERROR,
            null,
            $response['error'],
            $response['errorMessage']
        );
    }

} catch (WebServiceException $e) {
    WebServiceUtilities::setWebServiceResponse(
        400,
        WebServiceStatus::WEBSERVICE_STATUS_ERROR,
        null,
        WebServiceStatus::WEBSERVICE_REQUEST_FAILED,
        $e->message
    );
}

error_reporting(0);