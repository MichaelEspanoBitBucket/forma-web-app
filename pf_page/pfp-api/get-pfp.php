<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of get-pfp
 *
 * @author Joshua Clifford Reyes
 */

error_reporting(E_ERROR);

include_once realpath(dirname(__FILE__) . '/../..').'/WebServices/WebServiceDependencies.php';
include_once APPLICATION_PATH.'/WebServices/WebService.php';

use WebServices\WebServiceConstant;
use WebServices\WebServiceStatus;
use WebServices\WebServiceException;
use WebServices\WebServiceChecker;

use WebServices\WebServiceUtilities; 
use WebServices\WebServiceManager;

/**
    WEBSERVICE VALIDATION
        RULE:
        1. ACCESS METHOD only POST
        2. Auth() is required
*/
WebServiceUtilities::setWebServiceHeader();
WebServiceUtilities::setWebServiceMethod('POST');

$currentUser = Auth::getAuth('current_user');
if (!$currentUser) {
    WebServiceUtilities::setWebServiceResponse(
        401,
        WebServiceStatus::WEBSERVICE_STATUS_ERROR,
        null,
        WebServiceStatus::WEBSERVICE_UNAUTHORIZED,
        'Need to be authenticated.'
    );

    return;
}

try {

    /**

     * get-pfp.php
     *
     * @param no available parameter

     */

    $database = WebServiceManager::webServiceDatabase();
    $db = $database->getConnection('MYSQL', array());
    
    $db->connect();

    $getPfpStatement = "SELECT * FROM `tbl_pfp_preferences` WHERE CompanyId = :companyId";
    $getPfpBindParams = array(
        ':companyId' => $currentUser['company_id']
    );
    $getPfp = $db->query($getPfpStatement, 'row', $getPfpBindParams);

    $getCompanyStatement = "SELECT * FROM `tbcompany` where `tbcompany`.`id` = :companyId";
    $getCompanyBindParams = array(
        ':companyId' => $currentUser['company_id']
    );
    $getCompanyInfo = $db->query($getCompanyStatement, 'row', $getCompanyBindParams);

    $response = array(
        'PFP_INFO' => $getPfp,
        'COMPANY_INFO' => $getCompanyInfo,
        'USER_INFO' => array(
            'id' => $currentUser['id'],
            'email' => $currentUser['email'],
            'display_name' => $currentUser['display_name'],
            'user_level_id' => $currentUser['user_level_id']
            
            /*
                Unused Data

                'company_id' => $currentUser['company_id'],
                'contact_number' => $currentUser['contact_number'],
                'date_registered' => $currentUser['date_registered'],
                'department_id' => $currentUser['department_id'],
                'department_position_level' => $currentUser['department_position_level'],
                'email_activate' => $currentUser['email_activate'],
                'extension' => $currentUser['extension'],
                'first_name' => $currentUser['first_name'],
                'forgot_password' => $currentUser['forgot_password'],
                'is_active' => $currentUser['is_active'],
                'is_available' => $currentUser['is_available'],
                'last_name' => $currentUser['last_name'],
                'middle_name' => $currentUser['middle_name'],
                'position' => $currentUser['position'],
                'timeout' => $currentUser['timeout'],
                'username' => $currentUser['username'],
                'password' => $currentUser['password']
            */
        )
    );

    $db->disconnect();

    if (!empty($response)) {
        WebServiceUtilities::setWebServiceResponse(
            200,
            WebServiceStatus::WEBSERVICE_STATUS_SUCCESS,
            $response,
            null,
            null
        );
    } else {
        WebServiceUtilities::setWebServiceResponse(
            400,
            WebServiceStatus::WEBSERVICE_STATUS_ERROR,
            null,
            WebServiceStatus::WEBSERVICE_REQUEST_FAILED,
            'Failed getting Pfp record list, response is Empty.'
        );
    }

} catch (WebServiceException $e) {
    WebServiceUtilities::setWebServiceResponse(
        400,
        WebServiceStatus::WEBSERVICE_STATUS_ERROR,
        null,
        WebServiceStatus::WEBSERVICE_REQUEST_FAILED,
        $e->message
    );
}

error_reporting(0);