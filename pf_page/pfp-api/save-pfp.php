<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of save-pfp
 *
 * @author Joshua Clifford Reyes
 */

error_reporting(E_ERROR);

include_once realpath(dirname(__FILE__) . '/../..').'/WebServices/WebServiceDependencies.php';
include_once APPLICATION_PATH.'/WebServices/WebService.php';

use WebServices\WebServiceConstant;
use WebServices\WebServiceStatus;
use WebServices\WebServiceException;
use WebServices\WebServiceChecker;

use WebServices\WebServiceUtilities; 
use WebServices\WebServiceManager;

/**
    WEBSERVICE VALIDATION
        RULE:
        1. ACCESS METHOD only POST
        2. Auth() is required
*/
WebServiceUtilities::setWebServiceHeader();
WebServiceUtilities::setWebServiceMethod('POST');

$currentUser = Auth::getAuth('current_user');
if (!$currentUser) { 
    WebServiceUtilities::setWebServiceResponse(
        401,
        WebServiceStatus::WEBSERVICE_STATUS_ERROR,
        null,
        WebServiceStatus::WEBSERVICE_UNAUTHORIZED,
        'Need to be authenticated.'
    );

    return;
}

try {

    /**

     * save-pfp.php
     *
     * @param String $pfpName
     * @param Array Json $pfpForms
     * @param Integer $pfpEnableViewing 1 or 0 Flag
     * @param Integer $pfpLoginAnonymously 1 or 0 Flag
     
     */

    $pfpName             = WebServiceChecker::validateEmptyInput(filter_input(INPUT_POST, 'pfpName'), 'pfpName');
    $pfpForms            = WebServiceChecker::validateEmptyInput(filter_input(INPUT_POST, 'pfpForms'), 'pfpForms');
    $pfpEnableViewing    = filter_input(INPUT_POST, 'pfpEnableViewing') || 1;
    $pfpLoginAnonymously = filter_input(INPUT_POST, 'pfpLoginAnonymously') || 1;

    $database = WebServiceManager::webServiceDatabase();
    $db = $database->getConnection('MYSQL', array());
    
    $db->connect();

    $statement = "SELECT `tbl_pfp_preferences`.`CompanyId` as CompanyId 
                    FROM `tbl_pfp_preferences` 
                  WHERE `tbl_pfp_preferences`.`CompanyId` = :companyId";
    $bindParams = array(':companyId' => $currentUser['company_id']);
    
    $record = $db->query($statement, 'row', $bindParams);

    $data = array(
        'CompanyId'        => $currentUser['company_id'],
        'Name'             => $pfpName,
        'Forms'            => $pfpForms,
        'EnableViewing'    => $pfpEnableViewing,
        'LoginAnonymously' => $pfpLoginAnonymously
    );
    if (empty($record['CompanyId'])) {
        $response = $db->insert('tbl_pfp_preferences', $data);
    } else {
        $condition = array('CompanyId' => $currentUser['company_id']);
        $response = $db->update('tbl_pfp_preferences', $data, $condition);
    }

    $db->disconnect();

    if (!empty($response)) {
        WebServiceUtilities::setWebServiceResponse(
            200,
            WebServiceStatus::WEBSERVICE_STATUS_SUCCESS,
            $response,
            null,
            null
        );

    } else {
        WebServiceUtilities::setWebServiceResponse(
            400,
            WebServiceStatus::WEBSERVICE_STATUS_ERROR,
            null,
            WebServiceStatus::WEBSERVICE_REQUEST_FAILED,
            'Failed saving Pfp, response is Empty.'
        );
    }

} catch (WebServiceException $e) {
    WebServiceUtilities::setWebServiceResponse(
        400,
        WebServiceStatus::WEBSERVICE_STATUS_ERROR,
        null,
        WebServiceStatus::WEBSERVICE_REQUEST_FAILED,
        $e->message
    );
}

error_reporting(0);