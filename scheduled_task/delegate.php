<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getPathIndex() {
    $path = realpath(dirname(__FILE__));
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        $delimiter = "\\";
    } else {
        $delimiter = "/";
    }
    $path_arr = explode($delimiter, $path);
    $path_index = "";
    for ($ctr = 0; $ctr < count($path_arr) - 1; $ctr++) {
        $path_index.=$path_arr[$ctr] . $delimiter;
    }

    $path_index.="index.php";
    return $path_index;
}


include getPathIndex();

define('SMTP_USERNAME', 'gs3.aspdevelopment@gmail.com');
define('SMTP_PASSWORD', '@dministr@t0r');
define('SMTP_PORT', 465);
define('SMTP_HOST', 'smtp.gmail.com');
define('SMTP_AUTH', true);
define('SMTP_SECURE', 'ssl');
define('SMTP_FROM_NAME', 'Formalistics Web Application');
define('SMTP_FROM_EMAIL', 'formalistics@gmail.com');

//var_dump(SMTP_USERNAME);

$db = new Database();
$date = functions::currentDateTime();

$forms = $db->query("select id, form_table_name, company_id, form_name from tb_workspace where is_active = 1 ORDER BY form_name ASC");
$query = "SELECT "
        . " delegate.user_delegate_id as delegate_id, "
        . " processor_delegate.display_name as delegate_display_name, "
        . " processor_delegate.email as delegate_email, "
        . " processor.id AS processor_id,  "
        . " processor.email AS processor_email, "
        . " processor.display_name AS processor_display_name "
        . " FROM tbdelegate delegate"
        . " LEFT JOIN tbuser processor"
        . " ON processor.id = delegate.user_id"
        . " LEFT JOIN tbuser processor_delegate "
        . " ON processor_delegate.id = delegate.user_delegate_id "
        . " WHERE (DATE(now()) BETWEEN DATE(start_date) AND DATE(end_date)) ";

$result = $db->query($query, "array");
$from = SMTP_FROM_EMAIL;
$from_title = SMTP_FROM_NAME;
$subject = "Delegated Requests(s)";

foreach ($result as $row) {
    //get processor position
    $to = array();
    $to[$row["delegate_email"]] = $row["delegate_display_name"];
    $cc = array();
    $cc[$row["processor_email"]] = $row["processor_display_name"];
    $request_count = 0;
    $message = "<div style='float:left;width:100%;margin-bottom: 30px;font-size:11px;background-color:#fff;'>";
    $message .= "<div style='padding:20px;color: #222222; font-family: Arial; font-size: 14px;line-height: 150%;'>";
    $message .= $row["processor_display_name"] . " delegated the following request(s) for your action.<br/><br/>Click on the Tracking Number to open the request.<br/><br/>";

    foreach ($forms as $form) {
        $request_query = "SELECT * FROM (SELECT"
                . " request.ID,"
                . " request.TrackNo, "
                . " CASE request.ProcessorType
                        WHEN 1 THEN getDepartmentUsers(request.Processor, request.ProcessorLevel, {$form["company_id"]})
                        WHEN 2 THEN getUsersByPosition(request.Processor)
                        ELSE request.Processor
                    END AS processors_id "
                . " FROM {$form["form_table_name"]} request WHERE request.enable_delegate = 1) A WHERE FIND_IN_SET({$row["processor_id"]}, A.processors_id)";

        $requests = $db->query($request_query, "array");

        if (count($requests) > 0) {
            $request_count++;
            $message .= "<b>" . $form["form_name"] . " (" . count($requests) . ")</b><br/>";
        }

        foreach ($requests as $request) {
            $link = "<a href='" . MAIN_PAGE . "user_view/workspace?view_type=request&formID={$form["id"]}&requestID={$request["ID"]}&trackNo={$request["TrackNo"]}'>"
                    . "{$request["TrackNo"]}</a>";
            $message.=$link . "<br/><br/>";

            $notif_message = array("type" => "0",
                "message" => "A request with TrackNo {$request["TrackNo"]} has been delegated to you by {$row["processor_email"]}.");
//            notifications::insertNofiRequest($request["ID"], $row["processor_id"], $form["form_table_name"], $row["delegate_id"], $notif_message);

            $type = "5";
            $insert = array("noti_id" => $request["ID"],
                "type" => $type,
                "userID" => $row["delegate_id"],
                "user_read" => 0,
                "noti_by" => $row["processor_id"],
                "date" => $date,
                "table_name" => $form["form_table_name"],
                "request_noti_message" => json_encode($notif_message),
                "is_active" => 1);

            $db->insert("tbnotification", $insert);
            var_dump($insert);
        }
    }

    $message .= "</div>";
    $message .= "</div>";
    $message .= "</div>";

//    print_r($message);

    if ($request_count > 0) {
        var_dump($to);
        var_dump($cc);
        var_dump($subject);
        var_dump($message);
        $mailDoc = new Mail_Notification();
        $mailDoc->sendEmail_smtp($message, $to, $cc, null, $from, null, $from_title, $subject);
        var_dump("MAIL SENT..");
    }
}
