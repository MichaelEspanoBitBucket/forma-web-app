<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include 'C:\wamp\www\Projects\eformalistics\Formalistics\index.php';
//include 'C:\xampp\htdocs\eforms\eformalistics\Formalistics\index.php';

define("FIRST_HALF_CUTOFF", 10);
define("SECOND_HALF_CUTOFF", 25);

class Paysheet {

    public function generate() {
        $db = new Database();
        $date = $db->currentDateTime();
        
        //first cut off
        $date_range = $this->getCutOff(FIRST_HALF_CUTOFF);
        if($date_range){
            $data = array();
            $start_rangeObject = $date_range["Start Date"];
            $end_rangeObject = $date_range["End Date"];
            
            $timesheets = $this->getTimeSheets($date_range);
            
            foreach ($timesheets as $timesheet) {
                $requestDoc = new Request();
                $requestDoc->load(234, 0);
                $data["PaysheetManNum"] = $timesheet["TSmanNum"];
                $data["PayPeriodStart"] = $start_rangeObject->format('Y-m-d');
                $data["PayPeriodEnd"] = $end_rangeObject->format('Y-m-d');
                $data['Requestor'] = "240";
                $data['Processor'] = "240";
                $data['Status'] = 'Draft';
                $data['imported'] = '1';
                $data['DateCreated'] = $date;
                $data['DateUpdated'] = $date;
                $requestDoc->data = $data;
                
                print_r($requestDoc->data);
                $requestDoc->save();
                
            }    
        }
        
        //second cut off
        $date_range = $this->getCutOff(SECOND_HALF_CUTOFF);
        if($date_range){
            $data = array();
            $start_rangeObject = $date_range["Start Date"];
            $end_rangeObject = $date_range["End Date"];
            
            $timesheets = $this->getTimeSheets($date_range);
            
            foreach ($timesheets as $timesheet) {
                $requestDoc = new Request();
                $requestDoc->load(234, 0);
                $data["PaysheetManNum"] = $timesheet["TSmanNum"];
                $data["PayPeriodStart"] = $start_rangeObject->format('Y-m-d');
                $data["PayPeriodEnd"] = $end_rangeObject->format('Y-m-d');
                $data['Requestor'] = "240";
                $data['Processor'] = "240";
                $data['Status'] = 'Draft';
                $data['imported'] = '1';
                $data['DateCreated'] = $date;
                $data['DateUpdated'] = $date;
                $requestDoc->data = $data;
                
                print_r($requestDoc->data);
                $requestDoc->save();
                
            }    
        }
        
    }

    private function getTimeSheets($date_range) {
        $db = new Database();
        $rangeObject = $date_range["Start Date"];

        if ($date_range) {
            $result = $db->query("SELECT TSmanNum FROM 15_tbl_timesheet WHERE DatePresent >={$db->escape($rangeObject->format('Y-m-d'))} GROUP BY TSmanNum", "array");
        }

        return $result;
    }

    private function getCutOff($cutt_off) {
        $start_date = new DateTime();
        $end_date = new DateTime();
        $date_now = date("d");
        $month_now = date("m");
        $year_now = date("Y");

        if ($cutt_off == $date_now) {
            if ($cutt_off < 15) {
                $difference = 15 - $cutt_off + 1;
                $start_date->modify("-1 month");
                $start_date->modify("+" . $difference . " day");

                $end_date->modify("-1 month");
                $end_date->modify("+" . $difference . " day");
            }

            if ($cutt_off >= 15) {
                $start_date->modify("-14 day");
                $end_date->modify("-14 day");
            }

            $end_date->modify("+14 day");
            $return_array = array("Start Date" => $start_date,
                "End Date" => $end_date);
        } else {
            $return_array = array();
        }

        return $return_array;
    }

}

$paysheetObject = new Paysheet();
$paysheetObject->generate();
