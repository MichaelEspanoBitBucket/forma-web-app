<?php

class PrintLog {
	public $print_LOGS = "";
	public function print_log($Str_LOG,$print_control = false){
		$br_line = "\r\n";
		$this->print_LOGS .= print_r($br_line,true);
		$this->print_LOGS .= print_r($Str_LOG,true);
		$this->print_LOGS .= print_r($br_line,true);
	}
}


$timezone = "Asia/Manila";

if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
//include 'C:\wamp\www\Projects\eformalistics\Formalistics\index.php'; // PROD
//include 'J:\eformalistics\Formalistics\index.php'; //TEST SERVER
include 'C:\wamp\www\eforms\eformalistics\Formalistics\index.php';

$php_ConsLog = new PrintLog();

for($ctr = 0 ; $ctr < 50 ; $ctr++){
	$php_ConsLog->print_log("\r\n");
}

$updated_fields = 0;
$updated_fields_log = "";
$print_control = false;
$php_ConsLog->print_log($br_line."========================START MIDDLE WAR RULE=======================================".$br_line);
$db = new Database();
$get_mid_set = $db->query("SELECT * FROM tbmiddleware_settings WHERE is_active = 1","array");
foreach($get_mid_set as $tbl_mid_set_each_row){
	$date_start = strtotime($tbl_mid_set_each_row["scheduled_process_start"]);
	$date_end = strtotime($tbl_mid_set_each_row["scheduled_process_end"]);
	$date_now = strtotime(date('Y-m-d H:i:s'));


	$php_ConsLog->print_log("DATE: ".(date("Y-m-d H:i:s",$date_now)." >= ".date("Y-m-d H:i:s",$date_start))." and ".(date("Y-m-d H:i:s",$date_now)." <= ".date("Y-m-d H:i:s",$date_end))." = (".($date_now >= $date_start)." and ".($date_now <= $date_end).")");

	if($date_now >= $date_start and $date_now <= $date_end){ //DATE START END CONDITION
		$php_ConsLog->print_log("DATE PASOK");


		// // Get All Forms
		$forms = $db->query(
			"SELECT id, form_json, form_table_name, form_name, active_fields, company_id
			FROM tb_workspace
			WHERE is_delete = 0  AND id = ".$tbl_mid_set_each_row["formname"],"array");
		foreach($forms as $form){
			// // Get all form records submitted
			// $php_ConsLog->print_log("Get all form records submitted");
			$form_table_name = $form['form_table_name'];
			$form_json = $form['form_json'];
			$decode_form_json = json_decode($form_json,true);
			$middleware_formula = $decode_form_json['middleware'];
			$getRequest = $db->query("SELECT * FROM " . $form_table_name,"array");

			$php_ConsLog->print_log($form_table_name);

			foreach($getRequest as $request){

				$php_ConsLog->print_log($tbl_mid_set_each_row["formula"]);
				$evaluated = false;
				$mid_set_formula = new Formula($tbl_mid_set_each_row["formula"]);
	            $mid_set_formula->DataFormSource[0] = $request;
	            try{
	            	$php_ConsLog->print_log($mid_set_formula->replaceFormulaFieldNames());
	            	$evaluated = $mid_set_formula->evaluate();
	            }catch(Exception $e){
					$php_ConsLog->print_log("Caught exception: ",  $e->getMessage(), "\n");
					$evaluated = false;
	            }
	            if($evaluated == true || $evaluated == "true"){
	            	$php_ConsLog->print_log("NAG TRUE");
	            	$php_ConsLog->print_log($tbl_mid_set_each_row);
	            	
	            	$json_enc_actions = json_decode($tbl_mid_set_each_row["actions"],true);
	            	
	            	if(!empty($json_enc_actions) or count($json_enc_actions) > 0){
	            		$php_ConsLog->print_log($tbl_mid_set_each_row["actions"]);
	            		$php_ConsLog->print_log($json_enc_actions);

	            		foreach ($json_enc_actions as $key => $value) {
		            		$php_ConsLog->print_log($json_enc_actions[$key]['name']);
		            		$action_name = $json_enc_actions[$key]['name'];
		            		$action_value = $json_enc_actions[$key]['value'];
		            		
		            		if($action_name == "create_response"){
		            			//DO CREATE RESPONSE
		            		}else if($action_name == "update_response"){
		            			//DO UPDATE RESPONSE
		            		}else if($action_name == "send_sms"){
		            			//DO SMS
		            		}else if($action_name == "send_email"){
		            			//DO EMAIL
		            		}
		            	}
	            	}
	            }
			}
		}
	}


	$php_ConsLog->print_log("NEXT...");
}

echo $php_ConsLog->print_LOGS;

?>