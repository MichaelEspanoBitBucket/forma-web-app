<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getPathIndex() {
    $path = realpath(dirname(__FILE__));
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        $delimiter = "\\";
    } else {
        $delimiter = "/";
    }
    $path_arr = explode($delimiter, $path);
    $path_index = "";
    for ($ctr = 0; $ctr < count($path_arr) - 1; $ctr++) {
        $path_index.=$path_arr[$ctr] . $delimiter;
    }

    $path_index.="index.php";
    return $path_index;
}

include getPathIndex();
$conn = getCurrentDatabaseConnection();
$fs = new functions();

$mails = $conn->query("SELECT * FROM tbemail_logs WHERE Status = 'Failed'");
foreach ($mails as $mail) {
    var_dump($mail);
    $fs->sendEmail_smtp($mail["body"], json_decode($mail["mail_to"], true), json_decode($mail["cc"], true), json_decode($mail["bcc"], true), $mail["mail_from"], $mail["to_title"], $mail["from_title"], $mail["mail_title"], $mail["id"]);
}
