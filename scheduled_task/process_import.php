<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getPathIndex() {
    $path = realpath(dirname(__FILE__));
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        $delimiter = "\\";
    } else {
        $delimiter = "/";
    }
    $path_arr = explode($delimiter, $path);
    $path_index = "";
    for ($ctr = 0; $ctr < count($path_arr) - 1; $ctr++) {
        $path_index.=$path_arr[$ctr] . $delimiter;
    }

    $path_index.="index.php";
    return $path_index;
}

include getPathIndex();

// error_reporting(E_ALL);
$db = new Database();
$myRequest = new Request();

$options = getopt("f:r:o:c:a:m:n:u:k:v:s:");
$formID = $options["f"];
$import_update_reference_field = $options["r"];
$import_option = $options["o"];
$user_id = $options["u"];
$file_name = $options["c"];
$action = $options["a"];
$update_option = $options["m"]? : "";
$import_update_add_value_field = $options["n"]? : "";


$embed_ref_key = $options["k"]? : "";
$embed_ref_val = $options["v"]? : "";
$embed_submission = $options["s"]? : "";

$location = APPLICATION_PATH . "/logs/import_logs/tmp/" . $file_name;
$auth = $db->query("SELECT * FROM tbuser WHERE id = {$user_id}", "row");
$personDoc = new Person($db, $auth["id"]);
if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){
    $redis_cache = getRedisConnection();
}
$fs = new functions;

var_dump("form id = " . $formID);
var_dump("import option = " . $import_option);
var_dump("import update reference field = " . $import_update_reference_field);
var_dump("user id = " . $user_id);
var_dump("file name = " . $file_name);
var_dump("location = " . $location);
var_dump("action = " . $action);
var_dump("update_option  = " . $update_option);
var_dump("import_update_add_value_field  = " . $import_update_add_value_field);


//var_dump($auth);
$formDoc = new Form($db, $formID);
$return_array = import::importCSV($location);
$fiels = array();
if ($embed_ref_key != "" && $embed_ref_val != "") {
    $index = array_search($embed_ref_key, $return_array[0]);
    for ($i = 1; $i < count($return_array); $i++) {

        $return_array[$i][$index] = $embed_ref_val;
    }
}

var_dump($return_array);


//mail info
$to = array();
$to[$auth["email"]] = $auth["display_name"];
$from = SMTP_FROM_EMAIL;
$from_title = SMTP_FROM_NAME;
$subject = "Formalistics Import Details";


$headers = array();
$fieldValues = array();
foreach ($return_array as $key => $value) {
    if ($key == 0) {
        array_push($headers, $value);
    }
    // Remove By Sam
    // else {
    //     $append = false;
    //     foreach ($value as $val) {
    //         if (trim($val) != "") {
    //             $append = true;
    //             break;
    //         }
    //     }
    //     // $colname = $return_array[$key];
    //     if ($append == true) {
    //         // if(in_array($field['COLUMN_NAME'], $encrypted_selected_flds['encrypt'])){
    //         //     $data_to_encrypt = $fs->encrypt_decrypt("encrypt",$_POST[$field['COLUMN_NAME']]);
    //         //     $fields[$field['COLUMN_NAME']] = $data_to_encrypt;
    //         // }else if(in_array($field['COLUMN_NAME'], $encrypted_selected_flds['decrypt'])){
    //         //     $fields[$field['COLUMN_NAME']] = $_POST[$field['COLUMN_NAME']];
    //         // }else{
    //         //     $fields[$field['COLUMN_NAME']] = $_POST[$field['COLUMN_NAME']];
    //         // }
    //         // var_dump($value);
    //         array_push($fieldValues, $value);
    //         // $fieldValues[$colname] = $value;
    //     }
    //  }
}

// For Data Encryption (Added by Sam)

$encrypted_selected_flds = $myRequest->encrypt_decrypt_request_details($formDoc->form_json);


// Set header as key of values (For Encryption Sam)
$import_header = array_shift($return_array);

$import_datas = array_map(function($v)use($import_header) {
    return array_combine($import_header, $v);
}, $return_array);


foreach ($import_datas as $key => $value) {

    $set_data = array();

    $import_values = $import_datas[$key];
    foreach ($import_values as $keys => $values) {

        // if (trim($values) != "") {
        if (in_array($keys, $encrypted_selected_flds['encrypt'])) {
            $data_to_encrypt = $fs->encrypt_decrypt("encrypt", $values);
            $set_data[] = $data_to_encrypt;
        } else if (in_array($keys, $encrypted_selected_flds['decrypt'])) {
            $set_data[] = $values;
        } else {
            $set_data[] = $values;
        }
        // }
    }
    $fieldValues[$key] = $set_data;
}

// var_dump($fieldValues);
// return false;


if ($import_option == 1) {
    $non_existing_docs = array();

    $reference_counter_index = array_search($import_update_reference_field, $headers[0]);
    $reference_array = array();
    foreach ($fieldValues as $index) {
        array_push($reference_array, $db->escape($index[$reference_counter_index]));
    }

    $select_query = "SELECT GROUP_CONCAT({$import_update_reference_field}) as fields FROM {$formDoc->form_table_name} "
            . " WHERE {$import_update_reference_field} IN (" . implode(", ", $reference_array) . ")";

    $requests = $db->query($select_query, "row");
    if ($requests["fields"] != "") {
        $requests_arr = explode(",", $requests["fields"]);
    }

    $requests_arr = array_unique($requests_arr);

    $reference_counter_index = array_search($import_update_reference_field, $headers[0]);
    $update_sql = " UPDATE " . $formDoc->form_table_name . " SET middleware_process=0, ";
    $reference_array = array();
    foreach ($fieldValues as $index) {
        if ($requests_arr) {
            $exists = array_search($index[$reference_counter_index], $requests_arr);
        } else {
            $exists = false;
        }

        foreach ($index as $key => $val) {
            if ($exists !== FALSE) {

                if ($update_option == "0" || $update_option == "") {
                    $update_sql.= "{$headers[0][$key]} = IF({$headers[0][$reference_counter_index]}={$db->escape($index[$reference_counter_index])}, {$db->escape($val)},{$headers[0][$key]}) ,";
                } else {
                    if ($headers[0][$key] == $import_update_add_value_field) {
                        $update_sql.= "{$headers[0][$key]} = IF({$headers[0][$reference_counter_index]}={$db->escape($index[$reference_counter_index])}, {$headers[0][$key]}  + {$val},{$headers[0][$key]}) ,";
                    } else {
                        $update_sql.= "{$headers[0][$key]} = IF({$headers[0][$reference_counter_index]}={$db->escape($index[$reference_counter_index])}, {$db->escape($val)},{$headers[0][$key]}) ,";
                    }
                }
            }
        }
        if ($exists !== FALSE) {
            array_push($reference_array, $db->escape($index[$reference_counter_index]));
        } else {
            array_push($non_existing_docs, $index);
        }
    }

    $update_sql = substr($update_sql, 0, -1);
    $update_sql.=" WHERE {$headers[0][$reference_counter_index]} IN (" . implode(", ", $reference_array) . ")";
//        print_r($update_sql);

    $update_count = count($reference_array);
    if ($action == 1) {
        print_r($update_sql);
        $db->query($update_sql);
    } else {
        $update_count = 0;
    }

    $fieldValues = $non_existing_docs;
}


$fields_arr = $db->query("SELECT field_name, field_type, field_input_type FROM tbfields WHERE form_id={$formID}");
$fields_array = array();

foreach ($fields_arr as $field) {
    $fields_array[$field["field_name"]] = array("field_type" => $field["field_type"]
        , "field_input_type" => $field["field_input_type"]);
}


$result = array();
$date = $fs->currentDateTime();
$workflow_object = functions::getFormActiveWorkflow($formDoc);
$workflow_json = json_decode($workflow_object["json"], true);

$all_result = array();
foreach ($fieldValues as $key => $value) {
    $result = null;
    foreach ($fieldValues[$key] as $key_1 => $value_1) {
        $col_value = trim($fieldValues[$key][$key_1]);
        $field_reference = $headers[0][$key_1];
        $field_reference_type = $fields_array[$field_reference]["field_type"];
        $field_reference_input_type = $fields_array[$field_reference]["field_input_type"];

        var_dump($field_reference_input_type);
        switch ($field_reference_input_type) {
            case "Number":
                $result[$field_reference] = str_replace(",", "", $col_value);
                break;
            case "Currency":
                $result[$field_reference] = str_replace(",", "", $col_value);
                break;
            default :
                $result[$field_reference] = $col_value;
                break;
        }

        var_dump($result[$field_reference]);
        var_dump($col_value);
    }
    $result['imported'] = '1';
    $result['Requestor'] = $auth['id'];
    $result['UpdatedBy'] = $auth['id'];
    $result['Workflow_ID'] = $workflow_object["workflow_id"];
    $result['Node_ID'] = $workflow_json["workflow-default-action"];
    $result['DateCreated'] = $date;

//        var_dump($result);
    array_push($all_result, $result);
//        $newRequest = new Request();
//        $newRequest->load($formID, '0');
//        $newRequest->data = $result;
//        $newRequest->save();
//        $newRequest->processWF();
}

//modified by joshua reyes
$insertSQL = "INSERT INTO " . $formDoc->form_table_name;
//HEADERS
$ctr = 1;
// $data_collector = array();
foreach ($all_result as $key => $result) {
//        var_dump($result);
    if ($ctr == 1) {
        //headers
        $insertSQL .= " (";
        $keyctr = 1;
        foreach ($result as $key => $value) {
            if (count($result) == $keyctr) {
                $insertSQL.=" `" . $key . "` ";
            } else {
                $insertSQL.=" `" . $key . "`, ";
            }
            $keyctr++;
        }
        $insertSQL .= ") VALUES ";
    }
    $keyctr = 1;
    foreach ($result as $key => $value) {
        if ($keyctr == 1) {
            $insertSQL .= "(";
        }
        if (count($result) == $keyctr) {
            if (count($all_result) == $ctr) {
                $insertSQL.= $db->escape(utf8_encode($value)) . ") ";
            } else {
                $insertSQL.=$db->escape(utf8_encode($value)) . "), ";
            }
        } else {
            $insertSQL.=$db->escape(utf8_encode($value)) . ", ";
        }
        $keyctr++;
    }

    // $data_collector[$ctr] = $result;
    $ctr++;
}

//added by joshua reyes
// include_once API_LIBRARY_PATH . 'API.php';
// $apiDB = new APIDatabase();
// $dbBQ = new DatabaseBulkQueries();
// $apiDB->connect();

// $currentMaxID = 0;
// if ($embed_ref_key != "" && $embed_ref_val != "") {
//     $max_id = $apiDB->query('SELECT COALESCE(max(id), 0) + 1 as MaxID FROM ' . $formDoc->form_table_name . ' limit 1', 'row');
//     $currentMaxID = $max_id['MaxID'] - 1;
//     $insert_result = $max_id['MaxID'];
// } else {
//     $max_id = $apiDB->query('SELECT COALESCE(max(id), 0) + 1 as MaxID FROM ' . $formDoc->form_table_name . ' limit 1', 'row');
//     $insert_result = $max_id['MaxID'];
// }

// if (count($data_collector) > 0) {
//     $apiDB->beginTransaction();

//     $dbBQ->init($data_collector, $formDoc->form_table_name, 'LOAD_DATA_INFILE', array(
//         'action' => 'write_data',
//         'query_handler' => 'mysql_query',
//         'trigger_errors' => false,
//         'clean_memory' => true,
//         'link_identifier' => null
//             )
//     );
//     $dbBQ->load();
//     $dbBQ->execute();
//     $dbBQ->loadDataInFileCleanUp();
//     $apiDB->commit();
// }


// $apiDB->disconnect();
$insert_result = $db->query($insertSQL, "insert");


if ($embed_ref_key != "" && $embed_ref_val != "") {
    var_dump($currentMaxID);
    $insert_result = $db->query('SELECT id as MaxID FROM ' . $formDoc->form_table_name . ' WHERE id > ' . $currentMaxID . ' limit 1', 'row')['MaxID'];
}

$newRequest = new Request();
$newRequest->load($formID, $insert_result, $personDoc);
$newRequest->data = $all_result[0];

if ($workflow_json["workflow-default-action"] == "cancel") {
    $newRequest->data["Status"] = "Cancelled";
    $newRequest->data["Processor"] = "";
    $newRequest->data["LastAction"] = "";
    $newRequest->modify();
} else {
    $nextNode = $newRequest->getNextWorflowNode();
    $newRequest->send_email = false;
    $newRequest->send_notification = false;
    $newRequest->process_triggers = false;
    if ($embed_submission != "" && $embed_submission != "default") {
        $newRequest->data['Status'] = "DRAFT";
        $newRequest->data['Processor'] = $auth['id'];
        $newRequest->data['LastAction'] = $workflow_object['buttonStatus'];
        $newRequest->data['fieldEnabled'] = $workflow_object['fieldEnabled'];
        $newRequest->data['fieldRequired'] = $workflow_object['fieldRequired'];
        $newRequest->data['fieldHiddenValues'] = $workflow_object['fieldHiddenValue'];
        $newRequest->data['Node_ID'] = 'Draft';
        $newRequest->modify();
    } else {
        $newRequest->processWF();
    }
    $node_type = $nextNode['NodeType'];
    $workflowData = json_decode($nextNode['WorkflowData'], true);
    $trigger_data = $workflowData['workflow-trigger'];
    $processor_type = $workflowData['processorType'];
    $processor_value = $workflowData['processor'];

    $fields = functions::getFields(" WHERE form_id = {$formID} AND field_type  IN ('textbox_editor_support','textbox_reader_support')");
}



if ($node_type == 3 || $trigger_data || count($fields) > 0 || count($all_result) > 1000) {
    $request_ctr = 0;
    //complex workflow  update on by one
    for ($ctr = 1; $ctr <= count($all_result); $ctr++) {
        $request_ctr++;
        $result = $all_result[$ctr - 1];
        $newRequest = new Request();
        $newRequest->load($formID, $insert_result, $personDoc);
        $newRequest->data = $result;
        $trackNoField['TrackNo'] = library::getTrackNo($db, $newRequest->referencePrefix, $newRequest->referenceType, $newRequest->id);
        $newRequest->trackNo = $trackNoField['TrackNo'];
        $newRequest->data["TrackNo"] = $trackNoField['TrackNo'];
        if ($embed_submission != "" && $embed_submission != "default") {
            $newRequest->data['Status'] = "DRAFT";
            $newRequest->data['Processor'] = $auth['id'];
            $newRequest->data['LastAction'] = $workflow_object['buttonStatus'];
            $newRequest->data['fieldEnabled'] = $workflow_object['fieldEnabled'];
            $newRequest->data['fieldRequired'] = $workflow_object['fieldRequired'];
            $newRequest->data['fieldHiddenValues'] = $workflow_object['fieldHiddenValue'];
            $newRequest->data['Node_ID'] = 'Draft';
            $newRequest->modify();
        } else {
            $newRequest->modify();
            $newRequest->processWF();
        }
        $insert_result++;

        functions::clearRequestRelatedCache("request_list", "form_id_" . $formID);
        functions::clearRequestRelatedCache("picklist", "form_id_" . $formID);
        functions::clearRequestRelatedCache("calendar_view", "form_id_" . $formID);
        functions::clearRequestRelatedCache("request_list_count", "form_id_" . $formID);
        $deleteMemecachedKeys = array("request_details_" . $formID, "request_access_" . $formID);
        $fs->deleteMemcacheKeys($deleteMemecachedKeys);
        var_dump("insert count: " . $insert_result);

        if ($request_ctr == 30) {
            sleep(5);
            $request_ctr = 0;
        }
    }
} else {
    //basic just copy data
    $reference_request = $db->query("SELECT * FROM " . $formDoc->form_table_name . " WHERE ID = " . $insert_result, "row");

    $processor_sql = "";
    if ($processor_type == 5) {
        $field_to_copy = array("Status",
            "LastAction", "fieldEnabled", "fieldRequired", "fieldHiddenValues", "SaveFormula", "CancelFormula",
            "ProcessorType", "ProcessorLevel"
        );
        $processor_sql = " Processor = (SELECT u.id FROM tbuser u where display_name=r." . $processor_value . "), ";
    } else {
        $field_to_copy = array("Status",
            "LastAction", "fieldEnabled", "fieldRequired", "fieldHiddenValues", "SaveFormula", "CancelFormula",
            "Processor", "ProcessorType", "ProcessorLevel"
        );
    }


    $update_sql = "UPDATE " . $formDoc->form_table_name . " r SET ";

    if ($formDoc->reference_type == 'Sequential') {
        $update_sql.= " TrackNo = CONCAT('" . $formDoc->reference_prefix . "',IF(length(id)<=4,lpad(id, 4, 0),id)), ";
    } else {
        $dateNow = $db->query('SELECT Now() as DateTimeNow', 'row');
        $date = new DateTime($dateNow['DateTimeNow']);

        $trackNo = $referencePrefix . date_format($date, '-ymd-himA');
        $update_sql.= " TrackNo =CONCAT('" . $formDoc->reference_prefix . "','-', r.id ,{$db->escape($trackNo)}), ";
    }

    $update_sql.=$processor_sql;

    $ctr = 1;
    foreach ($field_to_copy as $value) {
        if (count($field_to_copy) != $ctr) {
            $update_sql.= $value . " = " . $db->escape($reference_request[$value]) . ", ";
        } else {
            $update_sql.= $value . " = " . $db->escape($reference_request[$value]) . " ";
        }
        $ctr++;
    }

    $update_sql .=" WHERE id IN ( ";
    $end_row = $insert_result + count($all_result) - 1;

    for ($row_ctr = $insert_result; $row_ctr <= $end_row; $row_ctr++) {
        if ($row_ctr < $end_row) {
            $update_sql .= $row_ctr . ", ";
        } else {
            $update_sql .= $row_ctr . " ";
        }
    }

    $update_sql .=")";
//        print_r($update_sql);

    $db->query($update_sql);

    //copy request users
    $request_users = $db->query("SELECT * FROM tbrequest_users WHERE requestid={$insert_result} "
            . " AND form_id={$formID}", "array");

    $copy_sql = " INSERT INTO tbrequest_users (Form_ID, RequestID, User, User_Type, Action_Type)  VALUES ";

    $end_row = $insert_result + count($all_result) - 1;
    for ($row_ctr = $insert_result; $row_ctr <= $end_row; $row_ctr++) {
        foreach ($request_users as $key => $value) {
            $copy_sql.= "('" . $value["Form_ID"] . "',"
                    . " '" . $insert_result . "',"
                    . " '" . $value["User"] . "',"
                    . " '" . $value["User_Type"] . "',"
                    . " '" . $value["Action_Type"] . "'"
                    . "),";
        }

        $insert_result++;
    }


    $copy_sql = substr($copy_sql, 0, -1);
    $db->query($copy_sql);


    //save audit logs
    $audit_logs = $db->query("SELECT * FROM tbrequest_logs WHERE form_id={$formID} AND request_id={$reference_request["ID"]}", "row");
    $insert_log_sql = "INSERT INTO tbrequest_logs (form_id, request_id, details, date_created, created_by)  VALUES ";

    $end_row = $audit_logs["request_id"] + count($all_result) - 1;
    for ($row_ctr = $audit_logs["request_id"] + 1; $row_ctr <= $end_row; $row_ctr++) {
        if ($row_ctr < $end_row) {
            $insert_log_sql .= "({$db->escape($audit_logs["form_id"])},{$row_ctr},"
                    . "{$db->escape($audit_logs["details"])}, Now(), {$db->escape($audit_logs["created_by"])} ),";
        } else {
            $insert_log_sql .= "({$db->escape($audit_logs["form_id"])},{$row_ctr},"
                    . "{$db->escape($audit_logs["details"])}, Now(), {$db->escape($audit_logs["created_by"])} )";
        }
    }


    $db->query($insert_log_sql);
}
if ($embed_ref_key != "" && $embed_ref_val != "") {
    $message = $formDoc->form_name . " request/s has been successfully imported.<br>"
            . " Insert Count : " . count($fieldValues);
} else {
    $message = $formDoc->form_name . " request/s has been successfully imported.<br>"
            . " Insert Count : " . count($fieldValues) . "<br/>"
            . " Update Count : " . count($reference_array);
}

$mail_info = array(
    "To" => $to,
    "CC" => null,
    "BCC" => null,
    "From" => $from,
    "Title" => $subject,
    "From_Title" => $from_title,
    "Body" => $message,
    "Form_Id" => $formID,
);

var_dump("sending mail to....");
var_dump($to);


$mailDoc = new Mail_Notification();
$mailDoc->import_notify_user($mail_info);

//$mailDoc->sendEmail_smtp($formDoc->form_name . " import details.", $to, null, null, $from, null, $from_title, $subject);
var_dump("mail sent.");
var_dump("import done");

echo json_encode(
        array(
            "application_id" => $fs->base_encode_decode("encrypt", $formID),
            "import_option" => $import_option,
            "insert_count" => count($fieldValues),
            "update_count" => count($reference_array)
));

functions::clearRequestRelatedCache("request_list", "form_id_" . $formID);
functions::clearRequestRelatedCache("picklist", "form_id_" . $formID);
functions::clearRequestRelatedCache("calendar_view", "form_id_" . $formID);
functions::clearRequestRelatedCache("request_list_count", "form_id_" . $formID);
$deleteMemecachedKeys = array("request_details_" . $formID, "request_access_" . $formID);
$fs->deleteMemcacheKeys($deleteMemecachedKeys);

unlink($location);
