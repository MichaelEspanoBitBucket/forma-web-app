<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getPathIndex() {
    $path = realpath(dirname(__FILE__));
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        $delimiter = "\\";
    } else {
        $delimiter = "/";
    }
    $path_arr = explode($delimiter, $path);
    $path_index = "";
    for ($ctr = 0; $ctr < count($path_arr) - 1; $ctr++) {
        $path_index.=$path_arr[$ctr] . $delimiter;
    }

    $path_index.="index.php";
    return $path_index;
}

include getPathIndex();

$db = new Database();

$options = getopt("f:r:u:");
$form_id = $options["f"];
$request_id = $options["r"];
$user_id = $options["u"];

$requestDoc = new Request();
$personDoc = new Person($db, $user_id);
$GLOBALS["auth"] = $db->query("SELECT * FROM tbuser WHERE id={$personDoc->id}", "row");
$requestDoc->load($form_id, $request_id, $personDoc);
$requestDoc->processWorkflowEvents();



