<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getPathIndex() {
    $path = realpath(dirname(__FILE__));
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        $delimiter = "\\";
    } else {
        $delimiter = "/";
    }
    $path_arr = explode($delimiter, $path);
    $path_index = "";
    for ($ctr = 0; $ctr < count($path_arr) - 1; $ctr++) {
        $path_index.=$path_arr[$ctr] . $delimiter;
    }

    $path_index.="index.php";
    return $path_index;
}

include getPathIndex();

class Rule_Controller {

    private static final function getRules() {
        $db = new Database();
        $result = array();

        $rules = $db->query("SELECT * FROM tbmiddleware_settings WHERE is_active={$db->escape(1)}", "array");
        foreach ($rules as $rule) {
            $ruleObject = new Rule($db, $rule["id"]);
            array_push($result, $ruleObject);
        }

        return $result;
    }

    /*     * *****************************
     * Import Facility
     * ********************************************* */

    public static final function auto_import_csv($location, $formID, $requestorID) {
        $location = $location;
        $formID = $formID;
        $db = new Database();
        $fs = new functions();
        $formDoc = new Form($db, $formID);
        $countImportedRecord = 0;
        $return_array2 = import::autoImportCSV($location);
        // print_r($return_array2);
        // return false;
        foreach ($return_array2 as $return_array) {

            $headers = array();
            $fieldValues = array();
            foreach ($return_array as $key => $value) {
                if ($key == 0) {
                    array_push($headers, $value);
                } else {
                    $append = false;
                    foreach ($value as $val) {
                        if (trim($val) != "") {
                            $append = true;
                            break;
                        }
                    }

                    if ($append == true) {
                        array_push($fieldValues, $value);
                    }
                }
            }
            // $countImportedRecord = count($fieldValues);
            //SEND THIS COUNT TO EMAIL

            $result = array();
            $date = $fs->currentDateTime();

            foreach ($fieldValues as $key => $value) {
                $result = null;
                foreach ($fieldValues[$key] as $key_1 => $value_1) {
                    $result[$headers[0][$key_1]] = $fieldValues[$key][$key_1];
                    $result['imported'] = '1';
                    $result['Requestor'] = $requestorID;
                    $result['Processor'] = $requestorID;
                    //            $result['Status'] = 'Draft';
                    $workflow_object = functions::getFormActiveWorkflow($formDoc);
                    $workflow_json = json_decode($workflow_object["json"], true);

                    $result['Workflow_ID'] = $workflow_object["workflow_id"];
                    $result['Node_ID'] = $workflow_json["workflow-default-action"];
                    $result['DateCreated'] = $date;
                }

                $newRequest = new Request();
                $person = new Person($db, $requestorID);
                $newRequest->load($formID, '0', $person);
                $newRequest->data = $result;
                $newRequest->save();
                $newRequest->processWF();

                $countImportedRecord++;
            }
        }
        $ret = array("count" => $countImportedRecord, "headers" => $headers, "records" => $fieldValues);
        return $ret;
    }

    private static final function getSelection() {
        $db = new Database();
        $date = $db->currentDateTime();

        $rules = Rule_Controller::getRules();
        foreach ($rules as $rule) {
            $continue = true;
            $selection = array();
            $formulaObject = new Formula($rule->formula);
            $actions = json_decode($rule->actions, true);
            $schedule = json_decode($rule->rep_data, true);

            $GLOBALS["auth"] = $db->query("SELECT * FROM tbuser WHERE id ={$rule->form->creator->id}", "row");

            $requests = Rule_Controller::getRequests($rule->form->id);
            $from_time = strtotime($rule->schedule_start_date);
//            $from_time = strtotime("2014-05-17 07:00");
            $to_time = strtotime($date);

            $scheduled_month = array();
            $values = $schedule[1]["value"];

//			print_r($rule->schedule_start_date);
//			print_r($date);
//			print_r('xxxxxxxxxxxxxxx');
//            print_r($date);
//            echo round(($to_time - $from_time) / 60, 2) . " minute";
            //validate schedule
            switch ($schedule[0]["value"]) {
                case "one_time":
                    if (round(($to_time - $from_time) / 60, 2) < 0) {
                        print_r("time not equal");
                        $continue = false;
                    }
                    break;
                case "daily":
                    if (round(($to_time - $from_time) / 60, 2) < 0) {
                        print_r("time not equal");
                        $continue = false;
                    }
                    break;
                case "weekly":
                    $scheduled_day = $schedule[1]["value"]["selected_weekly_byday"];
                    $day_today = date('l', strtotime($date));

                    if (strtolower($scheduled_day) != strtolower($day_today)) {
                        print_r("day not equal");
                        $continue = false;
                    }
                    if (round(($to_time - $from_time) / 60, 2) < 0) {
                        print_r("time not equal");
                        $continue = false;
                    }
                    break;
                case "monthly":


                    $month_continue = false;
                    $date_continue = false;
                    $month_today = date('n', strtotime($date));
                    $day_today = date('j', strtotime($date));
                    $months_dates = $schedule[1]["value"];

                    foreach ($months_dates as $key => $value) {
                        $month_pos = strrpos($key, "months");
                        if ($month_pos == 8 && $month_today == $value) {
                            $month_continue = true;
                        }
                    }

                    foreach ($months_dates as $key => $value) {
                        $days_pos = strrpos($key, "days");
                        if ($days_pos == 8 && $day_today == $value) {
                            $date_continue = true;
                        }
                    }

                    if ($month_continue == false || $date_continue == false) {
                        print_r("month or date not equal");
                        $continue = false;
                    }

                    if (round(($to_time - $from_time) / 60, 2) < 0) {
                        print_r("time not equal");
                        $continue = false;
                    }

                    break;
            }



            $lastRun = date("Y-m-d", strtotime($rule->processDate));
            $date_now = date("Y-m-d", strtotime($date));


            print_r($lastRun . " = " . $date_now);
            if (!$schedule[4]["value"] && $lastRun == $date_now) {
                $continue = false;
            }

            $rule->processDate = $date;
            $rule->update();

            if ($continue == true) {
                foreach ($requests as $request) {
                    $formulaObject->DataFormSource[0] = $request;
                    if ($formulaObject->evaluate() == 1) {
                        array_push($selection, $request);
                    }
                }

                foreach ($selection as $request) {
                    foreach ($actions as $action) {
                        if ($action["name"] == "ftrigger") {
                            foreach ($action["value"] as $respone) {
                                if ($respone["workflow-trigger-action"] == "Create") {
                                    //create response
                                    Rule_Controller::createResponse($rule->form, $request, $respone);
                                }
                                if ($respone["workflow-trigger-action"] == "Update Response") {
                                    //update response
                                    Rule_Controller::updateResponse($rule->form, $request, $respone);
                                }
                            }
                        }

                        if ($action["name"] == "etrigger") {
                            //send email
                            Rule_Controller::sendMail($rule->form, $request, $action["value"]);
                        }

                        if ($action["name"] == "smstrigger") {
                            //send sms
                            Rule_Controller::sendSMS($rule->form, $request, $action["value"]);
                        }
                    }
                }

                foreach ($actions as $action) {
                    if ($action["name"] == "auto-import") {
                        $auto_import_directory = $action["value"]['auto_import_directory'];
                        $auto_import_form = $action["value"]['auto_import_form'];
                        $auto_import_requestor = $action["value"]['auto_import_requestor'];
                        $auto_import_recepient = $action["value"]['auto_import_recepient'];
                        // print_r("OK BAGO MAG IMPORT\n");
                        $importedRecord = Rule_Controller::auto_import_csv($auto_import_directory, $auto_import_form, $auto_import_requestor);
                        // print_r("OK TAPOS MAG IMPORT\n");
                        // print_r("RECORD COUNT: ".$importedRecord['count']."\n");
                        // print_r("NOTIF RECEPIENT: ".$auto_import_recepient);

                        if ($importedRecord['count'] > 0) {
                            $json = array("count" => $importedRecord['count'], "recipient" => $auto_import_recepient, "headers" => $importedRecord['headers'], "records" => $importedRecord['records']);
                            $mailDoc = new Mail_Notification();
                            $mailDoc->sendMailCount($json);
                        }
                    }
                }
            }
        }
    }

    private static final function getRequests($form_id, $condition) {
        $db = new Database();
        $formDoc = new Form($db, $form_id);
        $result = $db->query("SELECT * FROM {$formDoc->form_table_name}", "array");

        return $result;
    }

    private static final function sendMail($parentDocForm, $parentDoc, $emailArr) {
        $db = new Database();

        $trackingNo = $parentDoc['TrackNo'];
        $requestor = $parentDoc['Requestor'];
        $processor = $parentDoc['Processor'];

        $personDoc = new Person($db, $requestor);
        $processorDoc = new Person($db, $processor);

//        $emailArr = json_decode($emailJSON, true);
        $recipients = array();
        $recipient_department_users = array();
        $recipient_position_users = array();
        $cc = array();
        $cc_department_users = array();
        $cc_position_users = array();
        $bcc = array();
        $bcc_department_users = array();
        $bcc_position_users = array();


        //to
        $recipient_department = $emailArr['email_recpient']['departments'];
        $recipient_position = $emailArr['email_recpient']['positions'];
        $recipient_users = $emailArr['email_recpient']['users'];
        $recipient_requestor = $emailArr['email_recpient']['requestor'];
        $recipient_processor = $emailArr['email_recpient']['processor'];
        //new aaron
        $recipient_otherRecepient_type = $emailArr['email_recpient']['otherRecepient_type'];
        $recipient_otherRecepient = $emailArr['email_recpient']['otherRecepient'];


        //other recepient
        if ($recipient_otherRecepient_type != "") {
            if ($recipient_otherRecepient_type == "1") {
                $formulaDoc = new Formula($recipient_otherRecepient);
                $formulaDoc->DataFormSource[0] = $parentDoc;
                $recipient_otherRecepient = $formulaDoc->evaluate();
            }

            $recipient_otherRecepient_array = split(';', $recipient_otherRecepient);
            foreach ($recipient_otherRecepient_array as $value) {
                if ($value != "") {
                    $recipients[$value] = "Other User";
                }
            }
        }

        //per department
        foreach ($recipient_department as $deparment) {
            array_push($recipient_department_users, functions::getUsers("WHERE department_id ={$db->escape($deparment)}"));
        }

        foreach ($recipient_department_users as $department_users) {
            foreach ($department_users as $userDoc) {
                $recipients[$userDoc->email] = $userDoc->display_name;
            }
        }

        //per position
        foreach ($recipient_position as $position) {
            array_push($recipient_position_users, functions::getUsers("WHERE position ={$db->escape($position)} AND company_id={$db->escape($personDoc->company->id)}"));
        }

        foreach ($recipient_position_users as $position_users) {
            foreach ($position_users as $userDoc) {
                $recipients[$userDoc->email] = $userDoc->display_name;
            }
        }

        //per user
        foreach ($recipient_users as $id) {
            $userDoc = new Person($db, $id);
            $recipients[$userDoc->email] = $userDoc->display_name;
        }

        //requestor
        if ($recipient_requestor == 1) {
            $requestorDoc = new Person($db, $requestor);
            $recipients[$requestorDoc->email] = $requestorDoc->display_name;
        }

        //processor
        if ($recipient_processor == 1) {
            $processorDoc = new Person($db, $processor);
            $recipients[$processorDoc->email] = $processorDoc->display_name;
        }

        //cc
        $cc_department = $emailArr['email_cc']['departments'];
        $cc_position = $emailArr['email_cc']['positions'];
        $cc_users = $emailArr['email_cc']['users'];
        $cc_requestor = $emailArr['email_cc']['requestor'];
        $cc_processor = $emailArr['email_cc']['requestor'];
        //new aaron
        $cc_otherRecepient_type = $emailArr['email_cc']['otherRecepient_type'];
        $cc_otherRecepient = $emailArr['email_cc']['otherRecepient'];

        //other recepient
        if ($cc_otherRecepient_type != "") {
            if ($cc_otherRecepient_type == "1") {
                $formulaDoc = new Formula($cc_otherRecepient);
                $formulaDoc->DataFormSource[0] = $parentDoc;
                $cc_otherRecepient = $formulaDoc->evaluate();
            }

            $cc_otherRecepient_array = split(';', $cc_otherRecepient);
            foreach ($cc_otherRecepient_array as $value) {
                if ($value != "") {
                    $cc[$value] = "Other User";
                }
            }
        }

        //per department
        foreach ($cc_department as $deparment) {
            array_push($cc_department_users, functions::getUsers("WHERE department_id ={$db->escape($deparment)}"));
        }

        foreach ($cc_department_users as $department_users) {
            foreach ($department_users as $userDoc) {
                $cc[$userDoc->email] = $userDoc->display_name;
            }
        }

        //per position
        foreach ($cc_position as $position) {
            array_push($cc_position_users, functions::getUsers("WHERE position ={$db->escape($position)} AND company_id={$db->escape($personDoc->company->id)}"));
        }

        foreach ($cc_position_users as $position_users) {
            foreach ($position_users as $userDoc) {
                $cc[$userDoc->email] = $userDoc->display_name;
            }
        }

        //per user
        foreach ($cc_users as $id) {
            $userDoc = new Person($db, $id);
            $cc[$userDoc->email] = $userDoc->display_name;
        }

        //requestor
        if ($cc_requestor == 1) {
            $requestorDoc = new Person($db, $requestor);
            $cc[$requestorDoc->email] = $requestorDoc->display_name;
        }

        //processor
        if ($cc_processor == 1) {
            $processorDoc = new Person($db, $processor);
            $cc[$processorDoc->email] = $processorDoc->display_name;
        }

        //bcc
        $bcc_department = $emailArr['email_bcc']['departments'];
        $bcc_position = $emailArr['email_bcc']['positions'];
        $bcc_users = $emailArr['email_bcc']['users'];
        $bcc_requestor = $emailArr['email_bcc']['requestor'];
        $bcc_processor = $emailArr['email_bcc']['requestor'];
        //new aaron
        $bcc_otherRecepient_type = $emailArr['email_bcc']['otherRecepient_type'];
        $bcc_otherRecepient = $emailArr['email_bcc']['otherRecepient'];

        //other recepient
        if ($bcc_otherRecepient_type != "") {
            if ($bcc_otherRecepient_type == "1") {
                $formulaDoc = new Formula($bcc_otherRecepient);
                $formulaDoc->DataFormSource[0] = $parentDoc;
                $bcc_otherRecepient = $formulaDoc->evaluate();
            }

            $bcc_otherRecepient_array = split(';', $bcc_otherRecepient);
            foreach ($bcc_otherRecepient_array as $value) {
                if ($value != "") {
                    $bcc[$value] = "Other User";
                }
            }
        }

        //per department
        foreach ($bcc_department as $deparment) {
            array_push($bcc_department_users, functions::getUsers("WHERE department_id ={$db->escape($deparment)}"));
        }

        foreach ($bcc_department_users as $department_users) {
            foreach ($department_users as $userDoc) {
                $bcc[$userDoc->email] = $userDoc->display_name;
            }
        }

        //per position
        foreach ($bcc_position as $position) {
            array_push($bcc_position_users, functions::getUsers("WHERE position ={$db->escape($position)} AND company_id={$db->escape($personDoc->company->id)}"));
        }

        foreach ($bcc_position_users as $position_users) {
            foreach ($position_users as $userDoc) {
                $bcc[$userDoc->email] = $userDoc->display_name;
            }
        }

        //per user
        foreach ($bcc_users as $id) {
            $userDoc = new Person($db, $id);
            $bcc[$userDoc->email] = $userDoc->display_name;
        }

        //requestor
        if ($bcc_requestor == 1) {
            $requestorDoc = new Person($db, $requestor);
            $bcc[$requestorDoc->email] = $requestorDoc->display_name;
        }

        //processor
        if ($bcc_processor == 1) {
            $processorDoc = new Person($db, $processor);
            $bcc[$processorDoc->email] = $processorDoc->display_name;
        }
        // print_r("recepient");
        // print_r($recipients);
        // print_r("cc");
        // print_r($cc);
        // print_r("bcc");
        // print_r($bcc);
        //subject and body
        $subject_type = $emailArr['email-title-type'];
        $subject_message = $emailArr['title'];
        $body_type = $emailArr['email-message-type'];
        $body_message = $emailArr['message'];
        $requestFormDoc = new Form($db, $parentDocForm->id);

        if ($subject_type == 0) {
            $subject = $emailArr['title'];
        } else {
            $formulaDoc = new Formula($emailArr['title']);
            $formulaDoc->DataFormSource[0] = $parentDoc;
            $subject = $formulaDoc->evaluate();
        }

        if ($body_type == 0) {
            $message = $emailArr['message'];
        } else {
            $formulaDoc = new Formula($emailArr['message']);
            $formulaDoc->DataFormSource[0] = $parentDoc;
            $message = $formulaDoc->evaluate();
        }

        $from = SMTP_FROM_EMAIL;
        $from_title = SMTP_FROM_NAME;
        $company_id = $personDoc->company->id;
        $formID = $parentDocForm->id;
        $requestID = $parentDoc["ID"];

        $mail_info = array(
            "To" => $recipients,
            "CC" => $cc,
            "BCC" => $bcc,
            "From" => $from,
            "Title" => $subject,
            "From_Title" => $from_title,
            "Body" => $message,
            "Company_Id" => $company_id,
            "TrackNo" => $trackingNo,
            "Form_Id" => $formID,
            "Request_Id" => $requestID,
            "Requestor_name" => $personDoc->first_name . " " . $personDoc->last_name,
            "Processor_name" => $processorDoc->first_name . " " . $processorDoc->last_name,
        );

//        print_r($emailArr);
        // print_r($mail_info);
        $emailDoc = new Mail_Notification();
//        $json_decode = Settings::noti_settings($processor, 'notifications');
//
//        $json_request = $json_notifications['Request'];
//        if ($json_request[0] == 1) {
//            $emailDoc->workflow_notify_user($mail_info);
//        }
//        //$json_decode = Settings::noti_settings($processor,'notifications');
//        //$json_notifications = $json_decode[0]['notifications'];
//        //$json_request = $json_notifications['Request'];
//        //if($json_request[0]==1){
        $emailDoc->workflow_notify_user($mail_info);
    }

    private static final function sendSMS($parentDocForm, $parentDoc, $smsArr) {
        $db = new Database();

        $sms_recipients = array();
        $userDoc = new Person($db, $parentDoc["Requestor"]);

        $sms_recipient_department = $smsArr['departments'];
        $sms_recipient_position = $smsArr['positions'];
        $sms_recipient_users = $smsArr['users'];
        $sms_recipient_requestor = $smsArr['requestor'];
        $sms_recipient_processor = $smsArr['processor'];
        $sms_recipient_contact = $smsArr['contact'];
        $sms_recipient_contact_type = $smsArr['recipient-type'];


        if ($smsArr['message-type'] == '0') {
            $message = $smsArr['message'];
        } else {
            $formulaDoc = new Formula($smsArr['message']);
            $formulaDoc->DataFormSource[0] = $parentDoc;
            $message = $formulaDoc->evaluate();
        }

        $sms_recipient_department_users = array();
        $sms_recipient_position_users = array();
        //per department
        foreach ($sms_recipient_department as $deparment) {
            array_push($sms_recipient_department_users, functions::getUsers("WHERE department_id ={$db->escape($deparment)}"));
        }

        foreach ($sms_recipient_department_users as $department_users) {
            foreach ($department_users as $userDoc) {
                array_push($sms_recipients, $userDoc->contact_number);
            }
        }

        //per position
        foreach ($sms_recipient_position as $position) {
            array_push($sms_recipient_position_users, functions::getUsers("WHERE position ={$db->escape($position)} AND company_id={$db->escape($userDoc->company->id)}"));
        }

        foreach ($sms_recipient_position_users as $position_users) {
            foreach ($position_users as $userDoc) {
                array_push($sms_recipients, $userDoc->contact_number);
            }
        }

        //per user
        foreach ($sms_recipient_users as $id) {
            $userDoc = new Person($db, $id);
            array_push($sms_recipients, $userDoc->contact_number);
        }

        //requestor
        if ($sms_recipient_requestor) {
            $requestorDoc = new Person($db, $parentDoc["Requestor"]);
            array_push($sms_recipients, $requestorDoc->contact_number);
        }

        //processor
        if ($sms_recipient_processor) {
            $processorDoc = new Person($db, $parentDoc["Processor"]);
            array_push($sms_recipients, $processorDoc->contact_number);
        }

        //contact_number
        if ($sms_recipient_contact != '') {
            if ($sms_recipient_contact_type == "1") {
                $requestFormDoc = new Form($db, $parentDocForm->formId);
                $formulaDoc = new Formula($smsArr['contact']);
                $formulaDoc->DataFormSource[0] = $parentDoc;
                $sms_recipient_contact = $formulaDoc->evaluate();
            }
            $sms_recipient_contact_arr = split(';', $sms_recipient_contact);
            foreach ($sms_recipient_contact_arr as $contact_number) {
                array_push($sms_recipients, $contact_number);
            }
        }

        foreach ($sms_recipients as $number) {
            $smsDoc = new SMS($number, $message);
            $smsDoc->send();
        }
    }

    private static final function createResponse($parentDocForm, $parentDoc, $data) {
        $redis_cache = getRedisConnection();
        $db = new Database();
        $date = $db->currentDateTime();
        $personDoc = new Person($db, $parentDoc["Requestor"]);

        $formDoc = new Form($db, $data["workflow-form-trigger"]);
        $workflow_object = functions::getFormActiveWorkflow($formDoc);
        $workflow_json = json_decode($workflow_object["json"], true);
        $actions = $data["workflow-field_formula"];
        $result = array();

        $result['Requestor'] = $personDoc->id;
        $result['Processor'] = $personDoc->id;
        $result['DateCreated'] = $date;
        $result['DateUpdated'] = $date;
        $result['Workflow_ID'] = $workflow_object["workflow_id"];
        $result['Node_ID'] = $workflow_json["workflow-default-action"];

        foreach ($actions as $key) {
            $field_name = $key['workflow-field-update'];
            $formula = $key['workflow-field-formula'];

            $formulaDoc = new Formula($formula);
            $formulaDoc->DataFormSource[0] = $parentDoc;

            $returnValue = $formulaDoc->evaluate();

            $result[$field_name] = $returnValue;
        }

        print_r($result);
        $requestDoc = new Request();
        $requestDoc->load($formDoc->id, '0', $personDoc);
        $requestDoc->data = $result;
        $requestDoc->save();
        $requestDoc->processWF();

        $requestLog = new Request_Log($db);
        $requestLog->form_id = $data["workflow-form-trigger"];
        $requestLog->request_id = $requestDoc->id;
        $requestLog->details = 'Created from ' . $parentDocForm->form_name . ' with Tracking Number : ' . $parentDoc["TrackNo"];
        $requestLog->created_by_id = $parentDoc["Requestor"];
        $requestLog->date_created = $date;
        $requestLog->save();

        if ($redis_cache) {
            $myrequest_memcached = array("my_request_list_widget", "my_request_list_page", "my_request_list_page_count", "my_request_calendar");

            $deleteMemecachedKeys = array_merge($myrequest_memcached);

            functions::deleteMemcacheKeys($deleteMemecachedKeys);

            functions::clearRequestRelatedCache("request_list", "form_id_" . $formDoc->id);
            functions::clearRequestRelatedCache("picklist", "form_id_" . $formDoc->id);
            functions::clearRequestRelatedCache("calendar_view", "form_id_" . $formDoc->id);
            functions::clearRequestRelatedCache("request_list_count", "form_id_" . $formDoc->id);
//            functions::clearRequestRelatedCache("request_details_" . $formDoc->id, $_POST['ID']);
//            functions::clearRequestRelatedCache("request_access_" . $formDoc->id, $_POST['ID']);
            //formula
            $formula_arr = array("formula_lookup_get_max", "formula_lookup_get_min", "formula_lookup_sdev",
                "formula_lookup_sdev2", "formula_lookup_avg", "formula_lookup_count_if",
                "formula_lookup_count_if", "formula_lookup_total", "formula_lookup_where",
                "formula_lookup_where_array", "formula_lookup", "formula_lookup_record_count", "formula_lookup_record_count");
            functions::clearFormulaLookupCaches($formDoc->id, $formula_arr);
        }
    }

    public static final function updateResponse($parentDocForm, $parentDoc, $data) {
        $redis_cache = getRedisConnection();
        $db = new Database();
        $date = $db->currentDateTime();
        $form_id = $data['workflow-form-trigger'];
        $reference_field = $data['workflow-ref-field'];
        $parent_refernce_field = $workflowData['workflow-ref-field-parent'];
        $actions = $data['workflow-field_formula'];

        $requestFormDoc = new Form($db, $parentDocForm->id);
        $formDoc = new Form($db, $form_id);
        $personDoc = new Person($db, $parentDoc["Requestor"]);
        $result = array();
        foreach ($actions as $key) {
            $field_name = $key['workflow-field-update'];
            $formula = $key['workflow-field-formula'];


            $formulaDoc = new Formula($formula);

            $formulaDoc->DataFormSource[0] = $parentDoc;

            $returnValue = $formulaDoc->evaluate();
            $result[$field_name] = $returnValue;
        }

        $reference_field_value = $parentDoc[$reference_field];
        $parent_refernce_field_name = $parent_refernce_field;

        if ($parent_refernce_field == "") {
            $parent_refernce_field_name = "TrackNo";
        }

        $request_row = $db->query("SELECT ID FROM " . $formDoc->form_table_name . " WHERE $parent_refernce_field_name = {$db->escape($reference_field_value)}", "row");


        $requestDoc = new Request();
        $requestDoc->load($form_id, $request_row['ID']);
        $requestDoc->data = $result;
        $requestDoc->modify();
        if ($redis_cache) {
            $myrequest_memcached = array("my_request_list_widget", "my_request_list_page", "my_request_list_page_count", "my_request_calendar");

            $deleteMemecachedKeys = array_merge($myrequest_memcached);

            functions::deleteMemcacheKeys($deleteMemecachedKeys);

            functions::clearRequestRelatedCache("request_list", "form_id_" . $formDoc->id);
            functions::clearRequestRelatedCache("picklist", "form_id_" . $formDoc->id);
            functions::clearRequestRelatedCache("calendar_view", "form_id_" . $formDoc->id);
            functions::clearRequestRelatedCache("request_list_count", "form_id_" . $formDoc->id);
            functions::clearRequestRelatedCache("request_details_" . $formDoc->id, $request_row['ID']);
            functions::clearRequestRelatedCache("request_access_" . $formDoc->id, $request_row['ID']);
            //formula
            $formula_arr = array("formula_lookup_get_max", "formula_lookup_get_min", "formula_lookup_sdev",
                "formula_lookup_sdev2", "formula_lookup_avg", "formula_lookup_count_if",
                "formula_lookup_count_if", "formula_lookup_total", "formula_lookup_where",
                "formula_lookup_where_array", "formula_lookup", "formula_lookup_record_count", "formula_lookup_record_count");
            functions::clearFormulaLookupCaches($formDoc->id, $formula_arr);
        }
    }

    public static final function process() {
        Rule_Controller::getSelection();
    }

}

Rule_Controller::process();
