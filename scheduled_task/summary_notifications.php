<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//include realpath(dirname(__FILE__) . '/../index.php');

include 'C:\wamp\www\Projects\eformalistics\Formalistics\index.php';

$db = new Database();
$getUser = $db->query("SELECT * FROM tbuser WHERE is_active={$db->escape(1)}", "array");

foreach ($getUser as $user) {

    $noti_mention_post = $db->query("SELECT * FROM tbnotification WHERE
                                        userID={$db->escape($user['id'])}
                                        AND user_read={$db->escape(0)}
                                        AND is_active={$db->escape(1)}
                                        AND type={$db->escape(1)}", "numrows");
    $noti_comment_post = $db->query("SELECT * FROM tbnotification WHERE
                                        userID={$db->escape($user['id'])}
                                        AND user_read={$db->escape(0)}
                                        AND is_active={$db->escape(1)}
                                        AND type={$db->escape(2)}", "numrows");
    $noti_like_post = $db->query("SELECT * FROM tbnotification WHERE
                                        userID={$db->escape($user['id'])}
                                        AND user_read={$db->escape(0)}
                                        AND is_active={$db->escape(1)}
                                        AND type={$db->escape(3)}", "numrows");
    $noti_like_comment = $db->query("SELECT * FROM tbnotification WHERE
                                        userID={$db->escape($user['id'])}
                                        AND user_read={$db->escape(0)}
                                        AND is_active={$db->escape(1)}
                                        AND type={$db->escape(4)}", "numrows");
    $noti_request = $db->query("SELECT * FROM tbnotification WHERE
                                        userID={$db->escape($user['id'])}
                                        AND user_read={$db->escape(0)}
                                        AND is_active={$db->escape(1)}
                                        AND type={$db->escape(5)}", "numrows");
    $noti_mention_comment = $db->query("SELECT * FROM tbnotification WHERE
                                        userID={$db->escape($user['id'])}
                                        AND user_read={$db->escape(0)}
                                        AND is_active={$db->escape(1)}
                                        AND type={$db->escape(6)}", "numrows");
    $noti_task_Ateam = $db->query("SELECT * FROM tbnotification WHERE
                                        userID={$db->escape($user['id'])}
                                        AND user_read={$db->escape(0)}
                                        AND is_active={$db->escape(1)}
                                        AND type={$db->escape(10)}", "numrows");
    $noti_task_Eteam = $db->query("SELECT * FROM tbnotification WHERE
                                        userID={$db->escape($user['id'])}
                                        AND user_read={$db->escape(0)}
                                        AND is_active={$db->escape(1)}
                                        AND type={$db->escape(11)}", "numrows");
    $noti_task_Vteam = $db->query("SELECT * FROM tbnotification WHERE
                                        userID={$db->escape($user['id'])}
                                        AND user_read={$db->escape(0)}
                                        AND is_active={$db->escape(1)}
                                        AND type={$db->escape(12)}", "numrows");
    $noti_task_due = $db->query("SELECT * FROM tbnotification WHERE
                                        userID={$db->escape($user['id'])}
                                        AND user_read={$db->escape(0)}
                                        AND is_active={$db->escape(1)}
                                        AND type={$db->escape(13)}", "numrows");
    // $msg = userQueries::countMsg($user['id']);

    $msgCount = $db->query("SELECT COUNT(DISTINCT r.message_id)  FROM tbseen s
                                 LEFT JOIN tbreply_message r on r.id=s.seen_id
                                 WHERE s.userID={$user['id']} AND s.type={$db->escape(1)} AND s.user_read={$db->escape(0)} AND s.is_active={$db->escape(1)}
                                 GROUP BY r.message_id", "numrows");



    if ($msgCount < 0) {
        $msg = "0";
    } else {
        $msg = $msgCount;
    }




    //echo "ID" . $user['id'] . "=== (Mention) = " . $noti_mention_post . " (Comment) = " . $noti_comment_post;
    //echo " (Like Post) = " . $noti_like_post . " (Like Comment) = " . $noti_like_comment;
    //echo " (Request) = " . $noti_request . " (Mention Comment) = " . $noti_mention_comment;
    //echo " (Added Team) = " . $noti_task_team . " (Editor Team) = " . $noti_task_Eteam;
    //echo " (Viewer Team) = " . $noti_task_Vteam . " (Due Date) = " . $noti_task_due;
    //echo " (Msg) =" . $msg;
    //echo "<br>";

    $userQueries = new userQueries();

    $noti = array("Tadded" => $noti_task_team,
        "Tview" => $noti_task_Vteam,
        "Tedit" => $noti_task_Eteam,
        "Tdue" => $noti_task_due,
        "Msg" => $msg,
        "Lpost" => $noti_like_post,
        "Lcomment" => $noti_like_comment,
        "request" => $noti_request,
        "user_level_id" => $user['user_level_id'],
        "img" => $userQueries->avatarPic("tbuser", $user['id'], "44", "44", "small", "avatar ", "float: left;border-radius:50px;margin-right: 5px;"));

    $settingDoc = new Settings();
    $mailDoc = new Mail_Notification();

    $json_decode = $settingDoc->noti_settings($user['id'], 'notifications');
    $json_notifications = $json_decode[0]['notifications'];
    $json_summary_notification = $json_notifications['summary_notification'];
    if ($json_summary_notification[0] == 1) {
        echo $mailDoc->email_prompt_notifications(array($user['email'] => $user['display_name']), $user['first_name'] . " " . $user['last_name'], $getUser, $noti);
    }
}
